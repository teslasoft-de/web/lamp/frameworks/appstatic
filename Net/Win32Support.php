<?php
/**
 * @package AppStatic
 * @name Tools
 * @version 1.0 (2009-01-01)
 * 
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow
 */

if(!function_exists('checkdnsrr')){
    function checkdnsrr($host, $type=''){
        if(!empty($host)){
            $type = (empty($type)) ? 'MX' :  $type;
            $result = null;
            exec("nslookup.exe -type=$type " . escapeshellcmd($host) . " 192.168.178.19", $result);
            foreach ($result as $line){            	
            	if (preg_match("/.*$host/",$line))
            		return true;
            }
            /*$it = new ArrayIterator($result);
            foreach(new RegexIterator($it, '~^'.$host.'~', RegexIterator::GET_MATCH) as $result){
                if($result){
                    return true;
                }                
            }*/
        }
        return false;
    }
}

if (!function_exists ('getmxrr') ) {
  function getmxrr($hostname, &$mxHosts, &$mxWeight) {
    if (!is_array ($mxHosts) ) {
      $mxHosts = array ();
    }

    if (!empty ($hostname) ) {
      $output = array();
      @exec ("nslookup.exe -type=MX " . escapeshellcmd($hostname) . " 192.168.178.19", $output);
      $imx=-1;

      foreach ($output as $line) {        
        $parts = "";
        if (preg_match ("/^$hostname\tMX preference = ([0-9]+), mail exchanger = (.*)$/", $line, $parts) ) {
		$imx++;
          $mxWeight[$imx] = $parts[1];
          $mxHosts[$imx] = $parts[2];
        }
      }
      return ($imx!=-1);
    }
    return false;
  }
}