<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 21.04.2015
 * File: AppStatic.Config.inc.php
 * Encoding: UTF-8
 * Project: AppStatic
 * Author: H. Christian Kusmanow
 * Copyright: © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 * */

// Path Locations
define( 'APPSTATIC_PATH', realpath( __DIR__ ) );
define( 'APPSTATIC_INC_RMAIL', realpath( APPSTATIC_PATH . '/../inc/rmail' ) );

// Database
define( 'APPSTATIC_DATABASE', 'appstatic' );
define( 'APPSTATIC_DEFAILT_TIMEZONE', 'Europe/Berlin' );

// Disabled Save Trace for local testing systems. (delimited by |)
define( 'APPSTATIC_LOCAL_SYSTEM_IP_RANGE', '128.0.128' );