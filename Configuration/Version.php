<?php

namespace AppStatic\Configuration;

/**
 * @package AppStatic
 * @name Version
 * @version 2.1 (2009-01-01)
 *
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow
 *
 * CHANGE LOG
 *
 * Version  Name                  Date        Description
 * --------+---------------------+-----------+---------------------------------------------
 * 1.0      Scott Christensen     04/23/2001  Creation
 * 1.0.1    Scott Christensen     06/05/2001  Changed check for empty string from
 * the function empty() to checking != "".
 * The empty function was not working on
 * PHP4 if the string being checked contained
 * "0".  I assumed empty in this case should
 * return false, but it was returning true.
 * 1.1.0    Christian Kusmanow    11/15/2008  PHP 5.0 OOP Conversion.
 * --------+---------------------+-----------+---------------------------------------------
 */
use AppStatic\Core\ExceptionBase;
use AppStatic\Core\PropertyBase;


/**
 * <p>This class splits a php version number into its different components.  The
 * supported components are:
 * <ul>
 * <li>Major Version</li>
 * <li>Minor Version</li>
 * <li>Revision Number</li>
 * <li>Release Candidate Number</li>
 * <li>Beta Version Number</li>
 * <li>Alpha Version Number</li>
 * <li>Patch Level Number</li>
 * </ul>
 *
 * <p>By default, all of the above components are -1.  If a component is not -1,
 * that means there is a value for that component.
 *
 * <p>This class also contains a compare function for comparing two version numbers
 * together.  It compares the version number associated with this class and a
 * version number that is passed in as an argument.  If the passed in version number
 * is newer than the version number associated with this class, 1 is returned, if
 * the passed in version number is older than the version number associated with this
 * class, -1 is returned, and if they are equal, 0 is returned.
 *
 * @access public
 */
class Version extends PropertyBase
{

    protected $VersionString;
    protected $MajorVersion;
    protected $MinorVersion;
    protected $Build;
    protected $Revision;
    protected $Subversioning;
    protected $AlphaRevision;
    protected $BetaRevision;
    protected $PatchLevelRevision;
    protected $ReleaseCandidateRevision;

    /**
     * This is the constructor for the PHP Version class.  If a version number is passed
     * in, that version number is used, otherwise the version number is defined by the
     * <code>phpversion()</code> function.  The version number is then parsed out into
     * its components.
     *
     * @access public
     * @param string $version An optional paramater specifying a version number. If not passed
     * in, the value from <code>phpversion()</code> is used.
     * @param bool $subversioning
     * @throws VersionException
     */
    function Version($version, $subversioning = false)
    {
        $this->VersionString = -1;
        $this->MajorVersion = -1;
        $this->MinorVersion = -1;
        $this->Build = -1;
        $this->Revision = -1;
        $this->Subversioning = $subversioning;
        $this->AlphaRevision = -1;
        $this->BetaRevision = -1;
        $this->PatchLevelRevision = -1;
        $this->ReleaseCandidateRevision = -1;

        if (empty($version))
            throw new VersionException("Invalid Version specified! Version can not be empty!");

        $this->VersionString = trim($version);

        $arr = explode(".", $this->VersionString);

        // set major version and its information
        if (isset($arr[0])) {
            $result = $this->setNumericPart($arr[0], $this->MajorVersion);
            if ($subversioning)
                $this->setSubVersion($result);
        }

        // set minor version and its information
        if (isset($arr[1])) {
            $result = $this->setNumericPart($arr[1], $this->MinorVersion);
            if ($subversioning)
                $this->setSubVersion($result);
        }

        // set build and its information
        if (isset($arr[2])) {
            $result = $this->setNumericPart($arr[2], $this->Build);
            if ($subversioning)
                $this->setSubVersion($result);
        }

        // set revision and its information
        if (isset($arr[3])) {
            $result = $this->setNumericPart($arr[3], $this->Revision);
            if ($subversioning)
                $this->setSubVersion($result);
        }
    }

    /**
     * This function returns true if <code>$ident</code> is the first
     * characters in the string <code>$str</code>.  Returns false
     * otherwise
     *
     * @access private
     * @param string $str The string in which to search for <code>$ident</code>.
     * @param string $ident The string to search for in <code>$str</code>.
     * @return boolean True if <code>$ident</code> is the first characters
     * in <code>$str</code>.  False otherwise.
     */
    private static function isIdentifier($str, $ident)
    {
        return (strtolower(substr($str, 0, strlen($ident))) == $ident);
    }

    /**
     * This function sets all of the fields that are not major, minor or
     * revision numbers.  This includes alpha revisions, beta revisions,
     * patch levels and release candidates.
     *
     * @access private
     * @param string $str The string to search for the subversion information.  This
     * string must contain a subversion identifier in its first characters
     * (i.e. p14 is patch level 14).  It can not contain the version number.
     * @return string The string left over after search for all subversion
     * identifiers.  All identifiers and information associated with those
     * identifiers will have been stripped from the string.  If the string
     * passed in is a valid php version string, this function will usually
     * return an empty string.
     */
    private function setSubVersion($str)
    {
        $done = false;
        while (!$done && !(empty($str))) {
            switch (true) {

                case $this->isIdentifier($str, "a"):
                    $str = $this->setNumericPart(substr($str, 1), $this->AlphaRevision);
                    break;

                case $this->isIdentifier($str, "b"):
                    $str = $this->setNumericPart(substr($str, 1), $this->BetaRevision);
                    break;

                case $this->isIdentifier($str, "pl"):
                    $str = $this->setNumericPart(substr($str, 2), $this->PatchLevelRevision);
                    break;

                case $this->isIdentifier($str, "-pl"):
                    $str = $this->setNumericPart(substr($str, 3), $this->PatchLevelRevision);
                    break;

                case $this->isIdentifier($str, "rc"):
                    $str = $this->setNumericPart(substr($str, 2), $this->ReleaseCandidateRevision);
                    break;

                case $this->isIdentifier($str, "-rc"):
                    $str = $this->setNumericPart(substr($str, 3), $this->ReleaseCandidateRevision);
                    break;

                default:
                    $done = true;
                    break;
            }
        }

        return $str;
    }

    /**
     * This function takes a string and returns a number identified by the first
     * x characters in the string.  It goes through the string one character at
     * a time until it finds a non-numeric character.  The substring that is the
     * number up to that point is then returned to the caller in <code>$result</code>
     * and the rest of the string from that point forward is returned by the
     * function.
     *
     * @param string $str The string in which to search for a number
     * @param string $result The resulting number from searching through <code>$str</code>.
     * @return string The resulting number from searching through <code>$str</code> is
     * passed back to the caller in <code>$result</code>
     * @return string The substring that is left after searching through
     * <code>$str</code> for a number.
     * @access private
     */
    private function setNumericPart($str, &$result)
    {
        $result = "";
        $stringLength = strlen($str);
        for ($i = 0; ($i < $stringLength) && ((string)intval(($char = substr($str, $i, 1))) == (string)$char); $i++)
            $result .= $char;

        if ($i == strlen($str))
            return "";
        else
            return substr($str, $i);
    }

    /**
     * Returns the major version number after parsing the version string
     *
     * @access public
     * @return string The major version number
     */
    public function GetMajorVersion()
    {
        return $this->MajorVersion;
    }

    /**
     * Returns the minor version number after parsing the version string
     *
     * @access public
     * @return string The minor version number
     */
    public function GetMinorVersion()
    {
        return $this->MinorVersion;
    }

    /**
     * Returns the build number after parsing the version string
     *
     * @access public
     * @return string The build number
     */
    public function GetBuild()
    {
        return $this->MinorVersion;
    }

    /**
     * Returns the revision number after parsing the version string
     *
     * @access public
     * @return string The revision number
     */
    public function GetRevision()
    {
        return $this->Revision;
    }

    /**
     * Returns the alpha revision number after parsing the version string
     *
     * @access public
     * @return string The alpha revision number
     */
    public function GetAlphaRevision()
    {
        return $this->AlphaRevision;
    }

    /**
     * Returns the beta revision number after parsing the version string
     *
     * @access public
     * @return string The beta revision number
     */
    public function GetBetaRevision()
    {
        return $this->BetaRevision;
    }

    /**
     * Returns the patch level number after parsing the version string
     *
     * @access public
     * @return string The patch level number
     */
    public function GetPatchLevelRevision()
    {
        return $this->PatchLevelRevision;
    }

    /**
     * Returns the release candidate revision number after parsing the version string
     *
     * @access public
     * @return string The release candidate revision number
     */
    public function GetReleaseCandidateRevision()
    {
        return $this->ReleaseCandidateRevision;
    }

    /**
     * Returns the version string passed into the class or found through the
     * <code>phpversion()</code> function.
     *
     * @access public
     * @return string The version string
     */
    public function GetVersionString()
    {
        return $this->VersionString;
    }

    /**
     * @param int $current
     * @param int $other
     * @return int
     */
    private static function CompareNumbers($current, $other)
    {
        if (($current == -1) && ($other > -1))
            return -1;

        if (($current > -1) && ($other == -1))
            return 1;

        if (($current > -1) && ($other > -1)) {
            switch (true) {
                case ($current < $other):
                    return -1;

                case ($current > $other):
                    return 1;
            }
        }

        return 0;
    }

    /**
     * This function compares the version number associated with this class and a
     * version number that is passed in as an argument.  If the passed in version number
     * is newer than the version number associated with this class, 1 is returned, if
     * the passed in version number is older than the version number associated with this
     * class, -1 is returned, and if they are equal, 0 is returned.
     *
     * @access public
     * @param string $version The version to compare with the verions associated with this class.
     *
     * @return integer -1 if <code>$sVer</code> is older than the version associated
     * with this class, 1 if <code>$sVer</code> is newer than the version associated
     * with this class and 0 if the versions are equal.
     */
    public function Compare($version)
    {
        $cmpVer = new Version($version);

        if ($cmpVer->VersionString == $this->VersionString) {
            return 0;
        }

        $result = self::CompareNumbers($cmpVer->MajorVersion, $this->MajorVersion);
        if ($result != 0)
            return $result;

        $result = self::CompareNumbers($cmpVer->MinorVersion, $this->MinorVersion);
        if ($result != 0)
            return $result;

        $result = self::CompareNumbers($cmpVer->Build, $this->Build);
        if ($result != 0)
            return $result;

        $result = self::CompareNumbers($cmpVer->Revision, $this->Revision);
        if ($result != 0)
            return $result;

        if ($this->Subversioning) {
            if (($cmpVer->ReleaseCandidateRevision == -1) && ($this->ReleaseCandidateRevision > -1)) {
                // compare version is not release candidate, but this version is
                if ($cmpVer->BetaRevision > -1 || $cmpVer->AlphaRevision > -1)
                    return -1;
            } elseif (($cmpVer->ReleaseCandidateRevision > -1) && ($this->ReleaseCandidateRevision == -1)) {
                // this version is not release candidate, but compare version is
                if ($this->BetaRevision > -1 || $this->AlphaRevision > -1)
                    return 1;
            } elseif (($cmpVer->ReleaseCandidateRevision > -1) && ($this->ReleaseCandidateRevision > -1)) {
                switch (true) {

                    case ($cmpVer->ReleaseCandidateRevision < $this->ReleaseCandidateRevision):
                        return -1;

                    case ($cmpVer->ReleaseCandidateRevision > $this->ReleaseCandidateRevision):
                        return 1;
                }
            }

            if (($cmpVer->BetaRevision == -1) && ($this->BetaRevision > -1)) {
                // compare version is not beta, but this version is
                if ($cmpVer->AlphaRevision > -1)
                    return -1;
            } elseif (($cmpVer->BetaRevision > -1) && ($this->BetaRevision == -1)) {
                // this version is not beta, but compare version is
                if ($this->AlphaRevision > -1)
                    return 1;
            } elseif (($cmpVer->BetaRevision > -1) && ($this->BetaRevision > -1)) {
                switch (true) {

                    case ($cmpVer->BetaRevision < $this->BetaRevision):
                        return -1;

                    case ($cmpVer->BetaRevision > $this->BetaRevision):
                        return 1;
                }
            }

            if (($cmpVer->AlphaRevision == -1) && ($this->AlphaRevision > -1)) {
                // compare version is not alpha, but this version is
                return 1;
            } elseif (($cmpVer->AlphaRevision > -1) && ($this->AlphaRevision == -1)) {
                // this version is not alpha, but compare version is
                return -1;
            } elseif (($cmpVer->AlphaRevision > -1) && ($this->AlphaRevision > -1)) {
                switch (true) {

                    case ($cmpVer->AlphaRevision < $this->AlphaRevision):
                        return -1;

                    case ($cmpVer->AlphaRevision > $this->AlphaRevision):
                        return 1;
                }
            }


            if (($cmpVer->PatchLevelRevision == -1) && ($this->PatchLevelRevision > -1)) {
                return -1;
            } elseif (($cmpVer->PatchLevelRevision > -1) && ($this->PatchLevelRevision == -1)) {
                return 1;
            } elseif (($cmpVer->PatchLevelRevision > -1) && ($this->PatchLevelRevision > -1)) {
                switch (true) {

                    case ($cmpVer->PatchLevelRevision < $this->PatchLevelRevision):
                        return -1;

                    case ($cmpVer->PatchLevelRevision > $this->PatchLevelRevision):
                        return 1;
                }
            }
        }
        return 0;
    }

    function __toString()
    {
        return $this->VersionString;
    }
}


final class VersionException extends ExceptionBase
{

    function __construct($_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null)
    {
        parent::__construct($_message, $_code, $_innerException);
    }
}