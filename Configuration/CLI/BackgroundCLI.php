<?php

/* BACKGROUND CLI 1.0
   
   eric pecoraro _at_ shepard dot com - 2005-06-02
   Use at your own risk. No warranties expressed or implied.

   Include this file at the top of any script to run it in the background
   with no time limitations ... e.g., include('background_cli.php');
   
   The script that calls this file should not return output to the browser. 
*/
#  REQUIREMENTS - cURL and CLI
if (!function_exists( 'curl_setopt' ) or !function_exists( 'curl_setopt' )) {
    echo 'Requires cURL and CLI installations.';
    exit();
}

#  BUILD PATHS
$SCRIPT_NAME = $_SERVER['SCRIPT_NAME'];
$HTTP_HOST = $_SERVER['HTTP_HOST'];
$PATH_TRANSLATED = $_SERVER['PATH_TRANSLATED'];
$post = count($_POST) > 0;

$script = array_pop( explode( '/', $SCRIPT_NAME ) );
$script_dir = substr( $SCRIPT_NAME, 0, strlen( $SCRIPT_NAME ) - strlen( $script ) );
$scriptURL = 'http://' . $HTTP_HOST . $script_dir . "$script";
$curlURL = 'http://' . $HTTP_HOST . $script_dir . "$script?runscript=curl";
$runscript = isset( $_GET['runscript'] ) ? (string) $_GET['runscript'] : null;

#  Indicate that script is being called by CLI 
if (php_sapi_name() == 'cli') {
    $CLI = true;
}

#  Action if script is being called by cURL_prompt()
if ($runscript == 'curl') {
    $cmd = "/usr/local/bin/php " . $PATH_TRANSLATED; // server location of script to run
    exec( $cmd );
    exit();
}

#  USER INTERFACE
// User answer after submission.
if ($post) {
    cURL_prompt( $curlURL );
    echo '<div style="margin:25px;"><title>Background CLI</title>';
    echo 'O.K. If all goes well, <b>' . $script . '</b> is working hard in the background with no ';
    echo 'timeout limitations. <br><br><form action=' . $scriptURL . ' method=GET>';
    echo '<input type=submit value=" RESET BACKGROUND CLI "></form></div>';
    exit();
}
// Start screen.
if (!$CLI and !$runscript) {
    echo '<title>Background CLI</title><div style="margin:25px;">';
    echo '<form action=' . $scriptURL . ' method=POST>';
    echo 'Click to run <b>' . $script . '</b> from the PHP CLI command line, in the background.<br><br>';
    echo '<input type=hidden value=1 name=post>';
    echo '<input type=submit value=" RUN IN BACKGROUND "></form></div>';
    exit();
}

#  cURL URL PROMPT FUNCTION
function cURL_prompt( $url_path )
{
    ob_start(); // start output buffer
    $c = curl_init( $url_path );
    curl_setopt( $c, CURLOPT_TIMEOUT, 2 ); // drop connection after 2 seconds
    curl_exec( $c );
    curl_close( $c );
    ob_end_clean(); // discard output buffer
}