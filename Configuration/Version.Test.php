<?php

use AppStatic\Configuration\Version;

require_once __DIR__ . '/../Include.inc.php';

$version = new Version( "6.0.1.1304" );
echo $version->Compare("6.0.01.1304");