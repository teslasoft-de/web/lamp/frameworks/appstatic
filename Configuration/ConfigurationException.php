<?php

namespace AppStatic\Configuration;

use AppStatic\AppStaticException;

class ConfigurationException extends AppStaticException
{
    function __construct( $_message, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( "WebStatic configuration error. $_message", $_code, $_innerException );
    }
}
?>
