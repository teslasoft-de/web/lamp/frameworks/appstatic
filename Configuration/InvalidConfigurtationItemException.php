<?php

namespace AppStatic\Configuration;

final class InvalidConfigurtationItemException extends ConfigurationException
{
    public function __construct( $itemName )
    {
        $value = constant($itemName);
        $message = <<<EOT
Configuration item value is invalid.
Item Name: $itemName.
Item Value: '$value'
EOT;
        parent::__construct( $message, 0, null );
    }
}
?>
