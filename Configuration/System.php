<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: System.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Configuration;


/**
 * Class System
 * @package AppStatic\Configuration
 * @name System
 * @version 1.0
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
final class System {
    public static function IsOnline(){
        return !self::IsLocal();
    }

    public static function IsLocal()
    {
        if (!defined( 'APPSTATIC_LOCAL_SYSTEM_IP_RANGE' ))
            throw new MissingConfigurtationItemException( 'APPSTATIC_LOCAL_SYSTEM_IP_RANGE' );

        if (!preg_match( '/^(\d{1,3}\.*)+$/', APPSTATIC_LOCAL_SYSTEM_IP_RANGE ))
            throw new InvalidConfigurtationItemException( 'APPSTATIC_LOCAL_SYSTEM_IP_RANGE', APPSTATIC_LOCAL_SYSTEM_IP_RANGE );

        $register_globals = (bool) ini_get( 'register_gobals' );
        if ($register_globals)
            $ip = getenv( 'REMOTE_ADDR' );
        else if(isset ($_SERVER['REMOTE_ADDR']))
            $ip = $_SERVER['REMOTE_ADDR'];

        return !empty($ip) && preg_match( '/' . str_replace( '.', '\.', APPSTATIC_LOCAL_SYSTEM_IP_RANGE ) . '|127\.0\.0/', $ip );
    }

    /**
     * Returns a list with the installed php locales.
     *
     * @return array
     */
    public static function GetLocales()
    {
        ob_start();
        system('locale -a');
        $str = ob_get_contents();
        ob_end_clean();
        return explode("\\n", trim($str));
    }

    /**
     *
     */
    public static function DieRoflCopter()
    {
        die(<<<HTML
<pre>
ROFL:ROFL:LOL:ROFL:ROFL
       ____^____
 L   _/      [] \\
LOL==_           \\
 L    \\___________]
         I     I
       ----------/
</pre>
HTML
        );
    }
}