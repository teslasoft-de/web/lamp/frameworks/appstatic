<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 03.01.2014
 * File: Config.inc.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Configuration;

use AppStatic\Autoload\IncludePath;
use AppStatic\Core\PropertyBase;

require_once __DIR__ . '/../AppStatic.Config.inc.php';
require_once APPSTATIC_PATH . '/Autoload/Using.php';

/**
 * AppStatic main configuration class. This class enables autoload functionality for the document root (AppStatic parent directory)
 *
 * @package   \AppStatic\Configuration
 * @name Config
 * @version   1.0
 * @author    H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
final class Config extends PropertyBase
{
    private static $instance;

    /**
     * @return Config
     */
    public static function getInstance()
    {
        if (!self::$instance)
            self::$instance = new Config();

        return self::$instance;
    }

    /**
     *
     * @var IncludePath
     */
    private static $IncludePath;

    /**
     *
     * @return IncludePath
     */
    public static function getIncludePath()
    {
        return static::$IncludePath;
    }

    /**
     * Initializes the Config instance and sets the config include path to the AppStatic parent (document root).
     */
    public function __construct()
    {
        static::$IncludePath = new IncludePath(dirname(APPSTATIC_PATH));
        date_default_timezone_set(APPSTATIC_DEFAILT_TIMEZONE);
    }
}

Config::getInstance();