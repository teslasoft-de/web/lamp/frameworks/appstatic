<?php

namespace AppStatic\Configuration;

final class MissingConfigurtationItemException extends ConfigurationException
{
    public function __construct( $itemName )
    {
        parent::__construct( "Missing required configuration item '$itemName'.", 0, null );
    }
}
?>
