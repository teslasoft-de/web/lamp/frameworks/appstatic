<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 13.08.2014
 * File: myBrowserLanguage.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 * */

namespace AppStatic\Localization;

use AppStatic\Web\HttpUtility;

require_once '../Include.inc.php';

echo filter_input( INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE' ) . '<br/>';
echoArray(HttpUtility::GetHttpAcceptLanguages());

function echoArray( array $array )
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}
