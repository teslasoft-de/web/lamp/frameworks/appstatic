<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: PredefinedVariablesValidator.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Web\Security;

use AppStatic\Core\ExceptionBase;

define( 'MAGIC_QUOTES', (function_exists( "get_magic_quotes_gpc" ) && get_magic_quotes_gpc()) || (ini_get( 'magic_quotes_sybase' ) && (strtolower( ini_get( 'magic_quotes_sybase' ) ) != "off")) );

/**
 * @package AppStatic
 * @name PredefinedVariablesValidator
 * @version 1.0 (2009-01-01)
 *
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow
 *
 */
final class PredefinedVariablesValidator
{

    /**
     * Indicates if the specified variables existing in the predefined GET array.
     *
     * @param string $varName
     * @param boolean $throwException
     * @return boolean
     * @throws PredefinedVariablesException
     */
    public static function ValidateGET($varName, $throwException = false)
    {
        if ($varName == null)
            throw new PredefinedVariablesException("No var names specified.");

        $args = func_get_args();
        return self::Validate("GET", $_GET, $args, $throwException);
    }

    /**
     * Indicates if the specified variables existing in the predefined POST array.
     *
     * @param string $varName
     * @param boolean $throwException
     * @return boolean
     * @throws PredefinedVariablesException
     */
    public static function ValidatePOST($varName, $throwException = false)
    {
        if ($varName == null)
            throw new PredefinedVariablesException("No var names specified.");

        $args = func_get_args();
        return self::Validate("POST", $_POST, $args, $throwException);
    }

    /**
     * Indicates if the specified variables existing in the predefined COOKIE array.
     *
     * @param string $varName
     * @param boolean $throwException
     * @return boolean
     * @throws PredefinedVariablesException
     */
    public static function ValidateCOOKIE($varName, $throwException = false)
    {
        if ($varName == null)
            throw new PredefinedVariablesException("No var names specified.");

        $args = func_get_args();
        return self::Validate("COOKIE", $_COOKIE, $args, $throwException);
    }

    /**
     * Determines if the spezified varibles existing in the specified predefined array.
     *
     * @param string $preDefVarsName
     * @param array $preDefVars
     * @param array $variables
     * @param bool $throwException
     * @return bool
     * @throws PredefinedVariablesException
     */
    private static function Validate($preDefVarsName, &$preDefVars, $variables,
        /** @noinspection PhpUnusedParameterInspection */ $throwException = false)
    {
        if (!defined('XSS_CHECK_' . $preDefVarsName)) {
            HttpRequestSecurity::XSSCheck($preDefVars);
            define('XSS_CHECK_' . $preDefVarsName, true);
        }

        $lastParamIndex = count($variables) - 1;
        if (is_bool($variables[$lastParamIndex])) {
            $throwException = $variables[$lastParamIndex];
            array_pop($variables);
        } else
            $throwException = false;

        if (!is_array($preDefVars))
            throw new PredefinedVariablesException("No $preDefVarsName paramter sepcified.");
        self::StripMagigSlashes();
        foreach ($variables as $var) {
            if (!isset($preDefVars[$var])) {
                if (!$throwException)
                    return false;
                else
                    throw new PredefinedVariablesException("$preDefVarsName paramter $var is missing.");
            }
        }
        return true;
    }

    public static function StripMagigSlashes()
    {
        if (!MAGIC_QUOTES || defined('MAGIC_QUOTES_STRIPPED'))
            return;

        HttpRequestSecurity::StripSlashesDeep($_GET);
        HttpRequestSecurity::StripSlashesDeep($_POST);
        HttpRequestSecurity::StripSlashesDeep($_COOKIE);

        define('MAGIC_QUOTES_STRIPPED', true);
    }
}

final class PredefinedVariablesException extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}