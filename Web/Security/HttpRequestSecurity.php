<?php
/**
 * Created by PhpStorm.
 * User: Cosmo
 * Date: 08.05.2015
 * Time: 23:40
 */

namespace AppStatic\Web\Security;


use AppStatic\Data\MySql\MySqliConnection;
use AppStatic\Web\DomainName;
use AppStatic\Web\Mail\EmailAddress;

class HttpRequestSecurity {

    /**
     * Performs stripslashes recursively on the specified data which can also be an array of strings.
     * @param array|string $value
     * @return array|string The supplied value type.
     */
    public static function StripSlashesDeep(&$value)
    {
        if (!isset($value))
            return $value;
        $value = is_array($value) ? array_map('HttpRequestSecurity::StripSlashesDeep', $value) : stripslashes($value); //array(self, 'StripSlashesDeep') functioniert nicht
        return $value;

        //count( $value ) > 0 ? array_combine( array_map( 'StripSlashesDeep', array_keys( $value ) ), array_map( 'StripSlashesDeep', array_values( $value ) ) )
    }

    /**
     * Performs mysqli_real_escape_string on suspicious input data.
     * @param string $input
     * @return string
     * @throws \AppStatic\Data\MySql\MySqliConnectionException
     */
    public static function Escape(&$input)
    {
        //These characters are single quote ('), double quote ("), backslash (\) and NUL (the NULL byte).
        $pattern = array("\\'", '\"', '\\\\', '\0');
        $replace = array('', '', '', '');
        if (preg_match('/[\\\\\'"\0]/', str_replace($pattern, $replace, $input))) { //"/[\\\\'\"\\0]/"
            return mysqli_real_escape_string(MySqliConnection::GetInstance()->getMySQLi(), $input);
        } else {
            return $input;
        }
    }

    /**
     * Checks the specified predefined variables array for XSS attack intrusion code
     * and notifies the abuse host administration in that case by email.
     *
     * @param array $predefinedVariable
     * @example http://ha.ckers.org/xss.html
     */
    public static function XSSCheck(&$predefinedVariable)
    {
        $detected = false;

        foreach ($predefinedVariable as &$check_url) {
            if (is_array($check_url)) {
                self::XSSCheck($check_url);
                continue;
            }

            $xssRulesMap = array(
                '<[^>]*script*"?[^>]*>',
                '<[^>]*object*"?[^>]*>',
                '<[^>]*iframe*"?[^>]*>',
                '<[^>]*applet*"?[^>]*>',
                '<[^>]*meta*"?[^>]*>',
                '<[^>]*style*"?[^>]*>',
                '<[^>]*form*"?[^>]*>',
                '\([^>]*"?[^)]*\)',
                '"');

            foreach ($xssRulesMap as $xssRule) {
                if (!preg_match("/$xssRule/i", $check_url))
                    continue;

                if (!$detected) {
                    // Report XSS attacks to the abuse host administration.
                    $detected = true;
                    $domainHost = DomainName::getCurrent()->ToString(1, 2);
                    $hostname = gethostname();
                    $hostDomain = new DomainName($hostname);
                    EmailAddress::SendEmail(
                        'abuse@' . $domainHost, $hostname, "{$hostDomain->ToString( 3 )}@$domainHost", null,
                        "XSS Attack - {$_SERVER['REMOTE_ADDR']} possibly tryed to attack",
                        print_r($predefinedVariable, true), null,
                        '<pre>' .
                        print_r($_SERVER, true) . '<br/>' .
                        print_r($_SESSION, true) . '<br/>' .
                        print_r($_REQUEST, true) . '<br/>' .
                        print_r($_ENV, true) . '</pre>');
                    //echo "xss detected. your ip {$_SERVER['REMOTE_ADDR']} has been tracked";
                }
                $check_url = strip_tags($check_url);

                if (is_string($check_url))
                    $check_url = utf8_htmlentities($check_url);
                break;
            }
        }
    }
}