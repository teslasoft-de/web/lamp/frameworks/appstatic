<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 06.01.2014
 * File: WebContentSecurity.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Web\Security;

define('X_FRAME_OPTIONS', 'X-Frame-Options');
/**
 * Adds X-Frame-Options to HTTP header, so that page cannot be shown in an iframe.
 */
define('X_FRAME_OPTIONS_DENY', 'deny');
/**
 * Adds X-Frame-Options to HTTP header, so that page can only be shown in an iframe of the same site.
 */
define('X_FRAME_OPTIONS_SAMEORIGIN', 'sameorigin');

define('X_CSP', 'X-Content-Security-Policy');

/**
 * Adds the Content-Security-Policy to the HTTP header.
 * JavaScript will be restricted to the same domain as the page itself.
 */
define('X_CSP_JAVASCRIPT_HOST', "allow 'self'");

/**
 * JavaScript is allowed only inside the HTML page (separate *.js files are not permitted).
 */
define('X_CSP_JAVASCRIPT_INLINE', '; options inline-script');

/**
 * Adds various X-Frame-Options and Content-Security-Policy to the HTTP header.
 *
 * @package AppStatic
 * @name WebContentSecurity
 * @version 0.1 (06.01.2014 01:29:30)
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 * @link http://w3c.github.io/webappsec/specs/content-security-policy/csp-specification.dev.html Content Security Policy 1.1
 */
class WebContentSecurity
{
    /**
     * Adds X-Frame-Options and Content-Security-Policy to the HTTP header for iFrames
     * and JavaScript source restrictions.
     *
     * @param string $xFrameOption One of the X_FRAME_OPTIONS_* constants
     * @param string $cspOption One of the X_CSP* constants
     */
    public static function DefendCrossSiteScrypting($xFrameOption = X_FRAME_OPTIONS_DENY, $cspOption = '')
    {
        header(X_FRAME_OPTIONS . ": $xFrameOption");
        header(X_CSP . ': ' . X_CSP_JAVASCRIPT_HOST . $cspOption);
    }
}