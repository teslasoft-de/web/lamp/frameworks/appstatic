<?php
/**
 * Created by PhpStorm.
 * User: Cosmo
 * Date: 20.04.2015
 * Time: 02:12
 */
namespace AppStatic\Web;
use AppStatic\Core;
use AppStatic\Core\ExceptionBase;
use datetime;
use Exception;
use AppStatic\Data\MySql\StoredProcedure;
use AppStatic\Web\Security\PredefinedVariablesValidator;

define( 'SESSION_TIMEOUT', 0xE10 ); //3600
define( 'SESSION_STATE_UNKNOWN', 0x0 );
define( 'SESSION_STATE_VALID', 0x1 );
define( 'SESSION_STATE_IP_CHANGED', 0x2 );
define( 'SESSION_STATE_TIMED_OUT', 0x4 );
define( 'SESSION_STATE_ABUSED', 0x8 );

/**
 * @package Treiber Studio Service
 * @name Session
 * @version  1.0 (2009-01-01)
 *
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow
 *
 * @property string SID
 */
final class Session extends Core\PropertyBase
{
    static $session;

    /**
     * @return Session
     */
    public static function getInstance()
    {
        if (!isset(self::$session)) {

            $sid = null;
            if (PredefinedVariablesValidator::ValidateGET('sid'))
                $sid = $_GET['sid'];
            elseif (PredefinedVariablesValidator::ValidatePOST('abe'))
                $sid = $_POST['abe'];
            elseif (PredefinedVariablesValidator::ValidateCOOKIE('sid'))
                $sid = $_COOKIE['sid'];
            else {
                global $cb;
                if (!empty($cb))
                    $sid = $cb->sid;
            }
            self::$session = new Session($sid);
        }
        return self::$session;
    }

    protected $SID;
    protected $UserID;
    protected $IP;
    protected $NewIP;
    protected $TimeStamp;

    protected $State;

    /**
     * Enter description here...
     *
     * @return string
     */
    public function getSID()
    {
        return $this->SID;
    }

    /**
     * Enter description here...
     *
     * @return integer
     */
    public function getUserID()
    {
        return $this->UserID;
    }

    /**
     * Enter description here...
     *
     * @return string
     */
    public function getIP()
    {
        return $this->IP;
    }

    /**
     * Enter description here...
     *
     * @return string
     */
    public function getNewIP()
    {
        return $this->NewIP;
    }

    /**
     * Enter description here...
     *
     * @return datetime timestamp
     */
    public function getTimeStamp()
    {
        return $this->TimeStamp;
    }

    /**
     * Enter description here...
     *
     * @return boolean
     */
    public function Exists()
    {
        return ($this->State & SESSION_STATE_ABUSED) == 0;
    }

    /**
     * Enter description here...
     *
     * @return boolean
     */
    public function IsValid()
    {
        return ($this->State & SESSION_STATE_VALID) == SESSION_STATE_VALID;
    }

    /**
     * Enter description here...
     *
     * @return boolean
     */
    public function TimedOut()
    {
        return ($this->State & SESSION_STATE_TIMED_OUT) == SESSION_STATE_TIMED_OUT;
    }

    /**
     * Enter description here...
     *
     * @return integer SESSION_STATE
     */
    public function State()
    {
        return $this->State;
    }

    /**
     * @param $sid
     * @throws SessionException
     */
    public function __construct($sid)
    {
        if ($sid == null || $sid == '')
            $exists = false;
        else {
            try {
                $procedure = new StoredProcedure('Web_QuerySession');
                $procedure->Prepare($sid);
                $procedure->Execute();
                $procedure->BindResult(array(
                    &$this->UserID,
                    &$this->IP,
                    &$this->TimeStamp));
                $exists = $procedure->Fetch(true);
                $procedure->Close();
                unset($procedure);
            } catch (Exception $e) {
                throw new SessionException('Fehler beim Abrufen der Session.', 0, $e);
            }
        }

        $this->SID = $sid;
        if ($this->IP != $_SERVER['REMOTE_ADDR']) {
            $this->NewIP = $_SERVER['REMOTE_ADDR'];
            $this->State |= SESSION_STATE_IP_CHANGED;
        }

        if (!$exists) {
            $this->State |= SESSION_STATE_ABUSED;
            $this->TimeStamp = strtotime('0000-00-00 00:00:00');
            return;
        }

        if ($this->TimeStamp == null || $this->TimeStamp == '0000-00-00 00:00:00' || ((strtotime($this->TimeStamp) + SESSION_TIMEOUT) < time())) { //ELSE mysql_query("UPDATE $tabelle SET LastTime = NOW() WHERE SID = '$sid'");
            $this->State |= SESSION_STATE_TIMED_OUT;
            return;
        }
        if ($this->State == null)
            $this->State |= SESSION_STATE_VALID;
    }

    public function Validate($onlyTimeout = false)
    {
        if (!$this->IsValid() && !isset($_GET['sid']))
            die('Session timed out.');

        if ($onlyTimeout) {
            if (!self::CheckTempSession($_GET['sid']))
                throw new SessionException('Session timed out.', SESSION_STATE_TIMED_OUT);
        } elseif (!$this->IsValid())
            throw new SessionException('Session timed out.', $this->State());
    }


    /*public function Validate( $onlyTimeout = false )
    {
        if (!$this->Valid() && !isset( $_GET[ 'sid' ] )){
            //ArrayUtility::MovePage( 401, "http://{$_SERVER['HTTP_HOST']}/login.cgi" );
            $ex = new SessionException( 'Session timed out.', SESSION_STATE_TIMED_OUT );
            die( $ex->GenerateMessage() );
            ArrayUtility::DieRoflCopter();
        }


        if ($onlyTimeout) {
            if (!self::CheckTempSession( $_GET[ 'sid' ] )){
                //ArrayUtility::MovePage( 401, "http://{$_SERVER['HTTP_HOST']}/login.cgi" );
                throw new SessionException( 'Session timed out.', SESSION_STATE_TIMED_OUT );
            }
        }
        elseif (!$this->Valid())
            //ArrayUtility::MovePage( 401, "http://{$_SERVER['HTTP_HOST']}/login.cgi" );
            throw new SessionException( 'Session timed out.', $this->State() );
    }*/


    public function Update()
    {
        if (!$this->IsValid())
            throw new SessionException('Can not update an invalid session.', $this->State);

        try {
            $procedure = new StoredProcedure('Web_UpdateSession');
            $procedure->Prepare($this->SID);
            $procedure->Execute();
            $procedure->Close();
            unset($procedure);
        } catch (Exception $e) {
            throw new SessionException('Fehler beim Aktualisieren der Session.', 0, $e);
        }
    }

    public static function CheckTempSession($sid)
    {
        $modify = null;
        try {
            $procedure = new StoredProcedure('Web_QueryTempSession');
            $procedure->Prepare($sid);
            $procedure->Execute();
            $procedure->BindResult(array(&$modify));
            $procedure->Fetch(true);
            $procedure->Close();
            unset($procedure);
        } catch (Exception $e) {
            throw new SessionException('Fehler beim Aktualisieren der Session.', 0, $e);
        }

        return ((strtotime($modify) + SESSION_TIMEOUT) >= time());
    }
}

final class SessionException extends ExceptionBase
{
    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}