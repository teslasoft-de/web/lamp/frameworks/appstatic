<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 28.06.2014
 * File: HttpCacheControl.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Web\Http;

/**
 * Handles the CacheControl for HTTP sessions. If PHP session enabled
 * the PHP internal session_cache implementation and the users private
 * cache will be used.
 * 
 * @package AppStatic
 * @name HttpCacheControl
 * @version 0.1 (28.06.2014 14:50:43)
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class HttpCacheControl
{
    /**
     * 
     * @param integer $lastModifiedTimestamp
     * @param integer $maxAge Max age in minutes.
     */
    public static function Init( $lastModifiedTimestamp, $maxAge )
    {
        if (self::ModifiedSince( $lastModifiedTimestamp ))
            self::LastModified( $lastModifiedTimestamp, $maxAge );
        else
            self::NotModified( $maxAge );
    }

    private static function ModifiedSince( $lastModifiedTimestamp )
    {
        $allHeaders = apache_request_headers();
        
        if (array_key_exists( "If-Modified-Since", $allHeaders )) {
            $gmtSinceDate = $allHeaders[ "If-Modified-Since" ];
            $sinceTimestamp = strtotime( $gmtSinceDate );

            // Can the browser get it from the cache?
            if ($sinceTimestamp != false && $lastModifiedTimestamp <= $sinceTimestamp)
                return false;
        }
        return true;
    }

    private static function NotModified( $maxAge )
    {
        // Set headers
        http_response_code( 304 );
        header( "Cache-Control: no-transform, public, max-age=$maxAge, s-max-age=" . ($maxAge *2), true );
        die();
    }

    private static function LastModified( $lastModifiedTimestamp, $maxAge )
    {
        // Enable private content caching for php sessions.
        if(session_status() == PHP_SESSION_ACTIVE){
            session_cache_expire( $maxAge );
            session_cache_limiter( 'private' );
        }
        // Enable public content caching by default.
        else{
            // Set up dates
            $date = gmdate( 'D, j M Y H:i:s', $lastModifiedTimestamp ) . ' GMT';
            $expiresDate = gmdate( 'D, j M Y H:i:s', time() + $maxAge * 60 ) . ' GMT';
        
            // Adding expires header for compatibility to older user agents.
            // (session_cache also still implements this header.)
            header( "Expires: $expiresDate" );
            // no-transform - Prevent CDNs from converting image formats and documents
            // for performance reasons.
            // public - Store this request in users shared cache.
            header( "Cache-Control: public, no-transform, max-age=$maxAge, s-max-age=" . ($maxAge *2), true );
            header( "Last-Modified: $date", true );
        }
    }

}
