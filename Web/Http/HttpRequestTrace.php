<?php

namespace AppStatic\Web\Http;

/**
 * Access the HTTP Request
 * @example
  1. $http_request = new HttpRequestTrace();
  2. $content_type = $http_request->getHeader('Content-Type');
  3. $content = $http_request->getBody();
  4.
  5. if ($content_type == 'text/json') {
  6.   // parse as JSON
  7.   $json = new JSON_READER();
  8.   $req_obj = $json->parse($content);
  9. } elseif ($conten_type == 'text/xml') {
  10.   // parse as XML
  11.   $xml = new XML_READER();
  12.   $req_obj = $xml->parse($content);
  13. } else {
  14.   // error
  15.   trigger_error('Content-Type unknown.. etc');
  16. }
 */
final class HttpRequestTrace extends HttpTrace
{
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        parent::Refresh();
    }

    protected function _GetHeaders()
    {
        return apache_request_headers();
    }

    protected function _GetBody()
    {
        return $this->Method == "POST" ? $GLOBALS['HTTP_RAW_POST_DATA'] : @file_get_contents( 'php://input' );
    }

    protected function _PrependHeader()
    {
        return $this->Method . ' ' . $this->Url . ' ' . $this->Protocol;
    }
}

/**
 * Example Usage
 * Echos the HTTP Request back to the client/browser
 *
$http_request = new HttpRequestTrace(true);

$resp = $http_request->getRaw();
echo '<pre>';
echo nl2br( $resp );
echo nl2br( print_r( apache_request_headers(), true ) );
echo '</pre>';
header*/
?>
