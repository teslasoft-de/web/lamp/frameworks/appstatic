<?php

namespace AppStatic\Web\Http;

/*
 * To change this template, choose ArrayUtility | Templates
 * and open the template in the editor.
 */
use AppStatic\Core;
use AppStatic\Core\String;
use AppStatic\Web\HttpUtility;

/**
 * Description of HttpTrace
 *
 * @author Christian Kusmanow
 */
abstract class HttpTrace extends Core\PropertyBase
{
    protected $Url;
    protected $Protocol;
    protected $Method;
    protected $RequestMethod;
    private $Headers;
    /** additional HTTP headers not prefixed with HTTP_ in $_SERVER superglobal */
    protected $ContentHeaders = array( 'CONTENT_TYPE', 'CONTENT_LENGTH' );
    private $AddContentHeaders;
    protected $Body;
    private $Raw;

    /**
     * Retrieve HTTP Protocol
     * @return string
     */
    public function getProtocol()
    {
        return $this->Protocol;
    }

    /**
     * Retrieve HTTP Method
     */
    public function getMethod()
    {
        return $this->Method;
    }

    /**
     * Retrieve HTTP Request Method
     * @return string
     */
    public function getRequestMethod()
    {
        return $this->RequestMethod;
    }

    /**
     * Retrieve all HTTP Headers
     * @return array HTTP Headers
     */
    public function getHeaders()
    {
        return $this->Headers;
    }

    /**
     * Retrieve HTTP Body
     */
    public function getBody()
    {
        return $this->Body;
    }

    /**
     * Construtor
     * Retrieve HTTP Body
     * @param bool $addContentHeaders
     */
    function __construct( $addContentHeaders = false )
    {
        $this->Url = HttpUtility::CurrentPageURL();

        if (isset( $_SERVER[ 'HTTP_METHOD' ] )) {
            $this->Method = $_SERVER[ 'HTTP_METHOD' ];
            unset( $_SERVER[ 'HTTP_METHOD' ] );
        }
        else
            $this->Method = isset( $_SERVER[ 'REQUEST_METHOD' ] ) ? $_SERVER[ 'REQUEST_METHOD' ] : null;

        $this->Protocol = isset( $_SERVER[ 'SERVER_PROTOCOL' ] ) ? $_SERVER[ 'SERVER_PROTOCOL' ] : null;
        $this->RequestMethod = isset( $_SERVER[ 'REQUEST_METHOD' ] ) ? $_SERVER[ 'REQUEST_METHOD' ] : null;

        $this->AddContentHeaders = $addContentHeaders;
    }

    protected abstract function _GetHeaders();

    protected abstract function _GetBody();

    /**
     * Retrieve the HTTP request/response headers
     */
    protected function RetrieveHeaders()
    {
        $this->Headers = array( );
        foreach ($this->_GetHeaders() as $name => $value)
            $this->Headers[ $name ] = $value;

        if ($this->AddContentHeaders) {
            foreach ($this->ContentHeaders as $header) {
                if (!isset( $this->Headers[$header] ) && isset( $_SERVER[ $header ] )) {
                    $words = preg_split( '/_/', $header );
                    $words = array_map( 'ArrayUtility::FormatToUpperLowerCase', $words );
                    $name = implode( '-', $words );

                    $this->Headers[ $name ] = $_SERVER[ $header ];
                }
            }
        }
    }

    public function Refresh()
    {
        $this->Body = $this->_GetBody();
        $this->RetrieveHeaders();
    }

    protected function _PrependHeader()
    {
        return '';
    }

    /**
     * Returns the Raw HTTP Request
     * @param boolean $refresh ReBuild the Raw HTTP Request
     * @return string
     */
    public function getRaw( $refresh = false )
    {
        if (!isset( $this->Raw ) || $refresh) {
            if(!isset($this->Body) || $refresh)
                $this->Refresh();

            $raw = $this->_PrependHeader();
            if (strlen( $raw ) > 0)
                $raw = trim( $raw ) . "\n";

            foreach ($this->getHeaders() as $name => $value)
                $raw .= "$name: $value\n";

            $raw .= "\n{$this->Body}";

            $this->Raw = $raw;
        }
        return $this->Raw; // return cached
    }

    public function getSize()
    {
        return String::GetBytesCount($this->getRaw());
        //return strlen( $this->getRaw() );
    }
}

?>
