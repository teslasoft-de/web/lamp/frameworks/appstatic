<?php

namespace AppStatic\Web\Http;
use AppStatic\Collections\ArrayUtility;
use AppStatic\Web\HttpUtility;

/**
 * Access the HTTP Request
 * @example
  1. $http_request = new HttpRequestTrace();
  2. $content_type = $http_request->getHeader('Content-Type');
  3. $content = $http_request->getBody();
  4.
  5. if ($content_type == 'text/json') {
  6.   // parse as JSON
  7.   $json = new JSON_READER();
  8.   $req_obj = $json->parse($content);
  9. } elseif ($conten_type == 'text/xml') {
  10.   // parse as XML
  11.   $xml = new XML_READER();
  12.   $req_obj = $xml->parse($content);
  13. } else {
  14.   // error
  15.   trigger_error('Content-Type unknown.. etc');
  16. }
 */
final class HttpResponseTrace extends HttpTrace
{
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        if(ArrayUtility::IndexOf('gzip', apache_request_headers()) > -1)
            ob_start('ob_gzhandler');
        else
            ob_start();
    }

    protected function _GetHeaders()
    {
        $headers = array(
            'Date' => gmstrftime("%a, %d %b %Y %H:%M:%S GMT", time() ),
            'Server' => $_SERVER['SERVER_SOFTWARE'] );
        return array_merge( $headers, apache_response_headers() );
    }

    protected function _GetBody()
    {
        $contents = ob_get_contents(); // retrieve all output thus far
        //header('Content-Lenght', ob_get_length() );
        //ob_flush();
        flush();
        return $contents;
    }

    protected function _PrependHeader()
    {
        return HttpUtility::HttpResponseCode();
    }
}