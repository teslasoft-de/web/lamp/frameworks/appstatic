<?php

namespace AppStatic\Web\Http;

error_reporting( E_STRICT | E_ALL );

class CurlHelper
{
    public static $userAgents = array( 
        'FireFox3' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.9) Gecko/2008052906 Firefox/3.0', 
        'GoogleBot' => 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 
        'IE7' => 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)', 
        'Netscape' => 'Mozilla/4.8 [en] (Windows NT 6.0; U)', 
        'Opera' => 'Opera/9.25 (Windows NT 6.0; U; en)' );
    public static $options = array( 
        CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; pl; rv:1.9) Gecko/2008052906 Firefox/3.0', 
        CURLOPT_AUTOREFERER => true, 
        CURLOPT_COOKIEFILE => '', 
        CURLOPT_FOLLOWLOCATION => true );
    
    private static $proxyServers = array();
    private static $proxyCount = 0;
    private static $currentProxyIndex = 0;
    
    public static function addProxyServer( $url )
    {
        self::$proxyServers[] = $url;
        ++self::$proxyCount;
    }
    
    public static function fetchContent( $url, $verbose = false )
    {
        if (($curl = curl_init( $url )) == false) {throw new \Exception( "curl_init error for url $url." );}
        
        if (self::$proxyCount > 0) {
            $proxy = self::$proxyServers[ self::$currentProxyIndex++ % self::$proxyCount ];
            curl_setopt( $curl, CURLOPT_PROXY, $proxy );
            if ($verbose === true) {
                echo "Reading $url [Proxy: $proxy] ... ";
            }
        }
        else if ($verbose === true) {
            echo "Reading $url ... ";
        }
        
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt_array( $curl, self::$options );
        
        $content = curl_exec( $curl );
        if ($content === false) {throw new \Exception( "curl_exec error for url $url." );}
        
        curl_close( $curl );
        if ($verbose === true) {
            echo "Done.\n";
        }
        
        $content = preg_replace( '#\n+#', ' ', $content );
        $content = preg_replace( '#\s+#', ' ', $content );
        
        return $content;
    }
    
    public static function downloadFile( $url, $fileName, $verbose = false )
    {
        if (($curl = curl_init( $url )) == false) {throw new \Exception( "curl_init error for url $url." );}
        
        if (self::$proxyCount > 0) {
            $proxy = self::$proxyServers[ self::$currentProxyIndex++ % self::$proxyCount ];
            curl_setopt( $curl, CURLOPT_PROXY, $proxy );
            if ($verbose === true) {
                echo "Downloading $url [Proxy: $proxy] ... ";
            }
        }
        else if ($verbose === true) {
            echo "Downloading $url ... ";
        }
        
        curl_setopt_array( $curl, self::$options );
        
        if (substr( $fileName, -1 ) == '/') {
            $targetDir = $fileName;
            $fileName = tempnam( sys_get_temp_dir(), 'c_' );
        }
        if (($fp = fopen( $fileName, "wb" )) === false) {throw new \Exception( "fopen error for filename $fileName" );}
        curl_setopt( $curl, CURLOPT_FILE, $fp );
        
        curl_setopt( $curl, CURLOPT_BINARYTRANSFER, true );
        if (curl_exec( $curl ) === false) {
            fclose( $fp );
            unlink( $fileName );
            throw new \Exception( "curl_exec error for url $url." );
        }
        elseif (isset( $targetDir )) {
            $eurl = curl_getinfo( $curl, CURLINFO_EFFECTIVE_URL );
            preg_match( '#^.*/(.+)$#', $eurl, $match );
            fclose( $fp );
            rename( $fileName, "$targetDir{$match[1]}" );
            $fileName = "$targetDir{$match[1]}";
        }
        else {
            fclose( $fp );
        }
        
        curl_close( $curl );
        if ($verbose === true) {
            echo "Done.\n";
        }
        return $fileName;
    }


    /**
     * Checks if the specified url is available for download.
     *
     * @param string $url
     * @param string $httpReferrer
     * @return bool
     */
    public static function CheckUrlIsAvailable( $url, $httpReferrer = null )
    {

        //TODO: Rename to VerifyUrl and update HardCare
        if (preg_match( '/^http/', $url )) {
            $ch = curl_init( $url );
            curl_setopt( $ch, CURLOPT_FRESH_CONNECT, 1 );
            curl_setopt( $ch, CURLOPT_REFERER, $httpReferrer );
            curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)' );
            curl_setopt( $ch, CURLOPT_HEADER, true );
            curl_setopt( $ch, CURLOPT_NOBODY, true );
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, false );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $status = array( );
            $response = @curl_exec( $ch );
            preg_match( '/HTTP\/.* ([0-9]+) .*/', $response, $status );
            return $status[1] == 200 && preg_match( '/Content-Type: application/i', $response );
        }

        $timeout = ini_get( 'default_socket_timeout' );
        ini_set( 'default_socket_timeout', 15 );
        $sock = @fopen( $url, 'r' );
        ini_set( 'default_socket_timeout', $timeout );
        $success = $sock !== false;
        if ($success) {
            $success = fread( $sock, 1 ) !== false;

            if ($success) {
                $success = false;

                $meta = stream_get_meta_data( $sock );

                $headers = $meta['wrapper_data']/* ['headers'] */;

                if (preg_match( '/^220/', $headers[0] ))
                    $success = true;
                else if (preg_match( '/200 OK/i', $headers[0] )) {
                    foreach ($headers as $header) {
                        if (preg_match( '/Content-Type: application/i', $header )) {
                            $success = true;
                            break;
                        }
                    }
                }
                else if ($meta['wrapper_type'] == 'ftp' || $meta['mode'] == 'r+')
                    $success = true;
            }
            fclose( $sock );
        }
        return $success;
    }
}
?>