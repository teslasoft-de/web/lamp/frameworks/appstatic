<?php
/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 04.05.2015
 * File: HtmlUtility.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Web;

use AppStatic\Data\XmlUtility;
use DOMNode;

/**
 * Class HtmlUtility
 * @package   AppStatic\Web
 * @name HtmlUtility
 * @version   1.0
 * @author    H. Christian Kusmanow
 * @copyright © 2015 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
final class HtmlUtility
{

    public static function br2nl( $string )
    {
        return preg_replace( '~<br\s*/?\s*>~i', chr( 13 ) . chr( 10 ), $string );
    }

    public static function StripWhitespace( $html )
    {
        return preg_replace( '~(?<=>)\s+|\s+(?=<)~', '', $html );
    }

    public static function HtmlEntitiesForNonHTML( $string )
    {
        if (!preg_match( "//<([^>]+)>/i", $string ))
            return utf8_htmlentities( $string );

        return $string;
    }

    public static function HasAttributeIdentifier( DOMNode $node, $value, array $hasIdentifiers = ['id', 'role'], array $containsIdentifiers = ['class'] )
    {
        foreach ($hasIdentifiers as $identifier)
            if (XmlUtility::HasAttributeValue( $node, $identifier, $value ))
                return true;

        foreach ($containsIdentifiers as $identifier)
            if (XmlUtility::ContainsAttributeValue( $node, $identifier, $value ))
                return true;

        return false;
    }

    /**
     * Converts non-self-closing HTML tags to self-closing XHTML tags.
     *
     * @param string $html
     *
     * @return string
     */
    public static function ConvertHtmlToXHtml( $html )
    {
        return preg_replace( <<<EOT
/<(img|hr|br|link|input)
((\s+[\w-]+(\s*=\s*(\"[^\"]*\"|'[^']*'|\w+))?)*)
\s*>/x
EOT
            , '<$1$2/>', $html );
    }
}