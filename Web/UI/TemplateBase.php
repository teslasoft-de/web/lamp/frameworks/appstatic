<?php

namespace AppStatic\Web\UI;

use AppStatic\Collections\ArrayUtility;
use AppStatic\Core;
use AppStatic\Core\String;
use AppStatic\Data\MySql\StoredProcedure;
use AppStatic\IO\Path;
use AppStatic\Web\Security\PredefinedVariablesValidator;
use Exception;

require_once APPSTATIC_PATH . '/Globalization/UTF8.inc.php';

abstract class TemplateBase extends Core\PropertyBase
{
    protected $Template;
    
    protected function __construct( $fileName = null )
    {
        if(empty($fileName))
            return;

        try {
            /** @noinspection PhpUndefinedConstantInspection */
            $this->Template = TemplateUtility::ReadTemplateFile(Path::Combine(TEMPLATE_BASE_PATH, $fileName));
        }
        catch(Exception $ex) {
            throw new TemplateBaseException( "Das Template konnte nicht geladen werden.", 0, $ex );
        }
    }

    protected $ErrorList = array();

    public function getErrorList()
    {
        return $this->ErrorList;
    }

    
    public function SetError( array $errorList )
    {
        $this->ErrorList = $errorList;
    }
    
    static $Spacer = '<div class="spacer"><!-- NoContent --></div>';
    static $Evidence = 'class="evidence" title="[TITLE]" ';
    
    protected function CreateEvidence( $evidenceName )
    {
        $evidence = '';
        if (isset( $this->ErrorList[ $evidenceName ] ))
            $evidence = str_replace( '[TITLE]', utf8_htmlentities( $this->ErrorList[ $evidenceName ] ), self::$Evidence );
        return $evidence;
    }
    
    protected function Replace( $replacements, $evidences = null )
    {
        $string = str_replace( array_keys( $replacements ), array_values( $replacements ), $this->Template );
        
        if ($evidences === null)
            return $string;
        
        foreach( array_keys( $this->ErrorList ) as $key ) {
            if (ArrayUtility::IndexOf($key, $evidences) == -1)
                unset( $this->ErrorList[ $key ] );
        }
        
        $error = '';
        $errorKeys = array_keys( $this->ErrorList );
        foreach( $evidences as $evidence ) {
            if (ArrayUtility::IndexOf($evidence, $errorKeys) > -1) {
                
                $errorCount = count( $this->ErrorList );
                if ($errorCount > 1)
                    $error = 'Bitte überprüfen Sie die rot markierten Felder.';
                elseif ($errorCount == 1) {
                    $values = array_values( $this->ErrorList );
                    $error = $values[ 0 ];
                }
                break;
            }
        }
        
        $replacements = array( '[ERROR]' => utf8_htmlentities( $error ) );
        foreach( $evidences as $evidence )
            $replacements[ $evidence ] = $this->CreateEvidence( $evidence );
        
        return str_replace( array_keys( $replacements ), array_values( $replacements ), $string );
    }
    
    public static $POSTValueCache = array();

    /**
     * Enter description here...
     *
     * @param string $name
     * @param bool $formatWordSpecialChars
     * @return mixed
     * @throws \AppStatic\Web\Security\PredefinedVariablesException
     */
    public static function getPOSTValue( $name, $formatWordSpecialChars = true )
    {
        if (isset( self::$POSTValueCache[ $name ] ))
            return self::$POSTValueCache[ $name ];
        if (PredefinedVariablesValidator::ValidatePOST( $name, false )) {
            $value = $_POST[ $name ];
            if (is_string( $value )) {
                $value = preg_replace( '/[ \t]+/', ' ', trim( $value ) );
                
                if($formatWordSpecialChars && !empty( $value ))
                    $value = String::FormatWordSpecialChars($value);
                    
                self::$POSTValueCache[ $name ] = $value;
            }
            return $value;
        }
        return null;
    }
    
    public static function ParseValue( &$value, $formatWordSpecialChars = true )
    {
        if (!is_string( $value ))
            return;
        $value = preg_replace( '/[ \t]+/', ' ', trim( $value ) );

        if ($formatWordSpecialChars && !empty( $value ))
            $value = String::FormatWordSpecialChars($value);

        $value = convToUtf8( $value );
    }
    
    public static function IsEmpty( $value )
    {
        return empty( $value );
    }
    
    public static function IsBadFormValue( $text )
    {
        return !self::IsVowel( $text ) || strlen($text) < 2;
    }
    
    public static function IsVowel( $text )
    {
        if (empty( $text ))
            return false;
        $text = convToUtf8( strtolower( $text ) );
        $strlen = mb_strlen( $text, 'UTF-8' );
        for( $i = 0; $i < $strlen; $i++ ) {
            $txt = mb_substr($text, $i, 1 );
            if (mb_strpos( 'aeiouäöü', $txt ) !== false)
                return true;
        }
        return false;
    }
    
    public static function GenerateOptionList( array $list, $useListKeys = false, $defaultValue = null )
    {
        $options = '';
        if ($defaultValue == null)
            $options .= '<option selected="selected" value=""></option>';
        
        foreach( $list as $key => $value ) {
            
            $option = '<option ';
            if ($value == $defaultValue)
                $option .= 'selected="selected" ';
            $option .= 'value="' . ($useListKeys ? $key : $value) . '">' . $value . '</option>';
            
            $options .= $option;
        }
        
        return $options;
    }
    
    public static function Checkresidence( $zipCode, &$location, &$wrongZipcode, &$wrongLocation )
    {
        $resisdenceExists = false;
        $wrongZipcode = false;
        $wrongLocation = $location == 'Stadt' || $location == 'Markt';
        if ($wrongLocation)
            return $resisdenceExists;
        $utf8 = mb_detect_encoding( $location, 'ISO-8859-1,' . ENCODING_UTF_8 ) == ENCODING_UTF_8;
        if(!$utf8)
            $location = convToUtf8( $location );
        static $umlauts = array( 'ä', 'ü', 'ö', 'ß' );
        static $diagraphs = array( 'ae', 'ue', 'oe', 'ss' );
        $tempLocation = str_replace( $umlauts, $diagraphs, strtolower( $location ) );
        
        try {
            $dbLocation = null;
            $procedure = new StoredProcedure( 'GetZipCodeLocation', APPSTATIC_DATABASE );
            $procedure->Prepare( $zipCode );
            $procedure->Execute();
            $procedure->BindResult( array( &$dbLocation ) );
            
            $wrongZipcode = !$procedure->Fetch( true );
            if(!$wrongZipcode){
                do {
                    $dbLocation = convToUtf8( trim( $dbLocation ) );
                    $tempDBLocation = str_replace( $umlauts, $diagraphs, strtolower( $dbLocation ) );
                    
                    if (@strpos( $tempDBLocation, $tempLocation ) !== false) {
                        
                        $commaIndex = strpos( $dbLocation, ',' );
                        if ($commaIndex !== false)
                            $dbLocation = substr( $dbLocation, 0, $commaIndex );
                        
                        $location = $dbLocation;
                        $resisdenceExists = true;
                        break;
                    }
                }
                while ($procedure->Fetch( true ));
            }
            unset( $procedure );
            
            $wrongLocation = !$resisdenceExists && !$wrongZipcode;
        }
        catch(Exception $ex) {
            unset( $procedure );
            throw $ex;
        }
        if(!$utf8)
            $location = convToLatin1( $location );
        return $resisdenceExists;
    }
}


final class TemplateBaseException extends Core\ExceptionBase
{
    function __construct( $_message = null, $_code = E_USER_ERROR, Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}
?>