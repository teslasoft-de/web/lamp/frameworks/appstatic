<?php
/**
 * Created by PhpStorm.
 * User: Cosmo
 * Date: 20.04.2015
 * Time: 16:12
 */

namespace AppStatic\Web\UI;

class TemplateUtility
{
    /**
     * Reads the specified template file and returnes the entire content string.
     *
     * @param string $fileName
     * @param bool   $escapeForEval
     *
     * @return string
     * @throws \Exception
     */
    public static function ReadTemplateFile($fileName, $escapeForEval = false)
    {
        if (!file_exists($fileName))
            trigger_error("Template file '{$fileName}' could not be found!", E_USER_ERROR);

        $file = fopen($fileName, 'r');
        if ($file == false)
            trigger_error("Failed to open template file $fileName.", E_USER_ERROR);

        $content = null;
        if (filesize($fileName) > 0)
            $content = fread($file, filesize($fileName));
        fclose($file);
        if ($escapeForEval)
            $content = str_replace('"', '\"', $content);

        return $content;
    }
}