<?php

use AppStatic\Configuration\Config;
use AppStatic\IO\Path;

require_once __DIR__ . '/../../Include.inc.php';

Config::getIncludePath()->AddPath( APPSTATIC_INC_RMAIL );

$mail = new Rmail();
$mail->setFrom('HCK <christian.kusmanow@teslasoft.de>');
$mail->setReturnPath('christian.kusmanow@teslasoft.de');
$mail->setHeader( 'X-Mailer', 'Outlooker' );
$mail->setSubject('Test email');
$mail->setPriority('high');
$mail->setText('Sample text');
$mail->setHTML('<b>Sample HTML</b> <img src="Teslasoft.png">', Path::Combine($_SERVER['DOCUMENT_ROOT'], 'img/') );
$result = $mail->send(array('hans.christian.kusmanow@gmail.com'), 'smtp');
$result = $result === true ? $result : $mail->getErrors();
echo '<pre>';
var_dump( $result );
echo ' ';
print_r( $result );
echo '</pre>';
