<?php

namespace AppStatic\Web\Mail;

use AppStatic\Configuration\Config;
use AppStatic\Core\ExceptionBase;
use AppStatic\Core\PropertyBase;
use AppStatic\Data\MySql\StoredProcedure;
use Exception;
use fileAttachment;
use Rmail;
use stringAttachment;

class EmailAddress extends PropertyBase
{
    public static function SendEmail($email, $mailerName, $mailerAddress, $bccAddress = null, $subject, $emailTXT,
                                     $emailHTM = null, $imageDirectory = null, $attachments = null, $dispositionNotification = false,
                                     $priority = 'normal', $unsubscribeEmail = null, $unsubscribeUrl = null)
    {
        Config::getIncludePath()->AddPath(APPSTATIC_INC_RMAIL);

        $mail = new Rmail();
        $mail->setPriority($priority);

        // Set email adresses
        $mail->setFrom("$mailerName <$mailerAddress>");

        $mail->setReturnPath($mailerAddress);

        if ($dispositionNotification)
            $mail->setReceipt("$mailerName <$mailerAddress>");

        if (!empty($bccAddress))
            $mail->setBcc($bccAddress);

        if (!empty($unsubscribeEmail) && !empty($unsubscribeUrl))
            $mail->setHeader('List-Unsubscribe', sprintf('<mailto:%s>, <%s>', $unsubscribeEmail, $unsubscribeUrl));

        // Set content
        $mail->setSubject(mb_detect_encoding($subject, 'UTF-8, ISO-8859-1') == 'UTF-8'
            ? utf8_decode($subject)
            : $subject);

        $mail->setText(mb_detect_encoding($emailTXT, 'UTF-8, ISO-8859-1') == 'UTF-8'
            ? utf8_decode($emailTXT)
            : $emailTXT);

        if ($imageDirectory != null && $imageDirectory{0} != '/')
            $imageDirectory = "/$imageDirectory";
        $mail->setHTML($emailHTM, $imageDirectory);

        // Set attachments
        if ($attachments != null) {
            if (is_array($attachments)) {
                foreach ($attachments as $key => $value) {
                    if ($value instanceof stringAttachment)
                        $mail->addAttachment($value);
                    else {
                        if (!is_file($value))
                            throw new Exception("Attachment file '$value' does not exist.");
                        $attachment = new fileAttachment($value);
                        if (is_string($key) && strlen($key) > 0)
                            $attachment->name = $key;

                        $mail->addAttachment($attachment);
                    }
                }
            } elseif (is_file($attachments))
                $mail->addAttachment(new fileAttachment($attachments));
            else
                throw new Exception("File $attachments does not exist.");
        }
        return $mail->send(array($email)/* , 'smtp' */) ? true : $mail->getErrors();
    }

    public function getName(){ return $this->Name; }
    protected $Name;
    protected $IsBlacklisted;

    /**
     * Enter description here...
     *
     * @return EmailAddressValidator
     */
    public function getValidator(){ return $this->Validator; }
    protected $Validator;

    public function __construct( $name )
    {
        $this->Name = $name;
        $this->Validator = new EmailAddressValidator( $name );
    }

    public function __destruct()
    {
        unset($this->Validator);
    }

    public function IsValid()
    {
        return $this->Validator->getEmailValid();
    }

    public function IsBlacklisted()
    {
        if($this->IsBlacklisted == null){
            if(!$this->Validator->getEmailValid())
                throw new EmailAddressValidatorException( "Can not perform blacklist check. '$this->Name' Email syntax is invalid." );

            try{
                $procedure = new StoredProcedure( 'QueryEmailDomainIsBlacklisted', APPSTATIC_DATABASE );
                $procedure->Prepare($this->Validator->getDomain());
                $procedure->Execute();
                $procedure->BindResult( array(&$this->IsBlacklisted) );
                $procedure->Fetch();
                unset($procedure);
            }
            catch(Exception $ex){
                unset($procedure);
                throw new EmailException('Blacklist check failed.', 0, $ex);
            }
        }
        return (boolean) $this->IsBlacklisted;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->Name;
    }
}

final class EmailException extends ExceptionBase
{
    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}
/*BEGIN

SELECT  COUNT(blacklistedemaildomains.BlacklistedEmailDomain_ID)
FROM    blacklistedemaildomains
WHERE   email REGEXP CONCAT('^.*', blacklistedemaildomains.BlacklistedEmailDomain_Name, '$');

END*/
/*public function IsBlacklisted()
    {
        $blacklist = array(
            'antichef.net',
            'bsnow.net',
            'bumpymail.com',
            'centermail.com',
            'centermail.net',
            'discardmail.com',
            'discardmail.de',
            'dodgeit.com',
            'dodgit.com',
            'dontsendmespam.de',
            'dumpmail.de',
            'e4ward.com',
            'eintagsmail.de',
            'emailias.com',
            'emailto.de',
            'fastacura.com',
            'fastchevy.com',
            'fastchrysler.com',
            'fastkawasaki.com',
            'fastmazda.com',
            'fastmitsubishi.com',
            'fastnissan.com',
            'fastsubaru.com',
            'fastsuzuki.com',
            'fasttoyota.com',
            'fastyamaha.com',
            'ghosttexter.de',
            'gomail.ws',
            'htl22.at',
            'jetable.net',
            'jetable.org',
            'kasmail.com',
            'klassmaster.com',
            'kurzepost.de',
            'mailin8r.com',
            'mailblocks.com',
            'mailexpire.com',
            'mailinator.com',
            'mailinator.net',
            'mailinator2.com',
            'mailshell.com',
            'mbx.cc',
            'messagebeamer.de',
            'mytrashmail.com',
            'netmails.net',
            'nervmich.net',
            'nervtmich.net',
            'netzidiot.de',
            'nospammail.net',
            'nurfuerspam.de',
            'pookmail.com',
            'privacy.net',
            'privy-mail.de',
            'punkass.com',
            'qv7.info',
            'rcpt.at',
            'sneakemail.com',
            'sofort-mail.de',
            'spam.la',
            'spambob.com',
            'spambob.net',
            'spambob.org',
            'spambog.com',
            'spambog.de',
            'spambox.de',
            'spambox.us',
            'spamex.com',
            'spamgourmet.com',
            'spamherelots.com',
            'spamhole.com',
            'spaminator.de',
            'spammotel.com',
            'spamoff.de',
            'spamtrail.com',
            'sogetthis.com',
            'svenz.eu',
            'temp-mail.org',
            'temporarily.de',
            'thisisnotmyrealemail.com',
            'trash-mail.at',
            'trash-mail.com',
            'trash-mail.de',
            'trash-mail.net',
            'trashmail.at',
            'trashmail.com',
            'trashmail.de',
            'trashmail.me',
            'trashmail.net',
            'wegwerfadresse.de',
            'wh4f.org',
            'whitemail.ie',
            'willhackforfood.biz',
            'yandex.ru',
            'zipzaps.de' );

        $pattern = '';
        foreach($blacklist as &$entry)
        {
            if($pattern != '')
                $pattern .= '|';
            $pattern .= $entry;
        }

        $pattern = "/^.*($pattern)$/i";

        return (boolean) preg_match( $pattern, $this->Name );
    }*/