<?php

define( 'BOUNCE_REASON_BAD_EMAIL_ADDRESS', '511' );
define( 'BOUNCE_REASON_MAILBOX_UNAVAILABLE', '550' );
define( 'BOUNCE_REASON_MAILBOX_NAME_NOT_ALLOWED', '553' );
define( 'BOUNCE_REASON_TRANSACTION_FAILED', '554' );
define( 'BOUNCE_REASON_BAD_MAILBOX_ADDRESS', '5.1.1' );
define( 'BOUNCE_REASON_BAD_SYSTEM_ADDRESS', '5.1.2' );
define( 'BOUNCE_REASON_BAD_ADDRESS_SYNTAX', '5.1.3' );
define( 'BOUNCE_REASON_ADDRESS_AMBIGOUS', '5.1.4' );
define( 'BOUNCE_REASON_MAILBOX_DISABLED', '5.2.1' );
define( 'BOUNCE_REASON_SPAM_DETECTED', '5.3.0' );
define( 'BOUNCE_REASON_DELIVERY_NOT_AUTHORIZED', '5.7.1' );

class BounceMessageParser extends \AppStatic\Core\PropertyBase
{
    protected $smtpReplyCode;
    protected $enhancedMailSystemStatusCode;
    public function getEnhancedMailSystemStatusCode()
            {
        return $this->enhancedMailSystemStatusCode;
    }
    protected $enhancedMailSystemStatusCodes = array(
        BOUNCE_REASON_BAD_MAILBOX_ADDRESS,
        BOUNCE_REASON_BAD_SYSTEM_ADDRESS,
        BOUNCE_REASON_BAD_ADDRESS_SYNTAX,
        BOUNCE_REASON_ADDRESS_AMBIGOUS,
        BOUNCE_REASON_MAILBOX_DISABLED,
        BOUNCE_REASON_DELIVERY_NOT_AUTHORIZED
    );
    
    public function getBounceReason()
            {
        return $this->bounceReason;
    }
    protected $bounceReason;
    protected $bounceReasons = array(
        'user unknown',
        'unknown user',
        'unknown alias',
        'no such local user',
        'recipient unknown',
        'no such user',
        'no such mailbox',
        'inactive user',
        'unknown local part',
        'invalid address',
        'unrouteable address',
        'address rejected',
        'Address unknown',
        'mailbox unavailable',
        'mailbox not found',
        'mailbox deactivated',
        'relay access denied',
        "This user doesn't have a .* account",
        'account has been disabled or discontinued',
        'no mailbox here by that name',
        "couldn't find any host named"
    );

    /**
     * Parses the bounce message and returnes true, if the message could be fully covered as hard bounce.
     *
     * @param string $message
     * @return boolean
     */
    public function Parse( $message )
    {
        $this->smtpReplyCode = null;
        $this->enhancedMailSystemStatusCode = null;
        $this->bounceReason = null;
        $matches = array( );
        // Mailbox unavailable
        if (preg_match( '/^(5\d\d)[ -]/', $message, $matches )) {
            $this->smtpReplyCode = $matches[ 1 ];

            switch ($this->smtpReplyCode) {
                case BOUNCE_REASON_BAD_EMAIL_ADDRESS:
                case BOUNCE_REASON_MAILBOX_UNAVAILABLE:
                case BOUNCE_REASON_MAILBOX_NAME_NOT_ALLOWED:
                case BOUNCE_REASON_TRANSACTION_FAILED:
                    foreach ($this->enhancedMailSystemStatusCodes as $statusCode) {
                    $pattern = '/' . preg_replace( '/\./', '\.',  $statusCode ) . '/';
                        if (preg_match( $pattern, $message )) {
                            $this->enhancedMailSystemStatusCode = $statusCode;
                            break;
                        }
                    }
                    break;
                default:
                    break;
            }

            if ($this->enhancedMailSystemStatusCode == null) {
                foreach ($this->bounceReasons as $bounceReason) {
                    if (preg_match( "/$bounceReason/i", $message )) {
                        $this->bounceReason = $message;
                        break;
                    }
                }
            }
        }
        // return true only if message could be fully parsed
        return $this->smtpReplyCode != null &&
                (($this->enhancedMailSystemStatusCode != null &&
                $this->enhancedMailSystemStatusCode != BOUNCE_REASON_DELIVERY_NOT_AUTHORIZED &&
                $this->enhancedMailSystemStatusCode != BOUNCE_REASON_SPAM_DETECTED) || $this->bounceReason != null );
    }
    // (.+storage space.+)|(.+too many messages.+)
}
