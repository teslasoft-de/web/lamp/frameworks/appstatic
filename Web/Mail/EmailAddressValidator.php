<?php

namespace AppStatic\Web\Mail;

use AppStatic\Core\PropertyBase;
use AppStatic\Core\ExceptionBase;

require_once APPSTATIC_PATH . '/Net/Win32Support.php';

define( 'PRE_DELAY_TIMEOUT', 1 );
define( 'PRE_DELAY_LIMIT', 1 );
define( 'EMAILADDRESSVALIDATOR_EMAIL_REGREX',
        '([a-zA-Z0-9\._\+-]+)\@((\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,7}|[0-9]{1,3})(\]?))' );

/**
 * Performs email address verification with regular expression validation, MX Lookup and SMTP probes.
 * 
 * @package AppStatic
 * @name EmailAddressValidator
 * @version 1.1 (2009-01-31)
 * 
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow
 * @uses AppStatic\Net\Win32Support.php, AppStatic\Core\ExeptionBase.php, AppStatic\Core\PropertyBase.php
 * 
 * @link http://www.tienhuis.nl/php-email-address-validation-with-verify-probe
 */
final class EmailAddressValidator extends PropertyBase
{
    private $email;

    /**
     * Gets the User.
     *
     * @return string
     */
    public function getUser()
    {
        return $this->User;
    }

    protected $User;

    /**
     * Gets the Domain.
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->Domain;
    }

    protected $Domain;
    private $mailers;
    protected $MailersConnectable;
    protected $ConnectionRefused;
    private $server_timeout;

    /**
     * Returnes a boolean value which indicates if the serial is valid.
     *
     * @return boolean
     */
    public function getEmailValid()
    {
        return $this->EmailValid;
    }

    protected $EmailValid;

    public function getMXRecordsFound()
    {
        return $this->mailers != null;
    }

    public function getMailersConnectable()
    {
        return $this->MailersConnectable;
    }

    public function getConnectionRefused()
    {
        return $this->ConnectionRefused;
    }

    private $debug;

    /**
     * Returns the trace output.
     *
     */
    public function getTrace()
    {
        return $this->trace;
    }

    private $trace;

    /**
     * Gets a string which describes the error.
     *
     * @return string
     */
    public function getError()
    {
        return $this->Error;
    }

    protected $Error;

    /**
     * Initializes a new instance of the EmailAddressValidator class.
     *
     * @param string $email The address you are trying to verify.
     * @param boolean $debug If set to true, trace messages will be generated for every action.
     */
    public function __construct( $email, $debug = false )
    {
        $this->debug = $debug;
        if ($this->debug)
            $this->trace = '';
        $this->server_timeout = 180; # timeout in seconds. Some servers deliberately wait a while (tarpitting)

        $this->email = $email;

        $matches = null;
        if (!preg_match( '/^' . EMAILADDRESSVALIDATOR_EMAIL_REGREX . '$/', $email, $matches )) {
            $this->Error = 'Address syntax not correct';
            $this->EmailValid = false;
        }
        else {
            $this->EmailValid = true;
            $this->User = $matches[1];
            $this->Domain = $matches[2];
        }
    }

    /**
     * Executes a DNS MX Lookup for the specified domain and retunes an array which containes a list of the asociated mailers.
     *
     * @param string $domain
     * @return array
     * @throws EmailAddressValidatorException
     */
    public static function GetMailers( $domain )
    {
        if (!function_exists( 'checkdnsrr' ))
            throw new EmailAddressValidatorException( "Required function 'checkdnsrr' does not exist." );

        # Construct array of available mailservers
        $mxhosts = null;
        $mxweight = null;
        $mailers = array( );

        if (getmxrr( $domain . '.', $mxhosts, $mxweight )) {
            $mxs = array( );
            for ($i = 0; $i < count( $mxhosts ); $i++)
                $mxs[$mxhosts[$i]] = $mxweight[$i];
            asort( $mxs );
            $mailers = array_keys( $mxs );
        }
        elseif (checkdnsrr( $domain, 'A' ))
            $mailers[0] = gethostbyname( $domain );

        return $mailers;
    }

    /**
     * Check if any DNS MX records exist for domain part
     *
     */
    public function CheckDomain()
    {
        # Check availability of DNS MX record
        $mailers = self::GetMailers( $this->Domain );
        $total = count( $mailers );

        if ($total <= 0) {
            $this->EmailValid = false;
            $this->Error = "No usable DNS records found for domain '$this->Domain'";
            return false;
        }
        self::Trace( "Check availability of DNS MX records for domain $this->Domain... Sucess" );
        unset( $total );
        $this->mailers = $mailers;
        unset( $mailers );
        return true;
    }

    /**
     * Verifies the specified mailer with SMTP probe
     *
     * @param string $mailer
     * @param string $probe_address
     * @param string $helo_address
     * @param boolean $grayListCheck
     * @return boolean
     * @throws EmailAddressValidatorException
     */
    private function VerifiMailer( $mailer, $probe_address, $helo_address, $grayListCheck = false )
    {

        # Check if socket can be opened
        self::Trace( "Opening up socket to $mailer... ", false );
        $errno = 0;
        $errstr = 0;
        # Try to open up socket
        $sock = @fsockopen( $mailer, 25, $errno, $errstr, $this->server_timeout );
        if (!$sock) {
            self::Trace( "Failed!" );
            $this->MailersConnectable = false;
            return false;
        }
        $response = fgets( $sock );

        // Make shure the complete welcome message has been read from the socket so no pre-greet-delay (Greet Pause) errors can occur.
        socket_set_blocking( $sock, false );
        /** @noinspection PhpUnusedLocalVariableInspection */
        $nextLine = '';
        do {
            $nextLine = fgets( $sock );
            if (!empty( $nextLine ))
                $response .= $nextLine;
        }while ($nextLine !== false);
        socket_set_blocking( $sock, true );

        self::Trace( "Success!\n" );

        $this->MailersConnectable = true;
        stream_set_timeout( $sock, 30 );
        $meta = stream_get_meta_data( $sock );
        self::Trace( "$mailer replied: $response" );

        # Hard error on connect -> break out 
        # Error means 'any reply that does not start with 2xx '
        $return = null;
        if ($meta['timed_out']) {
            $this->Error = "Error: $mailer timed out";
            $return = false;
        }
        elseif (!$grayListCheck && preg_match( '/^451[ -]/', $response )) { // Greylisting
            self::Trace( "$mailer uses Graylisting: $response" );
            $return = self::VerifiMailer( $mailer, $probe_address, $helo_address, true );
            $this->Error = !$return ? "Error: $mailer said: $response" : null;
        }
        elseif (!preg_match( '/^2\d\d[ -]/', $response )) {
            $this->Error = "Error: $mailer said: $response";
            if (preg_match( '/^554[ -]/', $response ))
                $this->ConnectionRefused = true;
            $return = false;
        }

        # Enshure closing socket properly.
        if ($return !== null) {
            fclose( $sock );
            return $return;
        }

        $cmds = array(
            "HELO $helo_address",
            "MAIL FROM: <$probe_address>",
            "RCPT TO: <$this->email>",
            "QUIT" );

        if ($this->debug)
            $before = 0.0;
        foreach ($cmds as $cmd) {

            if ($this->debug)
                $before = microtime( true );

            fputs( $sock, "$cmd\r\n" );
            $response = fgets( $sock, 4096 );

            if ($this->debug) {
                /** @noinspection PhpUndefinedVariableInspection */
                $t = 1000 * (microtime( true ) - $before);
                self::Trace( htmlentities( "$cmd\n$response" ) . "(" . sprintf( '%.2f', $t ) . " ms)" );
            }

            if (!$meta['timed_out'] && preg_match( '/^5\d\d[ -]/', $response )) {
                $this->Error = "Unverified address: $mailer said: $response";
                fclose( $sock );
                throw new EmailAddressValidatorException( ); // Breakes the currend server cycle checking because the email does not exist.
            }
        }
        fclose( $sock );
        return true;
    }

    /**
     * Verifies the email address with SMTP probes.
     *
     * @param boolean $verify Use SMTP verify probes to see if the address is deliverable.
     * @param string $probe_address This is the email address that is used as FROM address in outgoing
     * probes. Make sure this address exists so that in the event that the
     * other side does probing too this will work.
     * @param string $helo_address This should be the hostname of the machine that runs this site.
     * @return bool
     * @throws EmailAddressValidatorException
     */
    public function ValidateEmail( $verify = false, $probe_address = '', $helo_address = '' )
    {
        if (!$this->EmailValid)
            throw new EmailAddressValidatorException( "Can not validate Email. Syntax is invalid." );

        # Check availability of DNS MX records 
        if (!self::CheckDomain())
            $this->EmailValid = false;

        # Query each mailserver 
        if ($this->EmailValid && $verify) {
            $mailers = $this->mailers;
            $total = count( $this->mailers );
            try {
                # Check if mailers accept mail 
                for ($n = 0; $n < $total; $n++) {

                    self::Trace( "Checking server $mailers[$n]..." );
                  
                    if (self::VerifiMailer( $mailers[$n], $probe_address, $helo_address )) {
                        self::Trace( "Successful communication with $mailers[$n], no hard errors, assuming OK" );
                        $this->EmailValid = true;
                        break;
                    }
                    if($this->ConnectionRefused)
                        break;
                    
                    if ($n == ($total - 1)) {
                        $this->Error = "None of the mailservers listed for $this->Domain could be contacted";
                        $this->EmailValid = false;
                    }
                }
            }// Catch exception if the mail does not exist
            catch (EmailAddressValidatorException $ex) {
                $this->EmailValid = $ex == null;
            }
        }
        if ($this->EmailValid)
            $this->Error = null;
        return $this->EmailValid;
    }

    /**
     * Adds a new line to the trace.
     *
     * @param string $line
     * @param bool $newLine
     */
    private function Trace( $line, $newLine = true )
    {
        if (!$this->debug)
            return;
        $this->trace .= $line;
        if ($newLine)
            $this->trace .= '</br>';
    }

}

final class EmailAddressValidatorException extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }

}

/* try{
  //print_r( EmailAddressValidator::GetMailers( "gmail.com" ) );

  $emailValidator = new EmailAddressValidator( 'hotbabehs@web.de', true );
  if( $emailValidator->GetEmailValid() ){
  $emailValidator->ValidateEmail(true, 'info@publishdata.de', 'www.publishdata.de' );
  echo $emailValidator->GetEmailValid();
  }
  echo '<pre>';
  echo $emailValidator->GetTrace();
  echo '</pre>';
  if( $emailValidator->GetError() != null )
  echo utf8_htmlentities( $emailValidator->GetError() );
  }
  catch (EmailAddressValidatorException $ex){
  echo $ex->GenerateMessages();
  }
  catch (Exception $ex){
  ExceptionHandler::HandleException( $ex );
  } */
?>