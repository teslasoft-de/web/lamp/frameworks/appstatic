<?php

namespace AppStatic\Web\Mail;

use AppStatic\Web\DomainName;
use AppStatic\Web\Security\PredefinedVariablesValidator;
use Exception;

// Load AppStatic Environment
require_once __DIR__ . '/../../Include.inc.php';

$SCRIPT_NAME = $_SERVER['SCRIPT_NAME'];
$HTTP_HOST = $_SERVER['HTTP_HOST'];

$script = array_pop( explode( '/', $SCRIPT_NAME ) );
$script_dir = substr( $SCRIPT_NAME, 0, strlen( $SCRIPT_NAME ) - strlen( $script ) );
$scriptURL = 'http://' . $HTTP_HOST . $script_dir . "$script";

$email = PredefinedVariablesValidator::ValidateGET( 'email' ) ? (string) $_GET['email'] : null;
$debug = PredefinedVariablesValidator::ValidateGET( 'debug' ) ? (boolean) $_GET['debug'] : false;

$checked = $debug ? 'checked="checked"' : '';
echo <<< EOT
<html><body style="margin:25px;">
<div><title>Email Adress Validator</title>
Enter the mail you want to validate:
<br><br><form action='$scriptURL' method=GET>
<input name="email" value="$email" size="50" maxlength="254"><input type=submit value="Validate"><br/>
<input type=checkbox name="debug" value="1" $checked>Show Trace</form></div>
EOT;

if (empty($email)){
    echo "Please enter an email adress.";
}
else{
    $emailValidator = null;
    $emailValid = false;
    try {
        $emailValidator = new EmailAddressValidator( $email, $debug );
        if ($emailValidator->getEmailValid()) {
            $domain = DomainName::getCurrent();            
            $emailValidator->ValidateEmail( true, 'postmaster@' . $domain->ToString( 1, 2 ), $domain );
            //$emailValidator->ValidateEmail( true, 'info@publishdata.de', 'www.publishdata.de' );
            $emailValid = $emailValidator->getEmailValid();
        }

        if ($debug) {
            echo '<pre>';
            echo $emailValidator->getTrace();
            echo '</pre>';
        }

    }catch(Exception $ex) {
        echo $ex->getMessage();
    }

    echo $emailValid ? 'Success: Email seems to be valid' : 'Failed: Email is invalid'; 
    if ($emailValidator->getError() != null)
        echo '<br/>' . utf8_htmlentities( $emailValidator->getError() );
}
echo "</body></html>";
?>