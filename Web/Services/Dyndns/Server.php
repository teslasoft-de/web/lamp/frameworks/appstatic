<?php

namespace AppStatic\Web\Services\Dyndns;

/**
 * Simple Dynamic DNS server.
 */
class Server
{
    const MYIP_PARAM = 'myip';
    const MYIPv4_PARAM = 'ipv4';
    const MYIPv6_PARAM = 'ipv6';

    /**
     * Storage for all configuration variables, set in config.php
     * @var array
     */
    private $config;

    /**
     * The user currently logged in
     * @var string
     */
    private $user;

    /**
     * IP the hostnames should point to
     * @var string
     */
    private $myIp;

    /**
     * Hostnames that should be updated
     * @var array
     */
    private $hostnames;

    /**
     * Debug buffer
     * @var string
     */
    private $debugBuffer;

    /**
     * @var Users
     */
    private $users;

    /**
     * @var Hosts
     */
    private $hosts;

    private $debugHostname;

    /**
     * @param mixed $debugHostname
     */
    public function setDebugHostname( $debugHostname )
    {
        $this->debugHostname = $debugHostname;
    }

    public function __construct()
    {
        $this->config = [
            'hostsFile' => 'dyndns.hosts',  // Location of the hosts database
            'userFile'  => 'dyndns.users',  // Location of the user database
            'debugFile' => 'dyndns.log',    // Debug file
            'debug'     => false,           // Enable debugging

            'bind.server' => false,
            'bind.zone'   => '',
            'bind.ttl'    => 300,
            'bind.key'    => '',
        ];
    }

    public function Init()
    {
        $this->users = new Users( $this->config['userFile'] );
        $this->hosts = new Hosts( $this->config['hostsFile'] );

        $this->CheckHttpMethod();
        $this->CheckAuthentication();

        // Get hostnames to be updated
        $this->hostnames = [];
        if (array_key_exists( 'hostname', $_REQUEST ) && ($_REQUEST['hostname'] != '')) {
            $this->hostnames = explode( ',', strtolower( $_REQUEST['hostname'] ) );
            $this->CheckHostnames();
        }
        else
            $this->ReturnCode( 'notfqdn' );

        // Get IP address, fallback to REMOTE_ADDR
        $IPs = [ self::MYIP_PARAM => Helper::getMyIp() ];
        foreach ([ self::MYIP_PARAM, self::MYIPv4_PARAM, self::MYIPv6_PARAM ] as $ipParam) {
            if (array_key_exists( $ipParam, $_REQUEST )) {
                if (Helper::CheckValidIp( $_REQUEST[ $ipParam ] )) {
                    // Specific IPs having higher precedence and overriding all defaults so we remove the default IP.
                    if ($ipParam != self::MYIP_PARAM) {
                        if (isset($_REQUEST[ self::MYIP_PARAM ]))
                            $this->Debug( "Too many arguments specified. Dropping default Param '" . self::MYIP_PARAM . "' when receiving specific IP types." );
                        unset($IPs[ self::MYIP_PARAM ]);
                    }

                    $IPs[ $ipParam ] = $_REQUEST[ $ipParam ];
                }
                else
                    $IPs[ $ipParam ] = false;
            }
        }

        foreach ($IPs as $paramName => $ip) {
            if (!$ip) {
                if ($paramName == self::MYIP_PARAM)
                    $this->Debug( "Invalid parameter '$paramName'. Using default REMOTE_ADDR" );
                else
                    $this->Debug( "Invalid parameter '$paramName'. Skipping..." );
                continue;
            }
            $this->myIp [ $paramName ] = $ip;
        }

        $this->UpdateHosts();

        // Return "good" code as everything seems to be ok now
        $this->ReturnCode( 'good' );

        return $this;
    }

    /**
     * @param array|string $key
     * @param string       $value
     *
     * @return $this
     */
    public function setConfig( $key, $value = null )
    {

        if (is_array( $key )) {
            // Detect realpath candidates
            $key = array_map( function($k) {
                return strpos($k,'/../') ? realpath($k): $k;
            }, $key);
            $this->config = $key + $this->config;
        }
        else if (empty($value))
            trigger_error( "Cannot set empty config value.", E_USER_ERROR );
        else
            $this->config[ $key ] = $value;

        return $this;
    }

    public function getConfig( $key )
    {
        return $this->config[ $key ];
    }

    private function CheckHttpMethod()
    {
        // Only HTTP method "GET" is allowed here
        if ($_SERVER['REQUEST_METHOD'] != 'GET') {
            $this->Debug( 'ERROR: HTTP method ' . $_SERVER['REQUEST_METHOD'] . ' is not allowed.' );
            $this->ReturnCode( 'badagent', [ 'HTTP/1.0 405 Method Not Allowed' ] );
        }
    }

    private function CheckAuthentication()
    {
        // Request user/pw if not submitted yet
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            $this->Debug( 'No authentication data sent.' );
            $this->ReturnCode( 'badauth', [ 'WWW-Authenticate: Basic realm="DynDNS API Access"',
                                            'HTTP/1.0 401 Unauthorized' ]
            );
        }

        $user = strtolower( $_SERVER['PHP_AUTH_USER'] );
        $password = $_SERVER['PHP_AUTH_PW'];

        if (!$this->users->CheckCredentials( $user, $password )) {
            $this->ReturnCode( 'badauth', [ 'HTTP/1.0 403 Forbidden' ] );
        }
        $this->user = $user;
    }

    private function CheckHostnames()
    {
        foreach ($this->hostnames as $hostname) {
            // check if the hostname is valid FQDN
            if (!Helper::CheckValidHost( $hostname ))
                $this->ReturnCode( 'notfqdn' );

            // check if the user is allowed to update the hostname
            if (!$this->hosts->CheckUserHost( $this->user, $hostname ))
                $this->ReturnCode( 'nohost' );
        }
    }

    private function UpdateHosts()
    {
        foreach ($this->hostnames as $hostname) {
            $this->debugHostname = $hostname;
            if (!$this->hosts->Update( $hostname, $this->myIp ))
                $this->ReturnCode( 'dnserr' );
        }

        // Flush host database (write to hosts file)
        if (!$this->hosts->Flush())
            $this->ReturnCode( 'dnserr' );
    }

    private function ReturnCode( $code, $additionalHeaders = [] )
    {
        foreach ($additionalHeaders as $header)
            header( $header );

        $this->Debug( 'Sending return code: ' . $code );
        echo $code;
        $this->Shutdown();
    }

    private function Shutdown()
    {
        // Flush debug buffer
        if (($this->debugBuffer != "") && ($this->config['debug'])) {
            if ($fh = @fopen( $this->config['debugFile'], 'a' )) {
                fwrite( $fh, $this->debugBuffer );
                fclose( $fh );
            }
        }

        exit;
    }

    public function Debug( $message )
    {
        $this->debugBuffer .=
            @date( 'M j H:i:s' ) .
            " Dyndns: " . gethostbyaddr( $_SERVER['REMOTE_ADDR'] ) .
            ($this->debugHostname ? ' ' . $this->debugHostname : '') . " - $message\n";
    }
}
