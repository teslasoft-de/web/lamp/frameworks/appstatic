<?php

namespace AppStatic\Web\Services\Dyndns;

use AppStatic\Core\String;

/**
 * Host database.
 */
class Hosts
{
    /**
     * Filename of the hosts file (dyndns.hosts)
     * @var string
     */
    private $hostsFile;

    /**
     * Host/Users array:  'hostname' => array ('user1', 'user2', ...)
     * @var array
     */
    private $hosts;

    /**
     * List of updates in the format 'hostname' => 'ip'
     * @var array
     */
    private $updates;

    /**
     * This is true if the status / user files were read
     * @var boolean
     */
    private $initialized;

    /**
     * Constructor.
     *
     * @param string $hostsFile
     */
    public function __construct( $hostsFile )
    {
        $this->hostsFile = $hostsFile;
        $this->initialized = false;
        $this->updates = [];
    }

    protected function setUpdate( $hostname, $update )
    {
        if (!isset($this->updates[ $hostname ]))
            $this->updates[ $hostname ] = [ $update ];
        else
            $this->updates[ $hostname ] [] = $update;
    }

    /**
     * Adds an update to the list
     *
     * @param string $hostname
     * @param string $ips
     *
     * @return bool
     */
    public function Update( $hostname, $ips )
    {
        if (!$this->initialized)
            $this->Init();

        $this->setUpdate( $hostname, $ips );

        return true;
    }

    /**
     * Checks if the host belongs to the user
     *
     * @param string $user
     * @param string $hostname
     *
     * @return boolean True if the user is allowed to update the host
     */
    function CheckUserHost( $user, $hostname )
    {
        if (!Helper::CheckValidHost( $hostname )) {
            $this->Debug( 'Invalid host: ' . $hostname );

            return false;
        }

        if (!$this->initialized)
            $this->Init();

        if (is_array( $this->hosts )) {
            foreach ($this->hosts as $line) {
                if (preg_match( "/^(.*?):(.*)/", $line, $matches )) {
                    if (Helper::CompareHosts( $matches[1], $hostname, '*' ) &&
                        in_array( $user, explode( ',', strtolower( $matches[2] ) ) )
                    )
                        return true;
                }
            }
        }
        $this->Debug( 'Host ' . $hostname . ' does not belong to user ' . $user );

        return false;
    }

    /**
     * Write cached changes to the status file
     */
    public function Flush()
    {
        return $this->UpdateBind();
    }

    /**
     * Initializes the user and status list from the file
     *
     * @access private
     */
    private function Init()
    {
        if ($this->initialized) return;

        $this->ReadHostsFile();
        if (!is_array( $this->hosts ))
            $this->hosts = [];

        $this->initialized = true;
    }

    function ReadHostsFile()
    {
        $lines = @file( $this->hostsFile );
        if (is_array( $lines ))
            $this->hosts = $lines;

        else
            $this->Debug( 'Empty hosts file: "' . $this->hostsFile . '"' );
    }

    /**
     * Sends DNS Updates to BIND server
     *
     * @access private
     */
    private function UpdateBind()
    {
        $server = $this->getConfig( 'bind.server' );
        $zone = $this->getConfig( 'bind.zone' );
        $ttl = $this->getConfig( 'bind.ttl' ) * 1;
        $keyfile = $this->getConfig( 'bind.keyfile' );
        //var_dump($server, $zone, $ttl, $keyfile);

        // sanitiy checks
        if (!Helper::CheckValidHost( $server )) {
            $this->Debug( 'ERROR: Invalid bind.server config value' );

            return false;
        }
        if (!Helper::CheckValidHost( $zone )) {
            $this->Debug( 'ERROR: Invalid bind.zone config value' );

            return false;
        }
        if (!is_int( $ttl )) {
            $this->Debug( 'Invalid bind.ttl config value. Setting to default 300.' );
            $ttl = 300;
        }
        if ($ttl < 60) {
            $this->Debug( 'bind.ttl is too low. Setting to default 300.' );
            $ttl = 300;
        }
        if (!is_readable( $keyfile )) {
            $this->Debug( 'ERROR: Invalid bind.keyfile config value' );

            return false;
        }

        // create temp file with nsupdate commands
        $tempfile = tempnam( sys_get_temp_dir(), 'Dyndns' );
        $fh = @fopen( $tempfile, 'w' );
        if (!$fh) {
            $this->Debug( 'ERROR: Could not open temporary file' );

            return false;
        }
        fwrite( $fh, "server $server\n" );
        fwrite( $fh, "zone $zone\n" );
        foreach ($this->updates as $host => $ips) {
            $GLOBALS['dyndns']->setDebugHostname($host);
            foreach ($ips as $_ips) {
                foreach ($_ips as $paramName => $ip) {
                    $recType = Helper::getRecordType( $ip );

                    if ($recType === false) {
                        $this->Debug( "ERROR: unknown $paramName record type '$recType'. Skipping..." );
                        continue;
                    }

                    if (!Helper::HasIPChanged( $host, $ip )) {
                        $this->Debug( "$recType record has not changed. Skipping..." );
                        continue;
                    }
                    $this->Debug( "Update: $recType $ip" );
                    fwrite( $fh, "update delete $host $recType\n" );
                    fwrite( $fh, "update add $host $ttl $recType $ip\n" );
                }
            }
        }
        fwrite( $fh, "send\n" );
        fclose( $fh );

        // Execute nsupdate
        $command = '/usr/bin/nsupdate -k ' . String::EscapeShellArg( $keyfile ) . ' ' . $tempfile . ' 2>&1';
        $result = exec( $command );
        /*echo '<pre>';
        var_dump(readfile($tempfile),$command);
        echo '</pre>';*/

        unlink( $tempfile );
        if ($result != '') {
            $this->Debug( 'ERROR: nsupdate returns: ' . $result );

            return false;
        }

        return true;
    }

    private function getConfig( $key )
    {
        return $GLOBALS['dyndns']->getConfig( $key );
    }

    private function Debug( $message )
    {
        return $GLOBALS['dyndns']->Debug( $message );
    }
}
