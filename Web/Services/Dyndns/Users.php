<?php

namespace AppStatic\Web\Services\Dyndns;

use AppStatic\Security\Password as Password;

/**
 * User database.
 */
class Users
{
    private $userFile;

    public function __construct( $userFile )
    {
        $this->userFile = $userFile;
    }

    public function CheckCredentials( $user, $password )
    {
        $lines = @file( $this->userFile );
        $bcrypt = null;
        if (is_array( $lines )) {
            foreach ($lines as $line) {
                if (preg_match( "/^(.*?):(.*)/", $line, $matches )) {
                    if (strtolower( $matches[1] ) == strtolower( $user )) {
                        if (Password::Compare( $matches[2], $password, PASSWORD_BCRYPT )) {
                            $this->Debug( 'Login successful for user ' . $user );

                            return true;
                        }
                        else {
                            $this->Debug( 'Wrong password for user: ' . $user );

                            return false;
                        }
                    }
                }
            }
        }
        else $this->Debug( 'Empty user file: "' . $this->userFile . '"' );

        $this->Debug( 'Unknown user: ' . $user );

        return false;
    }

    private function Debug( $message )
    {
        $GLOBALS['dyndns']->Debug( $message );
    }
}
