<?php

namespace AppStatic\Web\Services\Ajax;

class AjaxXmlWriter extends \XMLWriter
{
    protected $RootNode;

    public function getRootNode()
    {
        return $this->RootNode;
    }

    public function __construct( $rootNode )
    {
        if (!is_string( $rootNode ))
            throw new \InvalidArgumentException ( 'Invalid parameter: $rootNode', 0 );
        
        self::openMemory(); 
        self::setIndent( true );
        self::setIndentString( " " );
        self::startDocument( '1.0', 'UTF-8' );
        self::writeComment( 'Copyright (c) ' . date( 'Y' ) . ' Teslasoft - www.teslasoft.de. All rights reserved.
Any unauthorized use, disclosure, reproduction or dissemination, in full or in part, in any media or by any means,
without the prior written permission of Publish Data is prohibited and will be punished severely by the law.' );
        self::startElement( $rootNode );
    }

    /**
     * Enter description here...
     *
     * @param mixed $tagName
     * @param array $attributes
     */
    public function Write( $tagName, $attributes = null )
    {
        if (!is_string( $attributes ) && is_array( $attributes )) {
            
            if (!is_integer( $tagName ))
                self::startElement( $tagName );
            
            foreach( $attributes as $key => $value ) {
                if (is_numeric( $key ) && !is_string( $value ) && is_array( $value )) {
                    foreach( $value as $attributeName => $attributeValue )
                        $this->write( $attributeName, $attributeValue );
                }else
                    $this->Write( $key, $value );
            }
            if (!is_integer( $tagName ))
                self::endElement();
        }else {	
     		//$encoding = mb_detect_encoding( $attributes );
     		$attributes = iconv( 'iso-8859-1', 'UTF-8', $attributes );
     		
     		if (is_string( $attributes ))
        		$attributes = html_entity_decode( $attributes, ENT_QUOTES, 'UTF-8' );
        	
        	if(!is_numeric( $tagName ))
	            self::writeAttribute( $tagName, $attributes );
            else
            	self::writeRaw( $attributes );
        }
    }

    /**
     * Closes and prints the entire ajax xml with the corresponding xml header.
     *
     */
    public function PrintXML()
    {
        self::endElement();
        self::endDocument();
        ob_end_clean();
        header( "Content-Type: text/xml" );
        echo self::outputMemory( true );
    }
}

?>