<?php

namespace AppStatic\Web\Services\Ajax;

use AppStatic\Configuration\System;
use AppStatic\Web\SessionException;
use Exception;

/**
 * Description of AjaxXmlBase
 *
 * @author Christian Kusmanow
 */
abstract class AjaxXmlBase extends AjaxXmlWriter
{
    public function __construct( $rootNode )
    {
        parent::__construct($rootNode);
    }

    protected abstract function GenerateElements();
    protected abstract function GenerateErrorElement( $errorMessage );

    public function Generate()
    {
        try{
            $this->GenerateElements();
        }
        catch(Exception $ex){
            $this->GenerateErrorElement(
                System::IsOnline() && !($ex instanceof SessionException)
                    ? '&#91;Error&#93;'
                    : $ex->getMessage() );
        }
        $this->PrintXML();
    }
}
?>
