<?php

namespace AppStatic\Web\Services\Soap;

use AppStatic\Core;
use AppStatic\Core\String;
use AppStatic\Data\MySql\StoredProcedure;
use AppStatic\Data\MySql\StoredProcedureEmptyResultException;
use AppStatic\Web\Http\HttpRequestTrace;
use AppStatic\Web\Http\HttpResponseTrace;

define( 'SOAP_SERVICE_ERROR_STATE_TRACE_ID_INVALID', 0x2 );
define( 'SOAP_SERVICE_ERROR_STATE_TRACE_ID_UNKNOWN', 0x4 );

/**
 * Description of SoapServiceTrace
 *
 * @author Christian Kusmanow
 */
class SoapServiceTrace extends Core\PropertyBase
{

    private $id;
    private $requestTrace;
    private $responseTrace;
    private $traceCount;
    private $remoteAddress;
    private $remotePort;
    private $debug;

    public function getID()
    {
        return $this->id;
    }

    /**
     *
     * @return HttpRequestTrace
     */
    public function getRequestTrace()
    {
        return $this->requestTrace;
    }

    /**
     *
     * @return HttpResponseTrace
     */
    public function getResponseTrace()
    {
        return $this->responseTrace;
    }

    public function getTraceCount()
    {
        return $this->traceCount;
    }

    public function __construct( $debug = false )
    {
        $this->requestTrace = new HttpRequestTrace();
        $this->responseTrace = new HttpResponseTrace();

        // store the remote ip-address and port
        $this->remoteAddress = $_SERVER[ 'REMOTE_ADDR' ];
        $this->remotePort = $_SERVER[ 'REMOTE_PORT' ];

        $requestHeaders = $this->requestTrace->getHeaders();
        $id = !isset( $requestHeaders[ SOAP_HEADER_X_TRACE_ID ] ) ? md5( $this->remoteAddress . mt_rand( $this->remotePort, mt_getrandmax() ) )
                : null;

        if ($id == null) {
            $id = $requestHeaders[ SOAP_HEADER_X_TRACE_ID ];
            if (!String::IsMD5($id))
                throw new SoapServiceTraceException( 'Trace ID invalid.', SOAP_SERVICE_ERROR_STATE_TRACE_ID_INVALID );
            // TODO: Generate blacklist

            try {
                $procedure = new StoredProcedure( 'Web_QuerySoapServiceTraceCount' );
                $procedure->Prepare( $id );
                $procedure->Execute();
                $procedure->BindResult( array( &$this->traceCount ) );
                $procedure->Fetch();
            }
            catch (StoredProcedureEmptyResultException $ex) {
                throw new SoapServiceTraceException( 'Trace ID unknown.', SOAP_SERVICE_ERROR_STATE_TRACE_ID_UNKNOWN, $ex );
            }
        }
        else
            $this->traceCount = 0;

        unset( $requestHeaders );

        $this->id = $id;

        $this->debug = $debug;
    }

    /**
     * @return string The modified request;
     * @throws SoapServiceException
     * @throws \AppStatic\Data\MySql\StoredProcedureException
     */
    //protected abstract function _BeforeHandle();
    //protected abstract function _AfterHandle();
    public function Insert()
    {
        $requestSize = $this->requestTrace->getSize();
        $responseSize = $this->responseTrace->getSize();

        $rawRequest = null;
        $rawResponse = null;

        if ($this->debug) {
            $rawRequest = $this->requestTrace->getRaw();
            $rawResponse = $this->responseTrace->getRaw();
        }

        $service = SoapService::getInstance();
        $procedure = new StoredProcedure( 'Web_InsertSoapServiceTrace' );
        $procedure->Prepare(
            $this->id,
            $service->getHandleCallStart(),
            $service->getHandleCallDuration(),
            $service->getNamespace(),
            $service->getMethodName(),
            $this->remoteAddress . ':' . $this->remotePort,
            memory_get_peak_usage(),
            $requestSize,
            $responseSize,
            $rawRequest,
            $rawResponse);
        $procedure->Execute();
        unset( $procedure );
    }

    public function Finalize( $soapAction, $trackingID = null )
    {
        $procedure = new StoredProcedure( 'Web_InsertSoapServiceTraceLog' );
        $procedure->Prepare(
            $this->id,
            $trackingID,
            $soapAction );
        $procedure->Execute();
        unset( $procedure );
    }
}

final class SoapServiceTraceException extends SoapServiceException
{

    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}