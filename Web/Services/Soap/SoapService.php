<?php

namespace AppStatic\Web\Services\Soap;

use AppStatic\Core\ExceptionBase;
use AppStatic\Core\Timer;
use SoapServer;

define( 'SOAP_HEADER_X_TRACE_ID', 'X-Trace-ID' );
define( 'SOAP_SERVICE_ERROR_STATE_INSTANCE_NOT_SET', 0x1 );
define( 'SOAP_SERVICE_ERROR_STATE_EMPTY_REQUEST', 0x2 );
define( 'SOAP_SERVICE_ERROR_STATE_EMPTY_RESPONSE', 0x3 );
define( 'SOAP_SERVICE_ERROR_STATE_METHOD_NOT_FOUND', 0x4 );
define( 'SOAP_SERVICE_ERROR_STATE_REQUEST_NOT_HANDELED', 0x5 );

/**
 * Description of SoapService
 *
 * @author Christian Kusmanow
 */
abstract class SoapService extends SoapServer
{

    private static $instance;

    /**
     * @return SoapService
     * @throws SoapServiceException
     */
    public static function getInstance()
    {
        if (!isset( self::$instance ))
            throw new SoapServiceException( 'SoapService instance has not been set.', SOAP_SERVICE_ERROR_STATE_INSTANCE_NOT_SET );
        return self::$instance;
    }

    protected static function _SetInstance( SoapService $instance )
    {
        self::$instance = $instance;
    }

    protected static function _GetInstanceSet()
    {
        return isset( self::$instance );
    }

    private $wsdl;
    private $namespace;
    private $methodName;
    private $handleCallStart;
    private $handleCallDuration;
    /* @var $trace SoapServiceTrace */
    private $trace;
    private $traceSoapAction;
    private $finalize = false;
    private $trackingID;

    public function getWsdl()
    {
        return $this->wsdl;
    }

    public function getNameSpace()
    {
        return $this->namespace;
    }

    public function getMethodName()
    {
        if (!isset( $this->methodName ))
            throw new SoapServiceException( 'Soap service method name unknown. Request has not been handeled yet.', SOAP_SERVICE_ERROR_STATE_REQUEST_NOT_HANDELED );
        return $this->methodName;
    }

    public function getHandleCallStart()
    {
        return $this->handleCallStart;
    }

    public function getHandleCallDuration()
    {
        return $this->handleCallDuration;
    }

    public function getFinalizeTrace()
    {
        return $this->finalize;
    }

    public static function getSoapAction()
    {
        $headers = apache_request_headers();
        if (isset( $headers[ 'SOAPAction' ] ))
            return trim( $headers[ 'SOAPAction' ], '"' );
        return null;
    }

    /**
     * Constructor
     *
     * @param mixed $wsdl
     * @param string $nameSpace
     * @param array[optional] $options
     * @param boolean $debug
     */
    public function __construct( $wsdl, $nameSpace, $options = null, $debug = false )
    {
        if ($options == null)
            /** @noinspection PhpUndefinedMethodInspection */
            parent::__construct( $wsdl );
        else
            /** @noinspection PhpUndefinedMethodInspection */
            parent::__construct( $wsdl, $options );

        $this->wsdl = $wsdl;
        $this->namespace = $nameSpace;
    }

    public function EnableTrace( $soapAction = null, $debug = false )
    {
        $headers = apache_request_headers();
        if (!isset( $headers[ SOAP_HEADER_X_TRACE_ID ] ) && !empty( $soapAction )) {
            if (!isset( $headers[ 'SOAPAction' ] ))
                return;
            $headerValue = trim( $headers[ 'SOAPAction' ], '"' );
            if ($soapAction != $headerValue) {
                unset( $headers );
                return;
            }
            
        }
        unset( $headers );
        $this->trace = new SoapServiceTrace( $debug );
        $this->traceSoapAction = $soapAction;
    }

    public function FinalizeTrace( $trackingID )
    {
        $this->finalize = true;
        $this->trackingID = $trackingID;
    }

    /**
     * Collects all needed perameters of the soap request and handles it.
     *
     * @param string $request
     * @throws SoapServiceException
     */
    public function handle( $request = null )
    {
        if (!isset( $this->trace )) {
            parent::handle();
            return;
        }

        // check input param
        if (is_null( $request ))
            $request = $this->trace->getRequestTrace()->getBody();

        if (empty( $request ) || $this->_ProcessRequest( $request ) && empty( $request ))
            throw new SoapServiceException( 'Request can not be null or empty.', SOAP_SERVICE_ERROR_STATE_EMPTY_REQUEST );

        $this->methodName = self::ParseSoapMethod( $request, $this->namespace );

        // anonymize passwords
        //$modifiedRequest = preg_replace('~(]*>)([^<]*)()~im', '$1' . str_repeat('X', 8) . '$3', $request);

        $timer = new Timer();
        $timer->Start();

        parent::handle( $request );

        $timer->Stop();
        $this->handleCallStart = $timer->getStartTime();
        $this->handleCallDuration = sprintf( '%01.4f', $timer->getTotal() );
        unset( $timer );

        if (!$this->finalize)
            header( SOAP_HEADER_X_TRACE_ID . ': ' . $this->trace->getID() );

        $responseTrace = $this->trace->getResponseTrace();
        $responseTrace->Refresh();
        $response = $responseTrace->getBody();
        if ($this->_ProcessResponse( $response )) {
            if (empty( $response ))
                throw new SoapServiceException( 'Response can not be null or empty.', SOAP_SERVICE_ERROR_STATE_EMPTY_RESPONSE );
            // TODO: Output modified response (modify HttpResponseTrace::_GetBody())
        }

        $this->trace->Insert();

        if ($this->finalize)
            $this->trace->Finalize( $this->traceSoapAction, $this->trackingID );
    }

    protected static function ParseSoapMethod( $request, $namespace )
    {
        $matches = array( );
        // get soap namespace identifier
        /* if (preg_match('~:Envelope[^>]*xmlns:([^=]*)="urn:' . $this->nameSpace . '"~im',
          $request, $matches)) {
          // grab called method from soap request
          $pattern = '~<' . $matches[1] . ':([^\/> ]*)~im';
          if (preg_match($pattern, $request, $matches))
          $this->methodName = $matches[1];

          } */

        if (!isset( $methodName ) &&
            !preg_match( "~<([^\/> ]*) xmlns=\"$namespace\"~im", $request, $matches ))
            throw new SoapServiceException( 'SOAP message parsing failed. SOAP method not found.', SOAP_SERVICE_ERROR_STATE_METHOD_NOT_FOUND );
        else
            $methodName = $matches[ 1 ];

        unset( $matches );

        return $methodName;
    }

    protected abstract function _ProcessRequest( &$request );

    protected abstract function _ProcessResponse( &$response );
}


class SoapServiceException extends ExceptionBase
{

    private $wsdl;
    private $namespace;

    public function getWsdl()
    {
        return $this->wsdl;
    }

    public function getNamespace()
    {
        return $this->namespace;
    }

    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        if ($_code != SOAP_SERVICE_ERROR_STATE_INSTANCE_NOT_SET) {
            $wsdl = SoapService::getInstance()->getWsdl();
            $namespace = SoapService::getInstance()->getNameSpace();
            $_message = <<<EOT
$_message
WSDL: $wsdl
Namespace: $namespace
EOT;
            $this->wsdl = $wsdl;
            $this->namespace = $namespace;
        }
        parent::__construct( $_message, $_code, $_innerException );
    }
}

