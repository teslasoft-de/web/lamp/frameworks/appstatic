<?php
/**
 * Created by PhpStorm.
 * User: Cosmo
 * Date: 05.05.2015
 * Time: 21:36
 */

// TODO: Read bot list from apache configuration
global $AppStatic_KnownBots;
$AppStatic_KnownBots = array('google', 'yahoo', 'msn');

global $AppStatic_MimeTypes;
$AppStatic_MimeTypes = array(
    // archives
    'bz2' => 'application/x-bz2',
    'gz' => 'application/x-gzip',
    'jar' => 'application/x-java-archive',
    'rar' => 'application/x-rar-compressed',
    'sit' => 'application/x-stuffit',
    'tar' => 'application/x-tar',
    'zip' => 'application/zip',
    // documents
    'aps' => 'application/postscript',
    'css' => 'text/css',
    'csv' => 'text/csv',
    'doc' => 'application/msword',
    'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'eml' => 'message/rfc822',
    'htm' => 'text/html',
    'html' => 'text/html',
    'mdb' => 'application/x-msaccess',
    'odg' => 'vnd.oasis.opendocument.graphics',
    'odp' => 'vnd.oasis.opendocument.presentation',
    'ods' => 'vnd.oasis.opendocument.spreadsheet',
    'odt' => 'vnd.oasis.opendocument.text',
    'pdf' => 'application/pdf',
    'ppt' => 'application/vnd.ms-powerpoint',
    'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'ps' => 'application/postscript',
    'rtf' => 'application/rtf',
    'svg' => 'image/svg+xml',
    'ttf' => 'application/x-font-truetype',
    'txt' => 'text/plain',
    'vcf' => 'text/x-vcard',
    'xls' => 'application/vnd.ms-excel',
    'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'xml' => 'application/xml',
    // executables
    'exe' => 'application/octet-stream',
    'dmg' => 'application/x-apple-diskimage',
    'hqx' => 'application/stuffit',
    // images
    'bmp' => 'image/bmp',
    'gif' => 'image/gif',
    'jpg' => 'image/jpeg',
    'jpeg' => 'image/jpeg',
    'png' => 'image/png',
    'tif' => 'image/tiff',
    'tiff' => 'image/tiff',
    // audio
    'aif' => 'audio/x-aiff',
    'aiff' => 'audio/x-aiff',
    'm3u' => 'audio/x-mpegurl',
    'm4a' => 'audio/mp4',
    'mid' => 'audio/midi',
    'midi' => 'audio/midi',
    'mp3' => 'audio/mpeg',
    'mp4' => 'video/mp4',
    'ogg' => 'audio/ogg',
    'wav' => 'audio/wav',
    'wma' => 'audio/x-ms-wma',
    'wmv' => 'audio/x-ms-wmv',
    // video
    'mpe' => 'video/mpeg',
    'mpg' => 'video/mpeg',
    'mpeg' => 'video/mpeg',
    'mov' => 'video/quicktime',
    'avi' => 'video/x-msvideo',
    'flv' => 'video/x-flv');