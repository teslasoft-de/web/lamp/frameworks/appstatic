<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 10.01.2013
 * File: DomainName.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Web;

use AppStatic\Core\ExceptionBase;
use AppStatic\Core\PropertyBase;

/**
 * Class DomainName
 * @package AppStatic\Web
 * @name DomainName
 * @version 1.0
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
final class DomainName extends PropertyBase
{
    protected $Name;
    protected $FirstLevel;
    protected $SecondLevel;
    protected $ThirdLevel;
    private $DomainLabels;
    
    public function getName()
    {
        return $this->Name;
    }
    
    public function getFirstLevel()
    {
        return $this->FirstLevel;
    }
    
    public function getSecondLevel()
    {
        return $this->SecondLevel;
    }
    
    public function getThirdLevel()
    {
        return $this->ThirdLevel;
    }
    
    public function getLevelCount()
    {
        return count( $this->DomainLabels );
    }

    /**
     * Gets the domain level part at the specified position.
     *
     * @param integer $level
     * @return string
     */
    public function GetLabel( $level )
    {
        return $this->DomainLabels[ $level - 1 ];
    }
    
    public function __construct( $name )
    {
        try {
            $domainLabels = self::GetDomainLabels( $name );
        }Catch(DomainLevelException $ex) {
            throw new DomainLevelException( "'{$name}' is not a valid domain name.", 0, $ex );
        }
        $this->Name = $name;
        $this->FirstLevel = $domainLabels[ 0 ];
        $this->SecondLevel = "{$domainLabels[ 1 ]}.{$domainLabels[ 0 ]}";
        $this->ThirdLevel = count( $domainLabels ) > 2 ? "{$domainLabels[ 2 ]}.$this->SecondLevel" : null;
        $this->DomainLabels = $domainLabels;
    }
    
    static $current;
    
    /**
     * Gets the current http host domain.
     *
     * @return DomainName
     */
    public static function getCurrent()
    {
        if(self::$current == null)
            self::$current = new DomainName( $_SERVER[ 'HTTP_HOST' ] );
        return self::$current;
    }

    /**
     * Evaluates a domain name and splits it into its domain level parts
     *
     * @param string $domainName
     * @return array
     * @throws DomainLevelException
     */
    static function GetDomainLabels( $domainName )
    {
        $labels = explode( '.', $domainName );
        
        if (count( $labels ) < 2)
            throw new DomainLevelException( "Domain name '{$domainName}' must have at least two domain levels." );
        
        $patterns = array( 
            '/^([A-Z]{2,7}|[0-9]{1,3})$/imx',  // First level of the domain name (.com)
            '/^([-A-Z0-9ÄÖÜ]{2,63})$/imx',  // Second level of the domain name (example)
            '/^([-A-Z0-9ÄÖÜ]+)$/imx' ); // Third Level of the domain name (www.)
        $levelCount = count( $labels ) - 1;
        for( $i = $levelCount; $i >= 0; $i-- ) {
            $patternIndex = $levelCount - $i;
            $pattern = $patterns[ $patternIndex < 2 ? $patternIndex : 2 ];
            if (!preg_match( $pattern, $labels[ $i ] ))
                throw new DomainLevelException( "Domain label '{$labels[ $i ]}' (level " . ($patternIndex + 1) . ") contains invalid characters." );
        }
        
        return array_reverse( $labels );
        
    /*$matches = array();
        if (preg_match(
            '/^((?<thirdLevel>[A-Z0-9]+)\.)?        # Third Level of the domain name (www.)
            (?<secondLevel>[-A-Z0-9ÄÖÜ]+)           # Second level of the domain name (example)
            \.(?<firstLevel>[A-Z]{2,7}|[0-9]{1,3})$ # First level of the domain name (.com)
            /imx ', $domainName, $matches )) {
            $firstLevel = $matches['firstLevel'];
            $secondLevel = $matches['secondLevel'];
            $thirdLevel = $matches['thirdLevel'];
            unset($matches);
            return true;
        }
        unset($matches);
        return false;*/
    }

    /**
     * @param integer $startLevel
     * @param integer $endLevel
     * @return string
     */
    public function ToString( $startLevel, $endLevel = null )
    {
        
        $labels = array_slice( $this->DomainLabels,
                empty($startLevel) ? 0 : $startLevel - 1,
                empty($endLevel) ? count($this->DomainLabels) : $endLevel);
        return implode( '.', array_reverse( $labels ) );
        
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->Name;
    }
}

class DomainLevelException extends ExceptionBase
{
    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}
?>