<?php
/**
 * Created by PhpStorm.
 * User: Cosmo
 * Date: 05.05.2015
 * Time: 02:05
 */

namespace AppStatic\Web;

use stdClass;

require_once __DIR__ . '/Config.inc.php';

final class HttpUtility
{
    /**
     * Returns an array containing accepted languages sorted in descending order.
     * @return stdClass[] [QFactor]{Name,Country,Region}
     */
    public static function GetHttpAcceptLanguages()
    {
        $matches = array();
        $countryCodes = array();
        // Strip whitespaces
        $header = preg_replace('~\s~', '', filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE'));
        preg_match_all('/(([^-;,]*)(?:-([^;,]*))?)(?:;q=(\d*\.*\d))?,*/', $header, $matches);

        // Iterate through the matches.
        for ($i = 0; $i < count($matches[0]); $i++) {
            if ($matches[0][$i]) {
                $countyCode = new stdClass();
                $match = array();

                // Get values from groups (1 => full name, 2 => country, 3 => region, 4 => q factor)
                if(preg_match( '~^([a-z]{2})(-[A-Z]{2})*~', $matches[1][$i], $match ))
                    $countyCode->Name = $match[0];

                if(preg_match( '~^[a-z]{2}~', $matches[2][$i], $match ))
                    $countyCode->Country = $match[0];

                if(preg_match( '~^[A-Z]{2}~', $matches[3][$i], $match ))
                    $countyCode->Region = $match[0];

                if(!isset($countyCode->Name) || !isset($countyCode->Country))
                    continue;
                // Save county code indexed by q factor. (Omited q factor is default 1)
                $countryCodes[$matches[4][$i] ? $matches[4][$i] : 1] = $countyCode;
            }
        }
        // Sort descending by q factor
        krsort($countryCodes, SORT_NUMERIC | SORT_DESC);
        return $countryCodes;
    }

    /**
     *
     * @param $path
     * @return mixed|string
     */
    public static function GetFileContentType($path)
    {
        if (!file_exists($path))
            throw new \InvalidArgumentException("File '$path' does not exist.");

        // file extension
        $extension = strtolower(substr(strrchr($path, "."), 1));

        /*
          // check if allowed extension
          if (!array_key_exists($fext, $allowed_ext)) {
          die("Not allowed file type.");
          } */

        global $AppStatic_MimeTypes;

        $mimeType = '';
        // get mime type
        if ($AppStatic_MimeTypes[$extension] == '') {
            // mime type is not set, get from server settings
            if (function_exists('mime_content_type')) {
                $mimeType = mime_content_type($path);
            } else
                if (function_exists('finfo_file')) {
                    $finfo = finfo_open(FILEINFO_MIME); // return mime type
                    $mimeType = finfo_file($finfo, $path);
                    finfo_close($finfo);
                }
            if ($mimeType == '') {
                $mimeType = "application/force-download";
            }
        } else {
            // get mime type defined by admin
            $mimeType = $AppStatic_MimeTypes[$extension];
        }
        return $mimeType;
    }

    /**

     * @param $filename
     * @param bool $ifModified
     * @param array $http_headers
     * @return int
     */
    public static function ReadFile($filename, $ifModified = false, $http_headers = array())
    {

        if (!is_file($filename)) {
            // header('HTTP/1.0 404 Not Found');
            return 404;
        }

        if (!is_readable($filename)) {
            // header('HTTP/1.0 403 Forbidden');
            return 403;
        }

        $stat = @stat($filename);
        $eTag = sprintf('%x-%x-%x', $stat['ino'], $stat['size'], $stat['mtime'] * 1000000);

        header('Expires: ');
        header('Cache-Control: ');
        header('Pragma: ');

        if ($ifModified) {
            if (isset($_SERVER['HTTP_IF_NONE_MATCH']) && $_SERVER['HTTP_IF_NONE_MATCH'] == $eTag) {
                header('Etag: "' . $eTag . '"');
                // header('HTTP/1.0 304 Not Modified');
                return 304;
            } elseif (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $stat['mtime']) {
                header('Last-Modified: ' . date('r', $stat['mtime']));
                // header('HTTP/1.0 304 Not Modified');
                return 304;
            }
        }

        header('Last-Modified: ' . date('r', $stat['mtime']));
        header('Etag: "' . $eTag . '"');
        header('Accept-Ranges: bytes');
        header('Content-Length:' . $stat['size']);
        foreach ($http_headers as $header) {
            header($header);
        }

        if (@readfile($filename) === false) {
            // header('HTTP/1.0 500 Internal Server Error');
            return 500;
        } else {
            // header('HTTP/1.0 200 OK');
            return 200;
        }
    }

    //-----------------------------------------------------------------
    // HTTP Request Security
    //-----------------------------------------------------------------


    /**
     * Check if user is a bot
     *
     * @return boolean
     */
    public static function BotRequest()
    {
        global $AppStatic_KnownBots;
        //takes the list above and returns (google)|(yahoo)|(msn)
        $regex = '(' . implode($AppStatic_KnownBots, ')|(') . ')';
        /* uses the generated regex above to see if those keywords are contained in the user agent variable */
        return preg_match("/$regex/i", $_SERVER['HTTP_USER_AGENT']);
    }

    //-----------------------------------------------------------------
    // HTTP Header
    //-----------------------------------------------------------------

    public static $HttpStatusCodes = array(
        100 => "Continue",
        101 => "Switching Protocols",
        200 => "OK",
        201 => "Created",
        202 => "Accepted",
        203 => "Non-Authoritative Information",
        204 => "No Content",
        205 => "Reset Content",
        206 => "Partial Content",
        300 => "Multiple Choices",
        301 => "Moved Permanently",
        302 => "Found",
        303 => "See Other",
        304 => "Not Modified",
        305 => "Use Proxy",
        307 => "Temporary Redirect",
        400 => "Bad Request",
        401 => "Unauthorized",
        402 => "Payment Required",
        403 => "Forbidden",
        404 => "Not Found",
        405 => "Method Not Allowed",
        406 => "Not Acceptable",
        407 => "Proxy Authentication Required",
        408 => "Request Time-out",
        409 => "Conflict",
        410 => "Gone",
        411 => "Length Required",
        412 => "Precondition Failed",
        413 => "Request Entity Too Large",
        414 => "Request-URI Too Large",
        415 => "Unsupported Media Type",
        416 => "Requested range not satisfiable",
        417 => "Expectation Failed",
        500 => "Internal Server Error",
        501 => "Not Implemented",
        502 => "Bad Gateway",
        503 => "Service Unavailable",
        504 => "Gateway Time-out",
        505 => "HTTP Version Not Supported",
        506 => "Variant Also Negotiates",
        507 => "Insufficient Storage",
        508 => "Loop Detected",
        509 => "Bandwidth Limit Exceeded",
        510 => "Not Extended",
        511 => "Network Authentication Required",
        598 => "Network read timeout error",
        599 => "Network connect timeout error");

    /**
     * Returns the current http response code header.
     *
     * @param integer $code
     * @return string
     */
    public static function HttpResponseCode($code = null)
    {
        if (!$code)
            $code = http_response_code();

        if (!isset(self::$HttpStatusCodes[$code]))
            trigger_error('Unknown http response code.', E_USER_ERROR);

        return filter_input(INPUT_SERVER, 'SERVER_PROTOCOL') . " $code " . self::$HttpStatusCodes[$code];
    }

    public static function MovePage($num, $url)
    {
        http_response_code($num);
        header("Location: $url");
    }

    public static function TimeoutRedirect($timeout = 5, $target)
    {
        header("Refresh: $timeout; url=$target");
    }

    public static function Header_NoFormRevalidate()
    {
        header("Cache-Control: must-revalidate");
        $offset = 60 * 60 * 24 * -1;
        $ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
        header($ExpStr);
    }

    public static function SendNoChacheRedirect($target)
    {
        header("Location: $target", TRUE, 302);
        self::SendNoCache();
    }

    public static function SendNoCache()
    {
        header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Cache-Control: post-check=0, pre-check=0', false);
        header('Pragma: no-cache');
    }

    //-----------------------------------------------------------------
    // Web
    //-----------------------------------------------------------------

    public static function CurrentPageURL()
    {
        $pageURL = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
        $pageURL .= ($_SERVER['SERVER_PORT'] != '80' ? $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] : $_SERVER['SERVER_NAME']) . $_SERVER['REQUEST_URI'];
        return $pageURL;
    }

    public static function GetHttpResponseCode($url, $referrer,
                                               $user_agent = 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)')
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_REFERER, $referrer);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $status = array();
        $response = curl_exec($ch);
        preg_match('/HTTP\/.* ([0-9]+) .*/', $response, $status);
        return $response ? $status[1] : $response;

        /* if (!curl_exec($c)) { return false; }

          $httpcode = curl_getinfo($c, CURLINFO_HTTP_CODE);
          return ($httpcode < 400); */
    }

    public static function DownloadWebPage($url, $referrer,
                                           $user_agent = 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)')
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_REFERER, $referrer);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     *
     * @param mixed $parameters
     * @param string $numeric_prefix
     * @param null $arg_separator
     * @return string
     * @internal param $value
     */
    public static function GenerateGETParameters($parameters = null, $numeric_prefix = '_', $arg_separator = null)
    {
        if ($parameters !== null) {
            // Convert value types and objects to an array.
            if (!is_array($parameters)) {
                if (is_object($parameters)) {
                    // Strip public propertys from the object
                    $parameters = get_object_vars($parameters);
                    if (count($parameters) == 0)
                        throw new \InvalidArgumentException('Given object does not contain any public propertys.');
                } else
                    $parameters = (array)$parameters;
            }
            // Generate parameters from existing $_GET entrys and update existing parameter values.
            $parameters = array_merge($_GET, $parameters);
        } else // Use existing $_GET entrys
            $parameters = $_GET;

        $arg_separator = $arg_separator == null ? ini_get('arg_separator.output') : $arg_separator;

        return http_build_query($parameters, $numeric_prefix, $arg_separator);
    }
}