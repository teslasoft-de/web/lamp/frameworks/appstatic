<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: Autoload.inc.php
 * Encoding: UTF-8
 * Project: AppStatic
 * Author: H. Christian Kusmanow
 * Copyright: © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 * */

use AppStatic\Configuration\System;
use AppStatic\Core\ExceptionHandler;
use AppStatic\Data\MySql\StoredProcedure;
use AppStatic\Web\HttpUtility;

require_once __DIR__ . '/../Include.inc.php';

System::DieRoflCopter();
ExceptionHandler::SetSaveTrace( false );

define( 'WIKI_PAGE_DE', 'http://de.wikipedia.org/wiki/ISO_3166-1_alpha-2' );
define( 'WIKI_PAGE_EN', 'http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2' );
define( 'ISO3166_NANE_INDEX', 0 );
define( 'ISO3166_ALPHA2_INDEX', 1 );
define( 'ISO3166_ALPHA3_INDEX', 2 );
define( 'ISO3166_NUMERIC_INDEX', 3 );
define( 'ISO3166_TLD_INDEX', 4 );
define( 'ISO3166_IOC_INDEX', 5 );

define( 'ISO3166_ALPHA2_REGEX', '[A-Z]{2}' );
define( 'ISO3166_ALPHA3_REGEX', '[A-Z]{3}' );
define( 'ISO3166_NUMERIC_REGEX', '[0-9]{3}' );
define( 'ISO3166_TLD_REGEX', '\.[a-z]{2}' );
define( 'ISO3166_IOC_REGEX', '[A-Z]{3}' );

function GetNodeValue( $node, $pattern, &$reserved )
{
    $value = null;
    $nodeValue = $node->nodeValue;
    if (($node != null && $nodeValue != '')) {
        $matches = array();
        $temp = '/' . $pattern . '(?![¹²³])/';
        if (preg_match( $temp, utf8_decode( $nodeValue ), $matches ))
            $value = $matches[ 0 ];
        else {
            preg_match( "/$pattern/", $nodeValue, $matches );
            $value = implode( ' ', $matches );
            $reserved = true;
        }
    }
    return $value;
}

###################################################################

$pageContent = HttpUtility::DownloadWebPage(WIKI_PAGE_DE, 'http://de.wikipedia.org/');
if ($pageContent == null) {
    $lastError = error_get_last();
    $message = 'Wiki page not found<br/>';
    if (isset( $lastError[ 'message' ] ))
        $message .= $lastError[ 'message' ];
    die( $message );
}

$document = new DOMDocument();
@$document->loadXML( $pageContent );
$tableElements = $document->getElementsByTagName( 'table' );

$table = null;
$index = 0;
do {
    /** @var DOMElement $table */
    $table = $tableElements->item( $index++ );
    if ($table != null && $table->hasAttributes() && $table->getAttribute( 'class' ) == 'wikitable sortable')
        break;

}
while ($table != null);

if ($table == null)
    die( 'Content table not found!' );

$rows = $table->getElementsByTagName( 'tr' );
$validCodes = array();
$reservedCodes = array();

/** @var DOMElement $row */
foreach( $rows as $row ) {
    
    $reserved = false;
    $cols = $row->getElementsByTagName( 'td' );
    
    // Country name
    /** @var DOMElement $nameNode */
    $nameNode = $cols->item( ISO3166_NANE_INDEX );
    if ($nameNode == null)
        continue;
    
    $links = $nameNode->getElementsByTagName( 'a' );
    $link = $links->item( 0 );
    if ($link == null)
        continue;
    
    $name = utf8_decode( $link->nodeValue );
    
    // ALPHA2
    $alpha2Node = $cols->item( ISO3166_ALPHA2_INDEX );
    if ($alpha2Node == null || $alpha2Node->nodeValue == '')
        continue;
    $alpha2 = GetNodeValue( $alpha2Node, ISO3166_ALPHA2_REGEX, $reserved );
    
    // ALPHA3
    $alpha3 = GetNodeValue( $cols->item( ISO3166_ALPHA3_INDEX ), ISO3166_ALPHA3_REGEX, $reserved );
    
    // NUMERIC
    $numeric = GetNodeValue( $cols->item( ISO3166_NUMERIC_INDEX ), ISO3166_NUMERIC_REGEX, $reserved );
    
    // TLD
    $tld = GetNodeValue( $cols->item( ISO3166_TLD_INDEX ), ISO3166_TLD_REGEX, $reserved );
    
    // IOC
    $ioc = GetNodeValue( $cols->item( ISO3166_IOC_INDEX ), ISO3166_IOC_REGEX, $reserved );
    
    $values = array( 
        ISO3166_NANE_INDEX => $name, 
        ISO3166_ALPHA2_INDEX => $alpha2, 
        ISO3166_ALPHA3_INDEX => $alpha3, 
        ISO3166_NUMERIC_INDEX => $numeric, 
        ISO3166_TLD_INDEX => $tld, 
        ISO3166_IOC_INDEX => $ioc );
    
    if ($reserved)
        $reservedCodes[] = $values;
    else
        $validCodes[] = $values;
}

PrintCodes( 'RESERVED', $reservedCodes );

PrintCodes( 'VALID', $validCodes );

//InsertCodes( $validCodes );

function PrintCodes( $name, $codes )
{
    $count = count( $codes );
    echo <<<EOT
<br/>
$name ($count):
<br/><br/>

EOT;
    
    foreach( $codes as $code ) {
        echo sprintf( <<<EOT
%s - %s - %s - %s - %s - %s<hr/>

EOT
            ,
            $code[ ISO3166_NANE_INDEX ],
            $code[ ISO3166_ALPHA2_INDEX ],
            $code[ ISO3166_ALPHA3_INDEX ],
            $code[ ISO3166_NUMERIC_INDEX],
            $code[ ISO3166_TLD_INDEX ],
            $code[ ISO3166_IOC_INDEX ] );
    }
}

function InsertCodes( $codes )
{
    /** @noinspection PhpIncludeInspection */
    require_once '../WebStatic/config/config.inc.php';
    /** @noinspection PhpUndefinedFunctionInspection */
    mysqli_connect_www_siteadmin();

    $procedure = new StoredProcedure( 'TruncateCountryCodes' );
    $procedure->Prepare();
    $procedure->Execute();
    unset( $procedure );
    
    $procedure = new StoredProcedure( 'InsertCountryCode' );
    $null = null;
    foreach( $codes as $code ){
        $procedure->Prepare(
            $code[ ISO3166_ALPHA2_INDEX ],
            $code[ ISO3166_ALPHA3_INDEX ],
            $code[ ISO3166_NUMERIC_INDEX],
            $code[ ISO3166_TLD_INDEX ],
            $code[ ISO3166_IOC_INDEX ],
            $null,
            $code[ ISO3166_NANE_INDEX ] );
        $procedure->Execute();
    }
}
