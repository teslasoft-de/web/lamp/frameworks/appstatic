<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: DateTimeUtility.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Globalization;


use DateInterval;
use DatePeriod;

final class DateTimeUtility{

    /**
     * Generates the duration as unix time stamp ticks for the specified days.
     *
     * @param integer $days
     * @return integer
     */
    public static function GetUnixTimeByDays($days)
    {
        if (!is_numeric($days) || $days < 1)
            throw new \InvalidArgumentException('Invalid days');

        return 60 * 60 * 24 * $days;
    }

    public static function GetAge($day, $month, $year)
    {
        $year = date("Y") - $year;
        $month = date("m") - $month;
        $day = date("d") - $day;
        if ($day < 0 || $month < 0)
            $year--;
        return $year;
    }

    public static function DateAfterWorkingDays($date, $workingdays = 5)
    {
        $day = 60 * 60 * 24;
        $week = $day * 7;

        $weeks = floor($workingdays / 7);
        $days = $workingdays % 7;
        $weekday = null;

        while ($days > 0) {
            $date += $day;
            $weekday = (integer)date("w", $date);
            if ($weekday != 0 && $weekday != 6)
                $days--;
        }
        $date += ($weeks * $week);

        return $date;
    }

    public static function MonthTimespan($beginDate, $endDate)
    {
        $ts1 = strtotime($endDate);
        $ts2 = strtotime($beginDate);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        return ((($year2 - $year1) * 12) + ($month2 - $month1)) * -1;
    }

    /**
     * Returns the timespan very precise between two date strings.
     * @param $format
     * @param string $beginDate
     * @param string $endDate
     * @return int
     */
    public static function DateTimespan($beginDate, $endDate, $format = '1 month')
    {
        $begin = new \DateTime($beginDate);
        $end = new \DateTime($endDate);
        $end = $end->modify('+' . $format);

        $interval = DateInterval::createFromDateString($format);

        $period = new DatePeriod($begin, $interval, $end);
        $counter = 0;
        /** @noinspection PhpUnusedLocalVariableInspection */
        foreach ($period as $dt)
            $counter++;

        return $counter;
    }

    /**
     * Format an interval to show all existing components.
     * If the interval doesn't have a time component (years, months, etc)
     * That component won't be displayed.
     *
     * @param DateInterval $interval The interval
     *
     * @return string Formatted interval string.
     */
    public static function FormatInterval(DateInterval $interval)
    {
        $result = "";
        if ($interval->y) {
            $result .= $interval->format("%y years ");
        }
        if ($interval->m) {
            $result .= $interval->format("%m months ");
        }
        if ($interval->d) {
            $result .= $interval->format("%d days ");
        }
        if ($interval->h) {
            $result .= $interval->format("%h hours ");
        }
        if ($interval->i) {
            $result .= $interval->format("%i minutes ");
        }
        if ($interval->s) {
            $result .= $interval->format("%s seconds ");
        }

        return $result;
    }
}