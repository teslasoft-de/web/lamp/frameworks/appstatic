<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: Autoload.inc.php
 * Encoding: UTF-8
 * Project: AppStatic
 * Author: H. Christian Kusmanow
 * Copyright: © 2009 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 * */

define( 'ENCODING_UTF_8', 'UTF-8' );

/**
 * UTF-8 aware version of htmlentities()
 * @param $text
 * @param int $quote
 * @param string $charset
 * @param bool $double_encode
 * @return string
 */
function utf8_htmlentities( $text, $quote = ENT_COMPAT, $charset = 'UTF-8', $double_encode = TRUE )
{
    return htmlentities( $text, $quote, $charset, $double_encode );
}

/**
 * UTF-8 aware version of html_entity_decode()
 * @param $text
 * @param int $quote
 * @param string $charset
 * @return string
 */
function utf8_html_entity_decode( $text, $quote = ENT_COMPAT, $charset = 'UTF-8' )
{
    return html_entity_decode( $text, $quote, $charset );
}

/**
 * UTF-8 aware version of htmlspecialchars()
 * @param $text
 * @param int $quote
 * @param string $charset
 * @param bool $double_encode
 * @return string
 */
function utf8_htmlspecialchars( $text, $quote = ENT_COMPAT, $charset = 'UTF-8',
    $double_encode = TRUE )
{
    return htmlspecialchars( $text, $quote, $charset, $double_encode );
}

/**
 *  Converts the given string to an UTF-8 string.
 *
 * @param string $string The ISO-8859-1|GBK encoded string
 * @return string The UTF-8 converted string
 */
function convToUtf8( $string )
{
    if ($string === null)
        return null;

    $detectedEncoding = mb_detect_encoding( $string, 'UTF-8, ISO-8859-1, GBK' );

    if ($detectedEncoding === false)
        throw new \InvalidArgumentException(
            sprintf( 'Target encoding %s for UTF-8 conversion is not supported.', mb_detect_encoding( $string, "auto" ) ) );

    return $detectedEncoding != ENCODING_UTF_8 ? iconv( $detectedEncoding, ENCODING_UTF_8, $string ) : $string;
}

/**
 *  Converts the given string to an Latin1 string.
 *
 * @param string $string The UTF-8 encoded string
 * @return string The Latin1 converted string
 */
function convToLatin1( $string )
{
    if ($string === null)
        return null;

    $detectedEncoding = mb_detect_encoding( $string, 'ISO-8859-1, UTF-8' );
    if ($detectedEncoding === false)
        throw new \InvalidArgumentException(
            sprintf( 'Target encoding %s for Latin1 conversion is not supported.', mb_detect_encoding( $string, "auto" ) ) );

    return $detectedEncoding == 'ISO-8859-1' ? utf8_decode( $string ) : $string;
}

/**
 * Converts a Unicode Codepoint to a literal UTF-8 character.
 *
 * @param int $codepoint The Unicode codepoint
 * @return string The UTF-8 converted string
 */
function codepointToUtf8($codepoint)
{
    if ($codepoint < 0x7F) // U+0000-U+007F - 1 byte
        return chr($codepoint);
    if ($codepoint < 0x7FF) // U+0080-U+07FF - 2 bytes
        return chr(0xC0 | ($codepoint >> 6)) . chr(0x80 | ($codepoint & 0x3F));
    if ($codepoint < 0xFFFF) // U+0800-U+FFFF - 3 bytes
        return chr(0xE0 | ($codepoint >> 12)) . chr(0x80 | (($codepoint >> 6) & 0x3F)) . chr(0x80 | ($codepoint & 0x3F));
    else // U+010000-U+10FFFF - 4 bytes
        return chr(0xF0 | ($codepoint >> 18)) . chr(0x80 | ($codepoint >> 12) & 0x3F) . chr(0x80 | (($codepoint >> 6) & 0x3F)) . chr(0x80 | ($codepoint & 0x3F));

    //throw new Exception('The codepoint is outside of Unicode range.');
}

/**
 * Returns the Unicode string of specified Unicode code. This is the inverse of ord() for Unicode strings.
 *
 * @param integer $code The Unicode code
 * @return string
 */
function unichr($code) {
    return iconv('UCS-4LE', 'UTF-8', pack('V', $code));
}