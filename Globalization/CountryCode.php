<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: CountryCode.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Globalization;

use AppStatic\Core\ExceptionBase;
use AppStatic\Core\PropertyBase;
use AppStatic\Data\MySql\StoredProcedure;

final class CountryCode extends PropertyBase
{
    protected $ID;
    protected $Iso3166Alpha2;
    protected $Iso3166Alpha3;
    protected $Iso3166Numeric;
    protected $TLD;
    protected $IOC;
    protected $NameEN;
    protected $NameDE;
    
    public function getID()
    {
        return $this->ID;
    }
    
    public function getIso3166Alpha2()
    {
        return $this->Iso3166Alpha2;
    }
    
    public function getIso3166Alpha3()
    {
        return $this->Iso3166Alpha3;
    }
    
    public function getTLD()
    {
        return $this->TLD;
    }
    
    public function getIOC()
    {
        return $this->IOC;
    }
    
    public function getNameEN()
    {
        return $this->NameEN;
    }
    
    public function getNameDE()
    {
        return $this->NameDE;
    }
    
    private function __construct()
    {

    }

    /**
     * Querys ISO 3166 country codes from content table.
     * @param string $iso3166Alpha2List Comma separated list of ISO 3166 Alpha 2 country codes.
     * @return array
     * @throws CountryCodeException
     */
    public static function QueryCountryCodes( $iso3166Alpha2List = null )
    {
        $countryCodes = array();
        $procedure = null;
        try{
            $procedure = new StoredProcedure( 'QueryCountryCodes', APPSTATIC_DATABASE );
            $procedure->Prepare( $iso3166Alpha2List );
            $procedure->Execute();
            $countryCode = null;
            do{
                $countryCode = new CountryCode();
                $procedure->BindResult( array(
                    &$countryCode->ID,
                    &$countryCode->Iso3166Alpha2,
                    &$countryCode->Iso3166Alpha3,
                    &$countryCode->Iso3166Numeric,
                    &$countryCode->TLD,
                    &$countryCode->IOC,
                    &$countryCode->NameEN,
                    &$countryCode->NameDE ) );
                if($procedure->Fetch( true ) )
                    $countryCodes []= $countryCode;
                else unset($countryCode);
            }while( isset( $countryCode ) );
            unset( $procedure );
        }
        catch( \Exception $ex ){
            unset( $procedure );
            throw new CountryCodeException( 'Fehler beim Abrufen der Ländercodes', 0, $ex );
        }
        
        return count($countryCodes) == 1 ? $countryCodes[0] : $countryCodes;
    }
    
    public function __toString()
    {
        return utf8_htmlentities( $this->NameDE );
    }
}

final class CountryCodeException extends ExceptionBase
{
    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}
?>