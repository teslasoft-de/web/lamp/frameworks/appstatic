<?php

namespace AppStatic\Core;

use Exception;

require_once APPSTATIC_PATH . '/Core/PHPException.php';

/**
 * @package AppStatic
 * @name ExceptionBase
 * @version 1.0 (2009-01-01)
 * 
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow
 *
 */
class ExceptionBase extends \Exception
{
    protected $InnerExeption;

    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code );
        $this->InnerException = $_innerException;
    }

    public function GetInnerException()
    {
        return $this->InnerException;
    }

    /**
     * Returnes the entire stack trace including all inner exceptions. 
     *
     * @return array
     */
    public function GetStackTrace()
    {
        if ($this->InnerException !== null) {
            $arr = array( );
            //			$trace = $this->getTrace();
            //			$arr []= $trace[0];
            //			unset($trace);
            if ($this->InnerException instanceof self) {
                foreach ($this->InnerException->GetStackTrace() as $trace)
                    $arr [] = $trace;
            }
            else
                foreach ($this->InnerException->getTrace() as $trace)
                    $arr [] = $trace;
            return $arr;
        }
        else {
            return $this->getTrace();
        }
    }

    /**
     * Gets a detailed html formatted exception message for this instance.
     *
     * @return string
     */
    public function GenerateMessage()
    {
        return ExceptionHandler::GenerateExceptionMessage( $this, $this->getTrace() );
    }

    /**
     * Gets detailed html formatted exception massages including all inner exceptions.
     *
     * @return string
     */
    public function GenerateMessages()
    {
        return ExceptionHandler::GenerateMessages( $this );
    }

    /**
     * Returns a String that represents the current Object.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getMessage();
    }

}
require_once __DIR__ . '/ExceptionHandler.php';
?>