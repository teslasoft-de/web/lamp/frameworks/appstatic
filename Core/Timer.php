<?php

namespace AppStatic\Core;

use AppStatic\Core;

define('TIMER_FORMAT_S', '%01.3fs');
define('TIMER_FORMAT_MS', '%01.3fms');

/**
 * Measures time intervals between two events.
 *
 * @author Christian Kusmanow
 */
final class Timer extends Core\PropertyBase
{
    protected $StartTime;
    protected $EndTime;
    protected $Total;
    protected $Format;
    protected $TotalFormat;

    /**
     * Returns the start time formatted by the Timer->Format member value.
     * @param bool $format
     * @return string
     */
    public function getStartTime($format = true)
    {
        return $format && is_string($this->Format) ? sprintf($this->Format, $this->StartTime) : $this->StartTime;
    }

    /**
     * Returns the end time formatted by the Timer->Format member value.
     * @param bool $format
     * @return string
     */
    public function getEndTime($format = true)
    {
        return $format && is_string($this->Format) ? sprintf($this->Format, $this->EndTime) : $this->EndTime;
    }

    /**
     * Returns the time span between the start and end time formatted in milliseconds, seconds
     * or by the Timer->TotalFormat member value.
     * @param bool $format If 'true' the time span should be formatted returned.
     * @return string
     */
    public function getTotal($format = true)
    {
        if ($format)
            return sprintf(is_string($this->TotalFormat)
                ? $this->TotalFormat
                : $this->Total <= 0.001 ? TIMER_FORMAT_MS : TIMER_FORMAT_S,
                $this->Total <= 0.001 ? $this->Total * 1000 : $this->Total);
        else
            return $this->Total;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->Format;
    }

    /**
     * @param string $value
     */
    public function setFormat($value)
    {
        $this->Format = $value;
    }

    /**
     * @return string
     */
    public function getTotalFormat()
    {
        return $this->TotalFormat;
    }

    /**
     * @param string $value
     */
    public function setTotalFormat($value)
    {
        $this->TotalFormat = $value;
    }

    /**
     * @param string $format
     * @param string $totalFormat
     */
    function __construct($format = null, $totalFormat = null)
    {
        $this->Format = $format;
        $this->TotalFormat = $totalFormat;
    }

    public function Start()
    {
        $time = microtime(true);

        $this->StartTime = $time;
    }

    public function Stop($format = true)
    {
        $this->EndTime = microtime(true);
        $this->Total = ($this->EndTime - $this->StartTime);
        return $this->getTotal($format);
    }

    public function __toString()
    {
        return $this->getTotal();
    }

    public static function GetScriptExceutionTime()
    {
        $time = microtime(true) - filter_var( $_SERVER['REQUEST_TIME_FLOAT'] );
        return floor($time) . '.' . sprintf( "%06d", ($time - floor($time)) * 1000000);
    }
}
