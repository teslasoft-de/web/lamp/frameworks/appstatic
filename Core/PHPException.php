<?php

namespace AppStatic\Core;

use ErrorException;

require_once __DIR__ . '/ExceptionBase.php';

final class PHPException extends \ErrorException
{
    public function __construct( $code, $message, $file, $line, $context = null )
    {
        $severity =
            1 * E_ERROR |
            1 * E_WARNING |
            1 * E_PARSE |
            0 * E_NOTICE |
            1 * E_CORE_ERROR |
            0 * E_CORE_WARNING |
            1 * E_COMPILE_ERROR |
            0 * E_COMPILE_WARNING |
            1 * E_USER_ERROR |
            0 * E_USER_WARNING |
            0 * E_USER_NOTICE |
            0 * E_STRICT |
            1 * E_RECOVERABLE_ERROR |
            0 * E_DEPRECATED |
            0 * E_USER_DEPRECATED;        
        
        parent::__construct( $message, $code, (integer) (($severity & $code) == $code), $file, $line, null );
        $this->m_arContext = $context;
    }
    
    public static function HandleError( $code, $message, $file, $line, $context )
    {
        // error was suppressed with the @-operator
        if (0 === error_reporting())
            return false;
                
        $ex = new PHPException( $code, $message, $file, $line, $context );
        if($ex->severity > 0)
            throw $ex;
        
        if(ini_get( 'display_errors' ))
            ExceptionHandler::HandleException ( $ex );
        else
            ExceptionHandler::Log( $ex );
        return null;
    }
    
    public static function HandleFatalError()
    {
        $error = error_get_last();
        if (empty( $error )){
            ExceptionHandler::PrintExceptionPage();
            return;
        }
        
        $ex = new PHPException( $error[ 'type' ], $error[ 'message' ], $error[ 'file' ], $error[ 'line' ] );
        
        if($ex->severity > 0 || ini_get( 'display_errors' ))
            ExceptionHandler::HandleException ( $ex );
        else
            ExceptionHandler::Log( $ex );
        
        ExceptionHandler::PrintExceptionPage();
    }
    
    public static function ErrnoToString($errno)
    {
        switch ($errno) {
            case 1:     $e_type = 'ERROR'; break;
            case 2:     $e_type = 'WARNING'; break;
            case 4:     $e_type = 'PARSE'; break;
            case 8:     $e_type = 'NOTICE'; break;
            case 16:    $e_type = 'CORE_ERROR'; break;
            case 32:    $e_type = 'CORE_WARNING'; break;
            case 64:    $e_type = 'COMPILE_ERROR'; break;
            case 128:   $e_type = 'COMPILE_WARNING'; break;
            case 256:   $e_type = 'USER_ERROR'; break;
            case 512:   $e_type = 'USER_WARNING'; break;
            case 1024:  $e_type = 'USER_NOTICE'; break;
            case 2048:  $e_type = 'STRICT'; break;
            case 4096:  $e_type = 'RECOVERABLE_ERROR'; break;
            case 8192:  $e_type = 'DEPRECATED'; break;
            case 16384: $e_type = 'USER_DEPRECATED'; break;
            case 30719: $e_type = 'ALL'; break;
            default:    $e_type = "UNKNOWN($errno)"; break;
        }
        return "$e_type";
    }
}
// Register PHPException as global PHP error handler.
set_error_handler( array('\AppStatic\Core\PHPException', 'HandleError' ) );
register_shutdown_function( array('\AppStatic\Core\PHPException', 'HandleFatalError' ) );

// PHP logging will be done only through the class ExceptionHandler.
ini_set( 'log_errors', false );