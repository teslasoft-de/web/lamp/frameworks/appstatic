<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 17.01.2014
 * File: ErrorTrap.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Core;
use Exception;


/**
 * Description of ErrorTrap
 * 
 * @package AppStatic
 * @name ErrorTrap
 * @version 0.1 (17.01.2014 15:13:36)
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 * 
 * @example
 * // create a DOM document and load the HTML data
 * $xmlDoc = new DomDocument();
 * $caller = new ErrorTrap(array($xmlDoc, 'loadHTML'));
 * // this doesn't dump out any warnings
 * $caller->call($fetchResult);
 * if (!$caller->ok()) {
 *     var_dump($caller->errors());
 * } 
 */
class ErrorHandler
{
    protected $Callback;
    protected $Errors = array();

    function __construct( $callback )
    {
        $this->Callback = $callback;
    }

    function Invoke()
    {        
        set_error_handler( array( $this, 'OnError' ) );
        $result = null;
        try {
            $result = call_user_func_array( $this->Callback, func_get_args() );
        } catch (Exception $ex) {
            $result = $ex;
        }
        restore_error_handler();
        if($result instanceof Exception)
            throw $result;
        return $result;
    }

    function OnError( $code, $message, $file, $line, $context )
    {
        $this->Errors[] = new PHPException( $code, $message, $file, $line, $context );
    }

    public function getHasErrors()
    {
        return count( $this->Errors ) !== 0;
    }

    /**
     * 
     * @return PHPException[]
     */
    public function getErrors()
    {
        return $this->Errors;
    }

}
