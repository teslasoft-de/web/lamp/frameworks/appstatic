<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: PropertyBase.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Core;

/**
 * @package AppStatic
 * @name PropertyBase
 * @version 1.0
 *
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow
 *
 */
abstract class PropertyBase
{
    final public function __get($propertyName)
    {
        $class = get_class($this);

        // validate if property exists
        if (!property_exists($class, $propertyName))
            throw new PropertyBaseExeption("Member $class::$propertyName does not exist.");

        // validate if property is readable
        if ($propertyName[0] == strtolower($propertyName[0]))
            throw new PropertyBaseExeption("Member $class::$propertyName is not publicly readable.");

        //use custom get method if available
        if (method_exists($class, ($method = "get" . $propertyName)) || method_exists($class, ($method = "Get" . $propertyName)))
            return $this->$method();

        //or return the value directly
        return $this->$propertyName;
    }

    final public function __set($propertyName, $val)
    {
        $class = get_class($this);

        // validate if property exists
        if (!property_exists($class, $propertyName))
            throw new PropertyBaseExeption("Class member $class::$propertyName does not exist.");

        // Cancel if the member variable lower case, which means private.
        if ($propertyName[0] == strtolower($propertyName[0]))
            throw new PropertyBaseExeption("Class member $class::$propertyName is not publicly writable.");

        if (!method_exists($class, ($method = "set" . $propertyName)) & !method_exists($class, ($method = "Set" . $propertyName)))
            throw new PropertyBaseExeption("No setter method for Property $class::$propertyName found.");

        $this->$method($val);
    }

    /**
     * Returns a String that represents the current Object.
     *
     * @return string
     */
    public function __toString()
    {
        return self::getClassName();
    }

    /**
     * Serves as a hash function for a particular type.
     *
     * @return string
     */
    public function getHashCode()
    {
        return spl_object_hash($this);
    }

    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * Converts all public object properties to an array
     * and does recursive conversion for all object and array properties.
     *
     * @param null $obj
     * @return array
     */
    public function ToArray($obj = null)
    {
        if(!$obj)
            return self::ToArray($this);

        $array = array();
        foreach ($obj as $key => $value) {
            if (is_object($value) || is_array($value))
                $array[$key] = self::ToArray($value);
            else
                $array[$key] = $value;
        }
        return $array;
    }

    /**
     * Converts the current object to xml.
     *
     * @return string
     */
    public function ToXml()
    {

    }
}

final class PropertyBaseExeption extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}