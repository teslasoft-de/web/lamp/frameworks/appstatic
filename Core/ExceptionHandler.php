<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose ArrayUtility | Templates
 * and open the template in the editor.
 */

namespace AppStatic\Core;

require_once APPSTATIC_PATH . '/Globalization/UTF8.inc.php';

use AppStatic\Web\HtmlUtility;
use AppStatic\Web\HttpUtility;
use Exception;

/**
 * Description of ExeptionHandler
 *
 * @author Cosmo
 */
final class ExceptionHandler
{
    private static $saveTrace = true;

    /**
     * Gets a boolean value indicating weather Save Trace is enabled.
     *
     * @return boolean
     */
    public static function GetSaveTrace()
    {
        return self::$saveTrace;
    }

    /**
     * Sets the Save Trace state.
     *
     * @param boolean $value
     */
    public static function SetSaveTrace( $value )
    {
        self::$saveTrace = $value;
        /* if( !ONLINE_SESSION )
          echo '<pre>Save trace ' . ($value ? 'enabled' : 'disabled') . '<br/>'. print_r( debug_backtrace(), true) . '</pre><br/>'; */
    }

    public static function CreateLogTimeStamp()
    {
        $time = microtime(true);
        $microseconds = sprintf( "%06d", ($time - floor($time)) * 1000000 );
        $dateTime = new \DateTime( date( 'Y-m-d H:i:s.' . $microseconds, $time ) );
        return $dateTime->format( "Y-m-d H:i:s.u O" );
    }

    /**
     * Handles the exception by printing detailed information as html.
     *
     * @param Exception $e
     * @param bool $return
     * @return null|string
     */
    public static function HandleException( Exception $e, $return = false )
    {
        // Create and cache exception timestamp for error log.
        self::$ExceptionTimeStamps[ spl_object_hash( $e ) ] = self::CreateLogTimeStamp();

        self::Log( $e );
        return self::PrintException( $e, $return );
    }

    /**
     * Writes a message to the php error log. (HTML will be converted to text)
     * @param Exception $e
     */
    public static function Log( Exception $e )
    {
        $objHash = spl_object_hash( $e );
        $timestamp = isset(self::$ExceptionTimeStamps[ $objHash ])
            ? self::$ExceptionTimeStamps[ $objHash ]
            : self::CreateLogTimeStamp();

        $saveTrace = self::GetSaveTrace();
        // If save trace is on, disable it and regenerate the messages.
        if($saveTrace)
            self::SetSaveTrace( false );

        $message = self::PrintException ($e, true);

        // Convert the error message to text.
        # convert br to nl
        $message = preg_replace('~\r\n~', PHP_EOL, HtmlUtility::br2nl($message));
        # strip html tags
        $message = utf8_html_entity_decode( strip_tags( $message ) );
        # convert utf-8 &nbsp; to space
        $message = str_replace("\xC2\xA0", ' ', $message );
        # remove white space at start
        $message = preg_replace('~^\s*\n\s*~', '', $message );
        # fix whitespaces
        $message = preg_replace('~\s*\n\s*~', "\n", $message );
        # indent all lines except trace
        $message = preg_replace('~\n(?=[A-Z_]+)~', "\n ", $message );
        # indent all trace lines except last
        $message = preg_replace('~\n(?![\sA-Z_]+|$)~', "\n  ", $message );
        # strip exception date
        $message = preg_replace('~^(.*)\n\s*~', '', $message);

        $phpScript = filter_input(INPUT_SERVER, 'SERVER_NAME') . filter_input(INPUT_SERVER, 'REQUEST_URI');
        error_log( '[' . $timestamp . '] ' . filter_input( INPUT_SERVER, 'REMOTE_ADDR') . " $phpScript\n $message", 3, ini_get( 'error_log' ) );

        self::SetSaveTrace( $saveTrace );
    }

    /**
     * Prints detailed exception information as html.
     *
     * @param Exception $e
     * @param bool $return
     * @return null|string
     */
    static function PrintException( Exception $e, $return = false )
    {
        if ($return)
            return self::GenerateMessages( $e );

        self::$Exceptions []= $e;

        return null;
    }

    static $Exceptions = array();
    static $ExceptionTimeStamps = array();

    public static function PrintExceptionPage()
    {
        if(count(self::$Exceptions) == 0){
            if(!ob_get_contents()){
                $responseHeader = HttpUtility::HttpResponseCode();
                $time = Timer::GetScriptExceutionTime();
                echo "Request processed in $time seconds with status: $responseHeader";
            }
            return;
        }

        $exceptionMessages = array();
        foreach (self::$Exceptions as $e)
            $exceptionMessages []= self::GenerateMessages( $e );

        $exceptionMessages = implode(
            file_get_contents(__DIR__ . '/ExceptionHandler.MessageDetails.phtml')
            , $exceptionMessages );

        $severenity = false;
        foreach (self::$Exceptions as $e){
            if(!($e instanceof \ErrorException) || $e->getSeverity() > 0){
                $title = 'Website script execution stopped. ';
                $title .= count(self::$Exceptions) > 1
                    ? 'Uncaught exceptions have been thrown.'
                    : 'An uncaught exception has been thrown.';
                $severenity = true;
                break;
            }
        }

        if(empty($title))
            $title = count(self::$Exceptions) > 1
                ? 'Uncaught exceptions have been thrown.'
                : 'An uncaught exception has been thrown.';

        // TODO: Merge error messages to existing content as dockable window (Bootstrap Dialog).
        if(ob_get_length() > 0){
            $content = ob_get_contents();
            ob_clean();
        }
        http_response_code(500);
        $logTitle = filter_input(INPUT_SERVER, 'SERVER_NAME') . " - Internal Server Error";
        $hostmaster = filter_input(INPUT_SERVER, 'SERVER_ADMIN') . '?subject=' . rawurlencode($logTitle);
        $message = String::Replace(
            file_get_contents(__DIR__ . '/ExceptionHandler.MessageDetails.phtml'),
            [
                '$logTitle' => $logTitle,
                '$title' => $title,
                '$exceptionMessages' => $exceptionMessages,
                '$hostmaster' => $hostmaster,
            ]);

        if($severenity) {
            if(!isset($_SERVER['ERROR_DOCUMENT']))
                trigger_error('Custom error pages are not available: Environment variable ERROR_DOCUMENT is not defined.', E_USER_NOTICE);
            else {
                $filename = "{$_SERVER['DOCUMENT_ROOT']}/{$_SERVER['ERROR_DOCUMENT']}";
                if (is_file( $filename )) {
                    include $filename;

                    return;
                }
            }
        }

        if (!empty($message)) {
            $replace = String::Replace(
                file_get_contents(__DIR__ . '/ExceptionHandler.Message.phtml'),
                [
                    '$title'   => $title,
                    '$message' => $message
                ]);
            echo $replace;
        }
        if(!empty($content))
            echo $content;
    }

    public static function GenerateMessages( Exception $e, &$messages = null )
    {
        if(!$messages)
            $messages = ExceptionHandler::GenerateExceptionMessage( $e, $e->getTrace() );

        $ie = $e instanceof ExceptionBase ? $e->GetInnerException() : $e->getPrevious();
        if ($ie != null)
            $messages .= self::GenerateMessages( $ie );

        return $messages;
    }

    /**
     * Generates an exception message formatted in html.
     *
     * @param Exception $e
     * @param array $stackTrace
     * @return string
     */
    public static function GenerateExceptionMessage( Exception $e, $stackTrace )
    {
        $type = get_class( $e );
        $code = PHPException::ErrnoToString( $e->getCode() );
        $message = nl2br( utf8_htmlentities( $e->getMessage() ) );
        $trace = '';

        if (self::GetSaveTrace()) {
            //$file = basename( $e->getFile() );
            $trace = <<<HTML
<tr valign="top" style="color: #00D0FF;font-family: 'trebuchet ms', arial">
    <td>Save Trace is on. You don't have the permission to view the entire trace.</td>
</tr>
HTML;
            // Remove all insecure information from error message.
            # strip direcories
            $message = preg_replace( '~/[\w\d\-_/]+/~', '', $message );
            # strip namespaces
            $message = preg_replace( '~[\w\d_\\\\]+\\\\~', '', $message );
        }
        else {
            $i = 0;

            if(count($stackTrace) > 1){
                if(!isset($stackTrace[0]['file']))
                    unset($stackTrace[0]);
                // Hide PHPException trace.
                if($e instanceof PHPException){
                    if(isset( $stackTrace[0]['class']) && isset( $stackTrace[1]['class']))
                        $stackTrace[0]['class'] = $stackTrace[1]['class'];
                    else
                        unset($stackTrace[0]['class']);
                    if(isset( $stackTrace[0]['type']) && isset( $stackTrace[1]['type']))
                        $stackTrace[0]['type'] = $stackTrace[1]['type'];
                    else
                        unset($stackTrace[0]['type']);
                    if(isset( $stackTrace[0]['function']) && isset( $stackTrace[1]['function']))
                        $stackTrace[0]['function'] = $stackTrace[1]['function'];
                    if(isset( $stackTrace[0]['args']) && isset( $stackTrace[1]['args']))
                        $stackTrace[0]['args'] = $stackTrace[1]['args'];
                }
            }elseif(!isset($stackTrace[0]['file']))
                $stackTrace[0]['file'] = $e->getFile();

            $traces = array();
            $callStackDepth = count($stackTrace) +1;
            foreach ($stackTrace as $traceLine)
                $traces []= self::GenerateTrace( $traceLine, $callStackDepth-- );

            foreach ($traces as $array)
                $trace .= <<<HTML
<tr valign="top" style="color: #00D0FF;">
    <td rowspan="2" style="vertical-align: top">{$array['Trace']}&nbsp;</td><td>{$array['Function']}</td>
</tr>
<tr valign="top">
    <td>{$array['File']}</td>
</tr>
HTML;

            $trace .= <<<HTML
<tr valign="top" style="color: #00D0FF;">
    <td>#$callStackDepth&nbsp;</td><td>{main}</td>
</tr>
HTML;
            unset( $i );
        }

        $color = 'color: ' . !($e instanceof \ErrorException) || $e->getSeverity() > 0 ? '#ff2d2d' : '#ffe501';
        $hash = spl_object_hash( $e );
        $timeStamp = isset(self::$ExceptionTimeStamps[ $hash ]) ? self::$ExceptionTimeStamps[ $hash ] : null;
        $htmldoc = <<<HTML
<tr valign="top" style="font-weight: bold;">
    <td rowspan="3" style="white-space: nowrap;">$timeStamp&nbsp;</td>
    <td rowspan="3" style="$color; letter-spacing: 1px;">$code&nbsp;</td><td>$type</td>
</tr>
<tr>
    <td>$message</td>
</tr>
<tr valign="top">
    <td colspan="2">
        <table cellpadding="1" cellspacing="0">
            $trace
        </table>
    </td>
</tr>
HTML;
        return $htmldoc;
    }

    private static function GenerateTrace( $_trace, $_i )
    {
        $trace['Trace'] = "#$_i";

        $file = '';
        if (array_key_exists( 'file', $_trace ))
            $file = $_trace['file'];

        if (array_key_exists( 'line', $_trace ))
            $file .= '(' . $_trace['line'] . ')';

        $trace['File'] = $file;

        $function = '';
        if (array_key_exists( 'class', $_trace ) && array_key_exists( 'type', $_trace ))
            $function .= $_trace['class'] . $_trace['type'];

        if (array_key_exists( 'function', $_trace )) {
            $function .= $_trace['function'] . '(';
            if (array_key_exists( 'args', $_trace )) {
                $func = null;
                if(!isset($_trace['class'])){
                    // TODO: Prevent loading of php language constructs via reflection (which would fail).
                    try {
                        $func = new \ReflectionFunction($_trace['function']);
                    }
                    catch (Exception $ex) {}
                }
                elseif(!preg_match( '~\{closure\}~', $_trace['function']))
                    $func = new \ReflectionMethod($_trace['class'], $_trace['function']);

                $result = array();
                if($func != null)
                    foreach ($func->getParameters() as $param)
                        $result[] = $param->name;

                $count = 0;
                foreach ($_trace['args'] as $value) {
                    if ($count > 0)
                        $function .= ', ';

                    if(array_key_exists( $count, $result ))
                        $function .= "{$result[$count]}:&nbsp;";

                    $type = gettype( $value );

                    if ($type == 'boolean')
                        $function .= $value ? 'true' : 'false';
                    elseif ($type == 'integer' || $type == 'double') {
                        if (settype( $value, 'string' ))
                            $function .= $value;
                        else
                            $function .= $type == 'integer'
                                ? '? integer ?'
                                : '? double or float ?';
                    }
                    elseif ($type == 'string'){
                        $function .= preg_match( '~<.*>~', $value )
                            ? '{html}'
                            : (preg_match('~.*(pass|pwd).*~i', $_trace['function']) || (array_key_exists( $count, $result ) && preg_match('~.*(pass|pwd).*~i', $result[$count])) || preg_match('~^\$(1|2a|2y|2x|5|6)\$~', $value))
                                ? "'[SECRET]'"
                                : "'$value'";
                    }
                    elseif ($type == 'array')
                        $function .= 'Array(' . count($value) . ')';
                    elseif ($type == 'object') {
                        $className = get_class($value);
                        $value = $value instanceof \DOMElement ? $value->tagName : $value;
                        $toString = $value instanceof \Closure ? "{closure}" : "$value";
                        $function .= $className;
                            if($className != $toString)
                                $function .= "($toString)";
                    }
                    elseif ($type == 'resource')
                        $function .= 'Resource';
                    elseif ($type == 'NULL')
                        $function .= 'null';
                    elseif ($type == 'unknown type')
                        $function .= '? unknown type ?';
                    unset( $type );
                    unset( $value );
                    $count++;
                }
                unset( $count );
            }
            $function .= ')';
        }
        $trace['Function'] = $function;
        return $trace;
    }
}
// Enable AppStatic Save Trace regarding on the PHP display_errors setting.
ExceptionHandler::SetSaveTrace( !ini_get( 'display_errors' ) );
// Register ExceptionHandler as global PHP exception handler.
set_exception_handler( array('\AppStatic\Core\ExceptionHandler', 'HandleException' ) );
// Start output buffering so exception messages appended or prepended to the client response.
ob_start();