<?php
/**
 * Created by PhpStorm.
 * User: Cosmo
 * Date: 04.05.2015
 * Time: 00:00
 */

namespace AppStatic\Core;


class String {

    /**
     * Enter description here...
     *
     * @param mixed $target
     * @param array $replacements
     * @return array|mixed
     */
    public static function Replace($target, array $replacements)
    {
        $keys = array_keys($replacements);
        $values = array_values($replacements);

        if (!is_array($target)) {
            $target = str_replace($keys, $values, $target);
        } else {
            $temp = str_replace($keys, $values, $target);
            $targetCount = count($target);
            for ($i = 0; $i < $targetCount; $i++)
                $target[$i] = $temp[$i];
            unset($i);
        }

        unset($keys);
        unset($values);
        return $target;
    }

    public static function EndsWidth($string, $value)
    {
        return substr_compare($string, $value, -strlen($value), strlen($value)) === 0;
    }

    public static function Insert($insertstring, $intostring, $offset)
    {
        $part1 = substr($intostring, 0, $offset);
        $part2 = substr($intostring, $offset);

        $part1 = $part1 . $insertstring;
        $whole = $part1 . $part2;
        return $whole;
    }

    public static function FormatToUpperLowerCase($string, $firstToLower = false)
    {
        // Erster Buchstabe groß oder klein
        $temp = !is_int($string[0]) ? ($firstToLower ? strtolower($string[0]) : strtoupper($string[0])) : '';
        // Rest klein
        $valueLength = strlen($temp);
        $temp .= strtolower(substr($string, $valueLength, strlen($string) - $valueLength));

        return $temp;
    }

    public static function FormatWordSpecialChars($string, $explodes = array(' ', '-', '.'))
    {
        // Entferne alle Trennsymbole die nicht im String vorhanden sind.
        foreach ($explodes as $key => $explode)
            if (strpos($string, $explode) == 0)
                unset($explodes[$key]);

        // Abbrechen wenn der String keine Trennsymbole enthält.
        if (count($explodes) == 0)
            return self::FormatToUpperLowerCase($string, false);

        // Erstes Trennsymbol aus dem Array holen und mit den restlichen Symbolen weiter machen.
        $explode = array_shift($explodes);
        $words = explode($explode, $string);
        $firstWord = true;
        // Immer das letzte leere Element aus dem Array entfernen wenn nur ein Wort in dem String vorhanden war.
        if ($words[count($words) - 1] == '')
            array_pop($words);

        foreach ($words as &$word) {

            // Suche nach speziell kleingeschriebenen Wörtern
            $wordsFound = false;
            foreach (array('an', 'am', 'auf', 'der', 'in', 'im', 'von', 'van', 'unter') as $val)
                if (strcasecmp($val, $word) === 0) {
                    $wordsFound = true;
                    break;
                }

            // Alle weiteren Trennsymbole formatieren wenn keine kleinzuschreibenden Wörtern gefunden werden konnten.
            if (count($explodes) > 0 && !$wordsFound) {
                $tmpWord = self::FormatWordSpecialChars($word, $explodes);
                if ($tmpWord != $word) {
                    $word = $tmpWord;
                    $firstWord = false;
                }
                continue;
            }

            // Wörter klein schreiben wenn mittem im Text
            $toLower = $firstWord == false && ($wordsFound && ($explode == ' '));

            $word = self::FormatToUpperLowerCase($word, $toLower);
            $firstWord = false;
        }
        if ($explode == '.') {
            $explode = '. ';
            return trim((count($words) == 1) ? $words[0] . $explode : implode($explode, $words));
        }

        return implode($explode, $words);
    }

    public static function IsMD5($string)
    {
        if (!is_string($string))
            throw new \InvalidArgumentException('Parameter $string must be type of string.');
        return preg_match('/^[0-9a-f]{32}$/i', $string);
    }

    public static function IsBase64Encoded($string)
    {
        //^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$
        return preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $string) ? TRUE : FALSE;
    }

    public static function IsIP($string)
    {
        return preg_match('~^(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:[.](?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}$~',
            $string) ? TRUE : FALSE;
    }

    /**
     * Count the number of bytes of a given string.
     * Input string is expected to be ASCII or UTF-8 encoded.
     * Warning: the function doesn't return the number of chars
     * in the string, but the number of bytes.
     *
     * @param string $str The string to compute number of bytes
     * @return int The length in bytes of the given string.
     * @see http://www.cl.cam.ac.uk/~mgk25/unicode.html#utf-8
     */
    public static function GetBytesCount($str)
    {
        // Number of characters in string
        $strlen_var = strlen($str);

        // string bytes counter
        $d = 0;

        /*
         * Iterate over every character in the string,
         * escaping with a slash or encoding to UTF-8 where necessary
         *
         */
        for ($c = 0; $c < $strlen_var; ++$c) {

            $ord_var_c = @ord($str{$d});

            switch (true) {
                // characters U-00000000 - U-0000007F (same as ASCII)
                case (($ord_var_c >= 0x20) && ($ord_var_c <= 0x7F)):
                    $d++;
                    break;
                // characters U-00000080 - U-000007FF, mask 110XXXXX
                case (($ord_var_c & 0xE0) == 0xC0):
                    $d += 2;
                    break;
                // characters U-00000800 - U-0000FFFF, mask 1110XXXX
                case (($ord_var_c & 0xF0) == 0xE0):
                    $d += 3;
                    break;
                // characters U-00010000 - U-001FFFFF, mask 11110XXX
                case (($ord_var_c & 0xF8) == 0xF0):
                    $d += 4;
                    break;
                // characters U-00200000 - U-03FFFFFF, mask 111110XX
                case (($ord_var_c & 0xFC) == 0xF8):
                    $d += 5;
                    break;
                // characters U-04000000 - U-7FFFFFFF, mask 1111110X
                case (($ord_var_c & 0xFE) == 0xFC):
                    $d += 6;
                    break;
                default:
                    $d++;
            }
        }

        return $d;
    }

    public static function EscapeShellArg( $input )
    {
        $input = str_replace( '\'', '\\\'', $input );

        return '\'' . $input . '\'';
    }
}