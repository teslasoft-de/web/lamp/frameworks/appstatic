<?php
/**
 * Created by PhpStorm.
 * User: Cosmo
 * Date: 20.04.2015
 * Time: 00:11
 */
namespace AppStatic\IO;

use AppStatic\Core\ExceptionBase;

if (!defined( 'PATH_SEPARATOR' )) {
    if (strpos( $_ENV['OS'], 'Win' ) !== false)
        define( 'PATH_SEPARATOR', ';' );
    else
        define( 'PATH_SEPARATOR', ':' );
}

if(!defined('DIRECTORY_SEPARATOR_WIN32'))
    define( 'DIRECTORY_SEPARATOR_WIN32', '\\' );

if(!defined('DIRECTORY_SEPARATOR_UNIX'))
    define( 'DIRECTORY_SEPARATOR_UNIX', '/' );

final class Path
{
    public static function Combine($path1, $path2)
    {
        if (($path1 === null) || ($path2 === null))
            throw new \InvalidArgumentException((($path1 == null) ? '$path1' : '$path2') . ' can not be null.');

        self::CheckInvalidPathChars($path1);
        self::CheckInvalidPathChars($path2);

        if (strlen($path2) == 0)
            return $path1;

        if (strlen($path1) == 0)
            return $path2;

        if (self::IsPathRooted($path2))
            return $path2;

        $char = $path1{strlen($path1) - 1};
        if ((($char != DIRECTORY_SEPARATOR_WIN32) && ($char != DIRECTORY_SEPARATOR_UNIX)) && ($char != ':'))
            return ($path1 . DIRECTORY_SEPARATOR . $path2);

        return ($path1 . $path2);
    }

    public static function IsPathRooted($path)
    {
        if ($path != null) {
            self::CheckInvalidPathChars($path);
            $length = strlen($path);
            if ((($length >= 1) && (($path{0} == DIRECTORY_SEPARATOR_WIN32) || ($path{0} == DIRECTORY_SEPARATOR_UNIX))) || (($length >= 0x2) && ($path{0x1} == ':')))
                return true;
        }
        return false;
    }

    static function CheckInvalidPathChars($path)
    {
        $asiiCode = null;
        for ($i = 0; $i < strlen($path); $i++) {
            $asiiCode = ord($path{$i});
            if ((($asiiCode == 0x22) || ($asiiCode == 0x3c)) || ((($asiiCode == 0x3e) || ($asiiCode == 0x7c)) || ($asiiCode < 0x20)))
                throw new \InvalidArgumentException('Illegal characters in path.');
        }
        unset($asiiCode);
    }

    public static $InvalidPathChars = array(
        '"', '<', '>', '|', "\0",
        "\x0001", "\x0002", "\x0003", "\x0004", "\x0005", "\x0006",
        /*"\a", "\b",*/
        "\t", "\n", "\v", "\f", "\r",
        "\x000e", "\x000f", "\x0010", "\x0011", "\x0012", "\x0013", "\x0014", "\x0015", "\x0016", "\x0017", "\x0018", "\x0019", "\x001a", "\x001b", "\x001c", "\x001d", "\x001e", "\x001f");

    public static function MakeValidFilename($fileName)
    {
        if ($fileName == null)
            throw new \InvalidArgumentException('Parameter name $fileName can not be null.');

        $path = '';
        $invalidPathChars = self::$InvalidPathChars;
        $invalidPathCharsCount = count($invalidPathChars);

        for ($i = 0; $i < strlen($fileName); $i++) {
            $char = $fileName{$i};

            for ($j = 0; $j < $invalidPathCharsCount; $j++) {
                if ($char == $invalidPathChars[$j])
                    continue (2);
                //TODO: Try with array_search
            }

            $path .= $char;
        }

        $path = self::GetFileName($path);

        if (strlen($path) == 0)
            throw new PathException("File name completely invalid.");

        return $path;
    }

    public static function GetFileName($path)
    {
        if ($path != null) {
            self::CheckInvalidPathChars($path);
            $length = strlen($path);
            $index = $length;
            while (--$index >= 0) {
                $char = $path{$index};
                if ((($char == DIRECTORY_SEPARATOR_WIN32) || ($char == DIRECTORY_SEPARATOR_UNIX)) || ($char == ':'))
                    return substr($path, $index + 1, ($length - $index) - 1);
            }
        }
        return $path;
    }

}

final class PathException extends ExceptionBase
{
    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}