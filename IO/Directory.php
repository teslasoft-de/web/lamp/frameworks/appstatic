<?php
/**
 * Created by PhpStorm.
 * User: Cosmo
 * Date: 05.05.2015
 * Time: 00:14
 */

namespace AppStatic\IO;


final class Directory {
    public static function Create( $dir, $chmod = 0777 )
    {
        if (is_dir( $dir ))
            return;

        $cwd = getcwd();
        chdir( $_SERVER['DOCUMENT_ROOT']  );
        if (!mkdir( $dir, $chmod, true ))
            trigger_error( "Failed to create directory '$dir'", E_USER_ERROR);
        if (strpos( $_ENV['OS'], 'Win' ) !== false)
            chmod( $dir, $chmod, true );
        chdir( $cwd );
    }
}