<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 19.04.2015
 * File: System.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic;

use AppStatic\Core;

abstract class AppStaticException extends Core\ExceptionBase
{
    function __construct( $_message, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}
?>
