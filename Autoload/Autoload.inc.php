<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: Autoload.inc.php
 * Encoding: UTF-8
 * Project: AppStatic
 * Author: H. Christian Kusmanow
 * Copyright: © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 * */

function __autoload( $className )
{
    if (!preg_match( '/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $className ))
        new AutoLoadException( "Class name '$className' is invalid!" );
    
    $include_path_tokens = explode( PATH_SEPARATOR, get_include_path() );
    $classPath = str_replace(
            array(DIRECTORY_SEPARATOR_UNIX, DIRECTORY_SEPARATOR_WIN32),
            DIRECTORY_SEPARATOR,
            $className);
    
    foreach( $include_path_tokens as $prefix ) {
        $path = $prefix . DIRECTORY_SEPARATOR . $classPath . '.php';
        if (file_exists( $path )) {
            /** @noinspection PhpIncludeInspection */
            require_once $path;
            if(class_exists($className, false) || interface_exists( $className, false ))
                return;
        }
    }
    
    $namespace = substr( $className, 0, strrpos( $className, DIRECTORY_SEPARATOR_WIN32 ) );
    if ($namespace) {
        $localClassName = substr( $className, strrpos( $className, DIRECTORY_SEPARATOR_WIN32 ) + 1 );

        @eval( "namespace $namespace;
            $ex = serialize( new AutoLoadException( \"Class $namespace::$localClassName not found\" ) );
          class $localClassName { 
            function __construct() {
                throw unserialize(\"$ex\");
            }
         
            static function __callstatic(\$m, \$args) {
                throw unserialize(\"$ex\");
            }
          }" );
    }else {
        @eval( "class $className {
            public static function ThrowAutoLoadException()
            {
                throw unserialize('" . serialize( new AutoLoadException( "Class $className not found" ) ) . "');
            }
          }
          $className::ThrowAutoLoadException();" );
    }
}

final class AutoLoadException extends \AppStatic\Core\ExceptionBase
{
    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}

spl_autoload_register( "__autoload" );