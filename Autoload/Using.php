<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: Using.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Autoload;

use AppStatic\IO\Path;

require_once APPSTATIC_PATH . '/Autoload/IncludePath.php';
require_once APPSTATIC_PATH . '/Autoload/Autoload.inc.php';

/**
 * @package AppStatic\Autoload
 * @name Using
 * @version 1.0
 *
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow
 *
 */
final class Using
{
    /**
     * Adds a new include path to the configuration.
     * If the directory is not absolute (not prefixed with '/') the server document root will be used as root. 
     * 
     * @param string $directory
     */
    public static function Directory( $directory = null )
    {
        if (!is_string( $directory ))
            throw new \InvalidArgumentException( 'Directory must be type of string.' );
        
        $documentRoot = filter_input( INPUT_SERVER, 'DOCUMENT_ROOT' );
        if(empty($documentRoot))
            $documentRoot = APPSTATIC_PATH . '/../';
        $includePath = Path::Combine( $documentRoot, $directory );
        
        IncludePath::Add( $includePath );
    }
    
    public static function IsDeclared( $className )
    {
        return in_array( $className, get_declared_classes() );
    }
}
?>