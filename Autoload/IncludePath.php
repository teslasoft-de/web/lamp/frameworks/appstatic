<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: IncludePath.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Autoload;

use AppStatic\Core\ExceptionBase;
use AppStatic\Core\PropertyBase;
use AppStatic\IO\Path;

require_once APPSTATIC_PATH . '/Core/ExceptionBase.php';
require_once APPSTATIC_PATH . '/Core/PropertyBase.php';
require_once APPSTATIC_PATH . '/IO/Path.php';

/**
 * @package AppStatic
 * @name IncludePath
 * @version 1.0
 *
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow
 *
 */
final class IncludePath extends PropertyBase
{
    protected $BasePath;
    
    public function GetBasePath()
    {
        return $this->BasePath;
    }
    
    /**
     * Intializes a new instance of the IncludePath class.
     * 
     * @param string $basePath
     */
    public function __construct( $basePath )
    {
        self::Add( $basePath );
        $this->BasePath = $basePath;
    }
    
    /**
     * Adds a new child include path to the include path.
     * 
     * @param string $path
     * @return string
     */
    public function AddPath( $path )
    {
        $path = Path::Combine( $this->BasePath, $path );
        self::Add( $path );
        return $path;
    }
    
    public function RemovePath( $path )
    {
        $path = Path::Combine( $this->BasePath, $path );
        self::Remove( $path );
    }
    
    /**
     * Adds a new include path to the configuration.
     * 
     * @param string $path
     */
    public static function Add( $path )
    {
        foreach( func_get_args() as $path ) {
            self::CheckPath( $path );
            
            $paths = explode( PATH_SEPARATOR, get_include_path() );
            
            if (array_search( $path, $paths ) === false) {
                $paths []= $path;
                set_include_path( implode( PATH_SEPARATOR, $paths ) );
            }
        }
    }

    /**
     * Removes the spezified include path.
     * Throws IncludePathException if the include path is the last one.
     *
     * @param string $path
     * @throws IncludePathException
     */
    public static function Remove( $path )
    {
        foreach( func_get_args() as $path ) {
            self::CheckPath( $path );
            
            $paths = explode( PATH_SEPARATOR, get_include_path() );
            
            if (($k = array_search( $path, $paths )) !== false)
                unset( $paths[$k] );
            else
                continue;
            
            if (!count( $paths ))
                throw new IncludePathException( "Include path '{$path}' can not be removed because it is the only", E_USER_NOTICE );
            
            set_include_path( implode( PATH_SEPARATOR, $paths ) );
        }
    }
    
    private static function CheckPath( $path )
    {
        if (!file_exists( $path ) or filetype( $path ) !== 'dir')
            throw new IncludePathException( "Include path '{$path}' not exists", E_USER_WARNING );
    }
}

final class IncludePathException extends ExceptionBase
{
    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}
?>