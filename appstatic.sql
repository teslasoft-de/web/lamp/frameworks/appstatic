/*
Navicat MySQL Data Transfer

Source Server         : iis02.dev.nw.teslasoft.local
Source Server Version : 50543
Source Host           : localhost:3306
Source Database       : appstatic

Target Server Type    : MYSQL
Target Server Version : 50543
File Encoding         : 65001

Date: 2015-04-22 00:50:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `blacklistedemaildomains`
-- ----------------------------
DROP TABLE IF EXISTS `blacklistedemaildomains`;
CREATE TABLE `blacklistedemaildomains` (
  `BlacklistedEmailDomain_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `BlacklistedEmailDomain_Name` varchar(256) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `BlacklistedEmailDomain_Source` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`BlacklistedEmailDomain_ID`),
  UNIQUE KEY `ID` (`BlacklistedEmailDomain_ID`) USING BTREE,
  UNIQUE KEY `Name` (`BlacklistedEmailDomain_Name`) USING BTREE,
  KEY `Owner` (`BlacklistedEmailDomain_Source`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=425 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blacklistedemaildomains
-- ----------------------------
INSERT INTO `blacklistedemaildomains` VALUES ('1', 'antichef.net', 'spamgourmet.com');
INSERT INTO `blacklistedemaildomains` VALUES ('2', 'bsnow.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('3', 'bumpymail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('4', 'centermail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('5', 'centermail.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('6', 'discardmail.com', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('7', 'discardmail.de', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('8', 'dodgeit.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('9', 'dodgit.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('10', 'dontsendmespam.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('11', 'dumpmail.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('12', 'e4ward.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('13', 'eintagsmail.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('14', 'emailias.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('15', 'emailto.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('16', 'fastacura.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('17', 'fastchevy.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('18', 'fastchrysler.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('19', 'fastkawasaki.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('20', 'fastmazda.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('21', 'fastmitsubishi.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('22', 'fastnissan.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('23', 'fastsubaru.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('24', 'fastsuzuki.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('25', 'fasttoyota.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('26', 'fastyamaha.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('27', 'ghosttexter.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('28', 'gomail.ws', null);
INSERT INTO `blacklistedemaildomains` VALUES ('29', 'htl22.at', null);
INSERT INTO `blacklistedemaildomains` VALUES ('30', 'jetable.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('31', 'jetable.org', null);
INSERT INTO `blacklistedemaildomains` VALUES ('32', 'kasmail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('33', 'klassmaster.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('34', 'kurzepost.de', 'trashmail.net');
INSERT INTO `blacklistedemaildomains` VALUES ('35', 'mailin8r.com', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('36', 'mailblocks.com', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('37', 'mailexpire.com', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('38', 'mailinator.com', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('39', 'mailinator.net', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('40', 'mailinator2.com', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('41', 'mailshell.com', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('42', 'mbx.cc', 'garbagemail.org');
INSERT INTO `blacklistedemaildomains` VALUES ('43', 'messagebeamer.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('44', 'mytrashmail.com', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('45', 'netmails.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('46', 'nervmich.net', 'nervmich.net');
INSERT INTO `blacklistedemaildomains` VALUES ('47', 'nervtmich.net', 'nervmich.net');
INSERT INTO `blacklistedemaildomains` VALUES ('48', 'netzidiot.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('49', 'nospammail.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('50', 'nurfuerspam.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('51', 'pookmail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('52', 'privacy.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('53', 'privy-mail.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('54', 'punkass.com', 'trashmail.net');
INSERT INTO `blacklistedemaildomains` VALUES ('55', 'qv7.info', null);
INSERT INTO `blacklistedemaildomains` VALUES ('56', 'rcpt.at', 'trashmail.net');
INSERT INTO `blacklistedemaildomains` VALUES ('57', 'sneakemail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('58', 'sofort-mail.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('59', 'spam.la', null);
INSERT INTO `blacklistedemaildomains` VALUES ('60', 'spambob.com', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('61', 'spambob.net', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('62', 'spambob.org', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('63', 'spambog.com', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('64', 'spambog.de', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('65', 'spambox.de', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('66', 'spambox.us', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('67', 'spamex.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('68', 'spamgourmet.com', 'spamgourmet.com');
INSERT INTO `blacklistedemaildomains` VALUES ('69', 'spamherelots.com            ', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('70', 'spamhole.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('71', 'spaminator.de', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('72', 'spammotel.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('73', 'spamoff.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('74', 'spamtrail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('75', 'sogetthis.com', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('76', 'svenz.eu', null);
INSERT INTO `blacklistedemaildomains` VALUES ('77', 'temp-mail.org', null);
INSERT INTO `blacklistedemaildomains` VALUES ('78', 'temporarily.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('79', 'thisisnotmyrealemail.com', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('80', 'trash-mail.at', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('81', 'trash-mail.com', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('82', 'trash-mail.de', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('83', 'trash-mail.net', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('84', 'trashmail.at', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('85', 'trashmail.com', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('86', 'trashmail.de', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('87', 'trashmail.me', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('88', 'trashmail.net', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('89', 'wegwerfadresse.de', 'nervmich.net');
INSERT INTO `blacklistedemaildomains` VALUES ('90', 'wh4f.org', null);
INSERT INTO `blacklistedemaildomains` VALUES ('91', 'whitemail.ie', null);
INSERT INTO `blacklistedemaildomains` VALUES ('92', 'willhackforfood.biz', null);
INSERT INTO `blacklistedemaildomains` VALUES ('93', 'yandex.ru', null);
INSERT INTO `blacklistedemaildomains` VALUES ('94', 'zipzaps.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('95', 'kkk.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('96', 'mt2009.com', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('97', 'trash2009.com', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('98', 'trashymail.com', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('99', 'jetable.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('100', 'test.domain.dienstleistungen.ws', null);
INSERT INTO `blacklistedemaildomains` VALUES ('101', 'meltmail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('102', 'uggsrock.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('103', 'tyldd.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('104', 'mail.ru', null);
INSERT INTO `blacklistedemaildomains` VALUES ('105', 'mmm.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('106', 'dotman.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('107', 'safetymail.info', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('108', 'suremail.info', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('109', 'spambog.ru', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('110', 'cust.in', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('111', 'imails.info', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('112', 'great-host.in', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('113', 'teewars.org', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('114', 'kulturbetrieb.info', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('115', 'bio-muesli.net', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('116', 'bio-muesli.info', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('117', '0815.ru', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('118', 'sandelf.de', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('119', 'recode.me', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('120', '3d-painting.com', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('121', 'nomail2me.com', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('122', 'bla.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('123', 'guerrillamailblock.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('124', 'gugus.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('125', 'nobody.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('126', 'justgo.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('127', '12mail.cc', null);
INSERT INTO `blacklistedemaildomains` VALUES ('128', '12mail.at', null);
INSERT INTO `blacklistedemaildomains` VALUES ('129', '12mail.org', null);
INSERT INTO `blacklistedemaildomains` VALUES ('130', 'antispam.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('131', 'orf.at', null);
INSERT INTO `blacklistedemaildomains` VALUES ('132', 'shell.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('133', 'fahr-zur-hoelle.org', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('134', 'nurfuerspam.de	', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('135', 'user.pl', null);
INSERT INTO `blacklistedemaildomains` VALUES ('136', 'best.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('138', 'anonbox.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('139', 'bist-eine-haelfte-von-mir.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('140', 'bleib-bei-mir.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('141', 'es-ist-liebe.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('142', 'ich-bin-verrueckt-nach-dir.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('143', 'ist-allein.info', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('144', 'ist-es-liebe.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('145', 'ist-ganz-allein.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('146', 'ist-willig.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('147', 'lass-es-geschehen.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('148', 'liebt-dich.info', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('149', 'loveyouforever.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('150', 'moechte-mit-dir-aufwachen.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('151', 'schmusemail.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('152', 'schreib-doch-mal-wieder.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('153', 'teilt-mit-dir-alle-geheimnisse.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('154', 'verlass-mich-nicht.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('155', 'vorsicht-scharf.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('156', 'weinenvorglueck.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('157', 'alphafrau.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('158', 'feinripptraeger.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('159', 'fettabernett.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('160', 'ist-einmalig.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('161', 'kaffeeschluerfer.com', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('162', 'liebt-den-mann-ihrer-chefin.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('163', 'liebt-die-frau-seines-chefs.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('164', 'maennerversteherin.com', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('165', 'partybombe.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('166', 'partyheld.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('167', 'polizisten-duzer.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('168', 'raubtierbaendiger.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('169', 'saeuferleber.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('170', 'tortenboxer.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('171', 'turboprinz.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('172', 'turboprinzessin.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('173', 'vorsicht-bissig.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('174', 'alpenjodel.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('175', 'das-spiel-hat-90-minuten.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('176', 'der-ball-ist-rund.net', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('177', 'die-genossen.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('178', 'die-optimisten.net', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('179', 'email-ausdrucker.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('180', 'fantasymail.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('181', 'freudenkinder.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('182', 'gentlemansclub.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('183', 'gmx-ist-cool.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('184', 'herr-der-mails.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('185', 'ich-habe-fertig.com', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('186', 'meine-wahrheit-deine-wahrheit.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('187', 'muskelshirt.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('188', 'quantentunnel.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('189', 'sags-per-mail.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('190', 'sonnenkinder.org', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('191', 'turnbeutel-vergesser.com', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('192', 'vollbio.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('193', 'volloeko.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('194', 'vorabend-einchecker.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('195', 'weibsvolk.org', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('196', 'wenns-um-email-geht.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('197', 'wir-sind-cool.org', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('198', 'wolke7.net', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('199', 'abwesend.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('200', 'baldmama.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('201', 'baldpapa.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('202', 'betriebsdirektor.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('203', 'buerotiger.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('204', 'habmalnefrage.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('205', 'hab-verschlafen.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('206', 'ich-will-net.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('207', 'kommespaeter.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('208', 'mailueberfall.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('209', 'netterchef.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('210', 'streber24.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('211', 'terminverpennt.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('212', 'unterderbruecke.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('213', 'will-hier-weg.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('214', 'wir-haben-nachwuchs.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('215', 'women-at-work.org', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('216', 'bin-wieder-da.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('217', 'coole-files.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('218', 'die-besten-bilder.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('219', 'digi-dia-freakshow.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('220', 'digital-filestore.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('221', 'digitalfoto-model-award.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('222', 'download-privat.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('223', 'gmx-topmail.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('224', 'meine-dateien.info', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('225', 'meine-diashow.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('226', 'meine-fotos.info', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('227', 'meine-urlaubsfotos.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('228', 'neue-dateien.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('229', 'office-dateien.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('230', 'peinliche-fotos.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('231', 'public-files.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('232', 'shared-files.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('233', 'topmail-files.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('234', 'war-im-urlaub.de', 'gmx.de');
INSERT INTO `blacklistedemaildomains` VALUES ('235', 'binkmail.com', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('236', 'treiber-studio.de', 'treiber-studio.de');
INSERT INTO `blacklistedemaildomains` VALUES ('237', 'hallo.ms', null);
INSERT INTO `blacklistedemaildomains` VALUES ('238', 'bobmail.info', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('239', 'tempemail.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('240', 'getonemail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('241', '3dl.am', null);
INSERT INTO `blacklistedemaildomains` VALUES ('242', 'hushmail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('243', '10minutemail.com', '10minutemail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('244', 'bofthew.com', '10minutemail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('245', 'lhsdv.com', '10minutemail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('246', 'wegwerfemail.de', 'trashmail.net');
INSERT INTO `blacklistedemaildomains` VALUES ('247', 'proxymail.eu', 'trashmail.net');
INSERT INTO `blacklistedemaildomains` VALUES ('248', 'objectmail.com', 'trashmail.net');
INSERT INTO `blacklistedemaildomains` VALUES ('249', 'wegwerfemail.net', 'trashmail.net');
INSERT INTO `blacklistedemaildomains` VALUES ('250', 'wegwerfemail.org', 'trashmail.net');
INSERT INTO `blacklistedemaildomains` VALUES ('251', '126.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('252', 'safe-mail.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('253', 'spamsafemail.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('254', 'maileater.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('255', 'bigfoot.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('256', 'politikerclub.de', 'malowa.de');
INSERT INTO `blacklistedemaildomains` VALUES ('399', 'cool.fr.nf', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('280', 'fakemail.fr', null);
INSERT INTO `blacklistedemaildomains` VALUES ('330', 'quickinbox.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('335', 'secretemail.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('275', 'despammed.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('307', 'mailcatch.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('277', 'dump-email.info', null);
INSERT INTO `blacklistedemaildomains` VALUES ('262', 'owlpic.com', '10minutemail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('351', 'misterpinball.de', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('340', 'SendSpamHere.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('270', 'b2cmail.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('318', 'makemetheking.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('312', 'SpamHerePlease.com', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('279', 'emailtemporanea.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('406', 'moncourrier.fr.nf', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('347', 'fr33mail.info', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('323', 'nospam4.us', null);
INSERT INTO `blacklistedemaildomains` VALUES ('371', 'trialmail.de', 'spoofmail.de');
INSERT INTO `blacklistedemaildomains` VALUES ('370', 'spoofmail.de', 'spoofmail.de');
INSERT INTO `blacklistedemaildomains` VALUES ('299', 'lr78.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('346', 'spamcero.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('363', 'spaml.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('310', 'devnullmail.com', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('309', 'chogmail.com', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('402', 'nomail.xl.cx', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('273', 'bugmenot.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('362', 'spamfree24.org', 'spamfree24.org');
INSERT INTO `blacklistedemaildomains` VALUES ('257', '0clickemail.com', '0clickemail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('302', 'express.net.ua', 'mail.zp.ua');
INSERT INTO `blacklistedemaildomains` VALUES ('392', 'wegwerf-email.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('344', 'spam.su', null);
INSERT INTO `blacklistedemaildomains` VALUES ('328', 'onlatedotcom.info', null);
INSERT INTO `blacklistedemaildomains` VALUES ('400', 'jetable.fr.nf', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('389', 'trashemail.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('349', 'hulapla.de', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('353', 'nospamthanks.info', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('395', 'yopmail.com', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('358', 'spamfree.eu', 'spamfree24.org');
INSERT INTO `blacklistedemaildomains` VALUES ('274', 'deadaddress.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('410', 'zehnminutenmail.de', 'zehnminuten.de');
INSERT INTO `blacklistedemaildomains` VALUES ('386', 'topranklist.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('315', 'mailme24.com', '');
INSERT INTO `blacklistedemaildomains` VALUES ('337', 'secure-mail.biz', 'secure-mail.biz');
INSERT INTO `blacklistedemaildomains` VALUES ('295', 'wegwerf-email-adressen.de', 'instant-mail.de');
INSERT INTO `blacklistedemaildomains` VALUES ('388', 'trashdevil.de', 'trashdevil.de');
INSERT INTO `blacklistedemaildomains` VALUES ('308', 'maileimer.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('342', 'shieldemail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('331', 'safetypost.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('284', 'garliclife.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('397', 'youmailr.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('263', 'pjjkp.com', '10minutemail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('316', 'mailnull.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('354', 's0ny.net', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('343', 'soodonims.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('294', 'sinnlos-mail.de', 'instant-mail.de');
INSERT INTO `blacklistedemaildomains` VALUES ('378', 'fudgerub.com', 'tempinbox.com');
INSERT INTO `blacklistedemaildomains` VALUES ('300', 'luckymail.org', null);
INSERT INTO `blacklistedemaildomains` VALUES ('367', 'fivemail.de', 'spoofmail.de');
INSERT INTO `blacklistedemaildomains` VALUES ('365', 'spamspot.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('306', 'mail4trash.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('267', 'anonmails.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('301', 'card.zp.ua', 'mail.zp.ua');
INSERT INTO `blacklistedemaildomains` VALUES ('366', 'SpamThisPlease.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('409', 'zehnminuten.de', 'zehnminuten.de');
INSERT INTO `blacklistedemaildomains` VALUES ('403', 'mega.zik.dj', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('345', 'spamavert.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('361', 'spamfree24.info', 'spamfree24.org');
INSERT INTO `blacklistedemaildomains` VALUES ('380', 'smellfear.com', 'tempinbox.com');
INSERT INTO `blacklistedemaildomains` VALUES ('381', 'tempinbox.com', 'tempinbox.com');
INSERT INTO `blacklistedemaildomains` VALUES ('293', 'instant-mail.de', 'instant-mail.de');
INSERT INTO `blacklistedemaildomains` VALUES ('407', 'monemail.fr.nf', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('377', 'dingbone.com', 'tempinbox.com');
INSERT INTO `blacklistedemaildomains` VALUES ('341', 'senseless-entertainment.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('396', 'yopmail.net', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('408', 'monmail.fr.nf', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('332', 'schafmail.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('379', 'lookugly.com', 'tempinbox.com');
INSERT INTO `blacklistedemaildomains` VALUES ('317', 'mailtrash.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('276', 'dispostable.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('320', 'mytempmail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('338', 'secure-mail.cc', 'secure-mail.biz');
INSERT INTO `blacklistedemaildomains` VALUES ('314', 'zippymail.info', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('264', 'prtnx.com', '10minutemail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('385', 'thismail.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('329', 'put2.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('258', 'noclickemail.com', '0clickemail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('324', 'nospamfor.us', null);
INSERT INTO `blacklistedemaildomains` VALUES ('326', 'obobbo.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('360', 'spamfree24.de', 'spamfree24.org');
INSERT INTO `blacklistedemaildomains` VALUES ('286', 'gishpuppy.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('285', 'getmails.eu', null);
INSERT INTO `blacklistedemaildomains` VALUES ('303', 'infocom.zp.ua', 'mail.zp.ua');
INSERT INTO `blacklistedemaildomains` VALUES ('393', 'wegwerfemail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('271', 'breakthru.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('359', 'spamfree24.com', 'spamfree24.org');
INSERT INTO `blacklistedemaildomains` VALUES ('311', 'slopsbox.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('387', 'trashdevil.com', 'trashdevil.de');
INSERT INTO `blacklistedemaildomains` VALUES ('368', 'giantmail.de', 'spoofmail.de');
INSERT INTO `blacklistedemaildomains` VALUES ('327', 'ohaaa.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('333', 'schmeissweg.tk', null);
INSERT INTO `blacklistedemaildomains` VALUES ('268', 'anonymbox.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('322', 'no-spam.ws', null);
INSERT INTO `blacklistedemaildomains` VALUES ('405', 'courriel.fr.nf', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('373', 'super-auswahl.de', 'malowa.de');
INSERT INTO `blacklistedemaildomains` VALUES ('297', 'irish2me.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('292', 'incognitomail.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('383', 'temporaryemail.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('398', 'yopmail.fr', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('259', 'jnxjn.com', '10minutemail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('391', 'wasteland.rfc822.org', null);
INSERT INTO `blacklistedemaildomains` VALUES ('325', 'nowmymail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('298', 'lifebyfood.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('261', 'nepwk.com', '10minutemail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('384', 'temporaryinbox.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('334', 'schrott-email.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('289', 'sharklasers.com', 'guerrillamail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('369', 'nevermail.de', 'spoofmail.de');
INSERT INTO `blacklistedemaildomains` VALUES ('304', 'mail.zp.ua', 'mail.zp.ua');
INSERT INTO `blacklistedemaildomains` VALUES ('266', 'anon-mail.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('336', 'lolfreak.net', 'secure-mail.biz');
INSERT INTO `blacklistedemaildomains` VALUES ('282', 'frapmail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('376', 'beefmilk.com', 'tempinbox.com');
INSERT INTO `blacklistedemaildomains` VALUES ('291', 'hidemail.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('305', 'mycard.net.ua', 'mail.zp.ua');
INSERT INTO `blacklistedemaildomains` VALUES ('364', 'spamobox.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('290', 'haltospam.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('296', 'wegwerf-emails.de', 'instant-mail.de');
INSERT INTO `blacklistedemaildomains` VALUES ('288', 'guerrillamail.org', 'guerrillamail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('357', 'spamcorptastic.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('352', 'mypartyclip.de', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('278', 'emailgo.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('339', 'z1p.biz', 'secure-mail.biz');
INSERT INTO `blacklistedemaildomains` VALUES ('372', 'stuffmail.de', 'spoofmail.de');
INSERT INTO `blacklistedemaildomains` VALUES ('321', 'thankyou2010.com', 'mytrashmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('375', 'tempail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('272', 'bspamfree.org', null);
INSERT INTO `blacklistedemaildomains` VALUES ('319', 'mintemail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('355', 'thanksnospam.info', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('390', 'twinmail.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('287', 'guerrillamail.com', 'guerrillamail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('283', 'garbagemail.org', null);
INSERT INTO `blacklistedemaildomains` VALUES ('269', 'antispam24.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('348', 'hochsitze.com', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('404', 'speed.1s.fr', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('265', 'akapost.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('350', 'm4ilweb.info', 'spambog.com');
INSERT INTO `blacklistedemaildomains` VALUES ('356', 'malowa.de', 'malowa.de');
INSERT INTO `blacklistedemaildomains` VALUES ('394', 'whyspam.me', null);
INSERT INTO `blacklistedemaildomains` VALUES ('374', 'teleworm.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('401', 'nospam.ze.tc', 'yopmail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('281', 'filzmail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('313', 'tradermail.info', 'mailinator.com');
INSERT INTO `blacklistedemaildomains` VALUES ('260', 'klzlk.com', '10minutemail.com');
INSERT INTO `blacklistedemaildomains` VALUES ('382', 'tempomail.fr', null);
INSERT INTO `blacklistedemaildomains` VALUES ('412', 'tempalias.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('418', 'mailtemp.net', null);
INSERT INTO `blacklistedemaildomains` VALUES ('416', 'incognitomail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('413', 'sapya.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('411', 'fakeinbox.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('414', 'easytrashmail.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('417', 'tempmailer.com', 'gentlesource.com');
INSERT INTO `blacklistedemaildomains` VALUES ('415', 'tempemail.co.za', null);
INSERT INTO `blacklistedemaildomains` VALUES ('419', 'anyemails.com', 'anyemails.com');
INSERT INTO `blacklistedemaildomains` VALUES ('420', 'meese24.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('421', 'ldforst.de', null);
INSERT INTO `blacklistedemaildomains` VALUES ('422', 'omiomail.zzn.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('423', 'mailforspam.com', null);
INSERT INTO `blacklistedemaildomains` VALUES ('424', 'wegwerfemailadresse.com', null);

-- ----------------------------
-- Table structure for `countrycodes`
-- ----------------------------
DROP TABLE IF EXISTS `countrycodes`;
CREATE TABLE `countrycodes` (
  `Country_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Country_ISO3166_ALPHA2` char(2) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Country_ISO3166_ALPHA3` char(3) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Country_ISO3166_Numeric` int(3) unsigned zerofill DEFAULT NULL,
  `Country_TLD` varchar(3) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Country_IOC` char(3) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Country_Name_EN` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Country_Name_DE` varchar(64) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`Country_ID`),
  UNIQUE KEY `ID` (`Country_ID`) USING BTREE,
  UNIQUE KEY `ISO3166_ALPHA2` (`Country_ISO3166_ALPHA2`) USING BTREE,
  UNIQUE KEY `IOC` (`Country_IOC`) USING BTREE,
  UNIQUE KEY `ISO3166_ALPHA3` (`Country_ISO3166_ALPHA3`) USING BTREE,
  UNIQUE KEY `TLD` (`Country_TLD`) USING BTREE,
  UNIQUE KEY `ISO3166_Numeric` (`Country_ISO3166_Numeric`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=247 DEFAULT CHARSET=utf8 COMMENT='http://de.wikipedia.org/wiki/ISO_3166-1_alpha-2';

-- ----------------------------
-- Records of countrycodes
-- ----------------------------
INSERT INTO `countrycodes` VALUES ('1', 'AF', 'AFG', '004', '.af', 'AFG', null, 'Afghanistan');
INSERT INTO `countrycodes` VALUES ('2', 'EG', 'EGY', '818', '.eg', 'EGY', null, 'Ägypten');
INSERT INTO `countrycodes` VALUES ('3', 'AX', 'ALA', '248', '.ax', null, null, 'Åland');
INSERT INTO `countrycodes` VALUES ('4', 'AL', 'ALB', '008', '.al', 'ALB', null, 'Albanien');
INSERT INTO `countrycodes` VALUES ('5', 'DZ', 'DZA', '012', '.dz', 'ALG', null, 'Algerien');
INSERT INTO `countrycodes` VALUES ('6', 'AS', 'ASM', '016', '.as', 'ASA', null, 'Amerikanisch-Samoa');
INSERT INTO `countrycodes` VALUES ('7', 'VI', 'VIR', '850', '.vi', 'ISV', null, 'Amerikanische Jungferninseln');
INSERT INTO `countrycodes` VALUES ('8', 'AD', 'AND', '020', '.ad', 'AND', null, 'Andorra');
INSERT INTO `countrycodes` VALUES ('9', 'AO', 'AGO', '024', '.ao', 'ANG', null, 'Angola');
INSERT INTO `countrycodes` VALUES ('10', 'AI', 'AIA', '660', '.ai', null, null, 'Anguilla');
INSERT INTO `countrycodes` VALUES ('11', 'AQ', 'ATA', '010', '.aq', null, null, 'Antarktis');
INSERT INTO `countrycodes` VALUES ('12', 'AG', 'ATG', '028', '.ag', 'ANT', null, 'Antigua und Barbuda');
INSERT INTO `countrycodes` VALUES ('13', 'GQ', 'GNQ', '226', '.gq', 'GEQ', null, 'Äquatorialguinea');
INSERT INTO `countrycodes` VALUES ('14', 'AR', 'ARG', '032', '.ar', 'ARG', null, 'Argentinien');
INSERT INTO `countrycodes` VALUES ('15', 'AM', 'ARM', '051', '.am', 'ARM', null, 'Armenien');
INSERT INTO `countrycodes` VALUES ('16', 'AW', 'ABW', '533', '.aw', 'ARU', null, 'Aruba');
INSERT INTO `countrycodes` VALUES ('17', 'AZ', 'AZE', '031', '.az', 'AZE', null, 'Aserbaidschan');
INSERT INTO `countrycodes` VALUES ('18', 'ET', 'ETH', '231', '.et', 'ETH', null, 'Äthiopien');
INSERT INTO `countrycodes` VALUES ('19', 'AU', 'AUS', '036', '.au', 'AUS', null, 'Australien');
INSERT INTO `countrycodes` VALUES ('20', 'BS', 'BHS', '044', '.bs', 'BAH', null, 'Bahamas');
INSERT INTO `countrycodes` VALUES ('21', 'BH', 'BHR', '048', '.bh', 'BRN', null, 'Bahrain');
INSERT INTO `countrycodes` VALUES ('22', 'BD', 'BGD', '050', '.bd', 'BAN', null, 'Bangladesch');
INSERT INTO `countrycodes` VALUES ('23', 'BB', 'BRB', '052', '.bb', 'BAR', null, 'Barbados');
INSERT INTO `countrycodes` VALUES ('24', 'BY', 'BLR', '112', '.by', 'BLR', null, 'Belarus');
INSERT INTO `countrycodes` VALUES ('25', 'BE', 'BEL', '056', '.be', 'BEL', null, 'Belgien');
INSERT INTO `countrycodes` VALUES ('26', 'BZ', 'BLZ', '084', '.bz', 'BIZ', null, 'Belize');
INSERT INTO `countrycodes` VALUES ('27', 'BJ', 'BEN', '204', '.bj', 'BEN', null, 'Benin');
INSERT INTO `countrycodes` VALUES ('28', 'BM', 'BMU', '060', '.bm', 'BER', null, 'Bermuda');
INSERT INTO `countrycodes` VALUES ('29', 'BT', 'BTN', '064', '.bt', 'BHU', null, 'Bhutan');
INSERT INTO `countrycodes` VALUES ('30', 'BO', 'BOL', '068', '.bo', 'BOL', null, 'Bolivien');
INSERT INTO `countrycodes` VALUES ('31', 'BA', 'BIH', '070', '.ba', 'BIH', null, 'Bosnien und Herzegowina');
INSERT INTO `countrycodes` VALUES ('32', 'BW', 'BWA', '072', '.bw', 'BOT', null, 'Botswana');
INSERT INTO `countrycodes` VALUES ('33', 'BV', 'BVT', '074', '.bv', null, null, 'Bouvetinsel');
INSERT INTO `countrycodes` VALUES ('34', 'BR', 'BRA', '076', '.br', 'BRA', null, 'Brasilien');
INSERT INTO `countrycodes` VALUES ('35', 'VG', 'VGB', '092', '.vg', 'IVB', null, 'Britische Jungferninseln');
INSERT INTO `countrycodes` VALUES ('36', 'IO', 'IOT', '086', '.io', null, null, 'Britisches Territorium im Indischen Ozean');
INSERT INTO `countrycodes` VALUES ('37', 'BN', 'BRN', '096', '.bn', 'BRU', null, 'Brunei Darussalam');
INSERT INTO `countrycodes` VALUES ('38', 'BG', 'BGR', '100', '.bg', 'BUL', null, 'Bulgarien');
INSERT INTO `countrycodes` VALUES ('39', 'BF', 'BFA', '854', '.bf', 'BUR', null, 'Burkina Faso');
INSERT INTO `countrycodes` VALUES ('40', 'BI', 'BDI', '108', '.bi', 'BDI', null, 'Burundi');
INSERT INTO `countrycodes` VALUES ('41', 'CL', 'CHL', '152', '.cl', 'CHI', null, 'Chile');
INSERT INTO `countrycodes` VALUES ('42', 'CN', 'CHN', '156', '.cn', 'CHN', null, 'China, Volksrepublik');
INSERT INTO `countrycodes` VALUES ('43', 'CK', 'COK', '184', '.ck', 'COK', null, 'Cookinseln');
INSERT INTO `countrycodes` VALUES ('44', 'CR', 'CRI', '188', '.cr', 'CRC', null, 'Costa Rica');
INSERT INTO `countrycodes` VALUES ('45', 'CI', 'CIV', '384', '.ci', 'CIV', null, 'Côte d\'Ivoire');
INSERT INTO `countrycodes` VALUES ('46', 'DK', 'DNK', '208', '.dk', 'DEN', null, 'Dänemark');
INSERT INTO `countrycodes` VALUES ('47', 'DE', 'DEU', '276', '.de', 'GER', null, 'Deutschland');
INSERT INTO `countrycodes` VALUES ('48', 'DM', 'DMA', '212', '.dm', 'DMA', null, 'Dominica');
INSERT INTO `countrycodes` VALUES ('49', 'DO', 'DOM', '214', '.do', 'DOM', null, 'Dominikanische Republik');
INSERT INTO `countrycodes` VALUES ('50', 'DJ', 'DJI', '262', '.dj', 'DJI', null, 'Dschibuti');
INSERT INTO `countrycodes` VALUES ('51', 'EC', 'ECU', '218', '.ec', 'ECU', null, 'Ecuador');
INSERT INTO `countrycodes` VALUES ('52', 'SV', 'SLV', '222', '.sv', 'ESA', null, 'El Salvador');
INSERT INTO `countrycodes` VALUES ('53', 'ER', 'ERI', '232', '.er', 'ERI', null, 'Eritrea');
INSERT INTO `countrycodes` VALUES ('54', 'EE', 'EST', '233', '.ee', 'EST', null, 'Estland');
INSERT INTO `countrycodes` VALUES ('55', 'FK', 'FLK', '238', '.fk', null, null, 'Falklandinseln');
INSERT INTO `countrycodes` VALUES ('56', 'FO', 'FRO', '234', '.fo', 'FRO', null, 'Färöer');
INSERT INTO `countrycodes` VALUES ('57', 'FJ', 'FJI', '242', '.fj', 'FIJ', null, 'Fidschi');
INSERT INTO `countrycodes` VALUES ('58', 'FI', 'FIN', '246', '.fi', 'FIN', null, 'Finnland');
INSERT INTO `countrycodes` VALUES ('59', 'FR', 'FRA', '250', '.fr', 'FRA', null, 'Frankreich');
INSERT INTO `countrycodes` VALUES ('60', 'GF', 'GUF', '254', '.gf', null, null, 'Französisch-Guayana');
INSERT INTO `countrycodes` VALUES ('61', 'PF', 'PYF', '258', '.pf', null, null, 'Französisch-Polynesien');
INSERT INTO `countrycodes` VALUES ('62', 'TF', 'ATF', '260', '.tf', null, null, 'Französische Süd- und Antarktisgebiete');
INSERT INTO `countrycodes` VALUES ('63', 'GA', 'GAB', '266', '.ga', 'GAB', null, 'Gabun');
INSERT INTO `countrycodes` VALUES ('64', 'GM', 'GMB', '270', '.gm', 'GAM', null, 'Gambia');
INSERT INTO `countrycodes` VALUES ('65', 'GE', 'GEO', '268', '.ge', 'GEO', null, 'Georgien');
INSERT INTO `countrycodes` VALUES ('66', 'GH', 'GHA', '288', '.gh', 'GHA', null, 'Ghana');
INSERT INTO `countrycodes` VALUES ('67', 'GI', 'GIB', '292', '.gi', null, null, 'Gibraltar');
INSERT INTO `countrycodes` VALUES ('68', 'GD', 'GRD', '308', '.gd', 'GRN', null, 'Grenada');
INSERT INTO `countrycodes` VALUES ('69', 'GR', 'GRC', '300', '.gr', 'GRE', null, 'Griechenland');
INSERT INTO `countrycodes` VALUES ('70', 'GL', 'GRL', '304', '.gl', null, null, 'Grönland');
INSERT INTO `countrycodes` VALUES ('71', 'GP', 'GLP', '312', '.gp', null, null, 'Guadeloupe');
INSERT INTO `countrycodes` VALUES ('72', 'GU', 'GUM', '316', '.gu', 'GUM', null, 'Guam');
INSERT INTO `countrycodes` VALUES ('73', 'GT', 'GTM', '320', '.gt', 'GUA', null, 'Guatemala');
INSERT INTO `countrycodes` VALUES ('74', 'GG', 'GGY', '831', '.gg', null, null, 'Guernsey');
INSERT INTO `countrycodes` VALUES ('75', 'GN', 'GIN', '324', '.gn', 'GUI', null, 'Guinea');
INSERT INTO `countrycodes` VALUES ('76', 'GW', 'GNB', '624', '.gw', 'GBS', null, 'Guinea-Bissau');
INSERT INTO `countrycodes` VALUES ('77', 'GY', 'GUY', '328', '.gy', 'GUY', null, 'Guyana');
INSERT INTO `countrycodes` VALUES ('78', 'HT', 'HTI', '332', '.ht', 'HAI', null, 'Haiti');
INSERT INTO `countrycodes` VALUES ('79', 'HM', 'HMD', '334', '.hm', null, null, 'Heard und McDonaldinseln');
INSERT INTO `countrycodes` VALUES ('80', 'HN', 'HND', '340', '.hn', 'HON', null, 'Honduras');
INSERT INTO `countrycodes` VALUES ('81', 'HK', 'HKG', '344', '.hk', 'HKG', null, 'Hongkong');
INSERT INTO `countrycodes` VALUES ('82', 'IN', 'IND', '356', '.in', 'IND', null, 'Indien');
INSERT INTO `countrycodes` VALUES ('83', 'ID', 'IDN', '360', '.id', 'INA', null, 'Indonesien');
INSERT INTO `countrycodes` VALUES ('84', 'IM', 'IMN', '833', '.im', null, null, 'Insel Man');
INSERT INTO `countrycodes` VALUES ('85', 'IQ', 'IRQ', '368', '.iq', 'IRQ', null, 'Irak');
INSERT INTO `countrycodes` VALUES ('86', 'IR', 'IRN', '364', '.ir', 'IRI', null, 'Iran, Islamische Republik');
INSERT INTO `countrycodes` VALUES ('87', 'IE', 'IRL', '372', '.ie', 'IRL', null, 'Irland');
INSERT INTO `countrycodes` VALUES ('88', 'IS', 'ISL', '352', '.is', 'ISL', null, 'Island');
INSERT INTO `countrycodes` VALUES ('89', 'IL', 'ISR', '376', '.il', 'ISR', null, 'Israel');
INSERT INTO `countrycodes` VALUES ('90', 'IT', 'ITA', '380', '.it', 'ITA', null, 'Italien');
INSERT INTO `countrycodes` VALUES ('91', 'JM', 'JAM', '388', '.jm', 'JAM', null, 'Jamaika');
INSERT INTO `countrycodes` VALUES ('92', 'JP', 'JPN', '392', '.jp', 'JPN', null, 'Japan');
INSERT INTO `countrycodes` VALUES ('93', 'YE', 'YEM', '887', '.ye', 'YEM', null, 'Jemen');
INSERT INTO `countrycodes` VALUES ('94', 'JE', 'JEY', '832', '.je', null, null, 'Jersey');
INSERT INTO `countrycodes` VALUES ('95', 'JO', 'JOR', '400', '.jo', 'JOR', null, 'Jordanien');
INSERT INTO `countrycodes` VALUES ('96', 'KY', 'CYM', '136', '.ky', 'CAY', null, 'Kaimaninseln');
INSERT INTO `countrycodes` VALUES ('97', 'KH', 'KHM', '116', '.kh', 'CAM', null, 'Kambodscha');
INSERT INTO `countrycodes` VALUES ('98', 'CM', 'CMR', '120', '.cm', 'CMR', null, 'Kamerun');
INSERT INTO `countrycodes` VALUES ('99', 'CA', 'CAN', '124', '.ca', 'CAN', null, 'Kanada');
INSERT INTO `countrycodes` VALUES ('100', 'CV', 'CPV', '132', '.cv', 'CPV', null, 'Kap Verde');
INSERT INTO `countrycodes` VALUES ('101', 'KZ', 'KAZ', '398', '.kz', 'KAZ', null, 'Kasachstan');
INSERT INTO `countrycodes` VALUES ('102', 'QA', 'QAT', '634', '.qa', 'QAT', null, 'Katar');
INSERT INTO `countrycodes` VALUES ('103', 'KE', 'KEN', '404', '.ke', 'KEN', null, 'Kenia');
INSERT INTO `countrycodes` VALUES ('104', 'KG', 'KGZ', '417', '.kg', 'KGZ', null, 'Kirgisistan');
INSERT INTO `countrycodes` VALUES ('105', 'KI', 'KIR', '296', '.ki', 'KIR', null, 'Kiribati');
INSERT INTO `countrycodes` VALUES ('106', 'CC', 'CCK', '166', '.cc', null, null, 'Kokosinseln');
INSERT INTO `countrycodes` VALUES ('107', 'CO', 'COL', '170', '.co', 'COL', null, 'Kolumbien');
INSERT INTO `countrycodes` VALUES ('108', 'KM', 'COM', '174', '.km', 'COM', null, 'Komoren');
INSERT INTO `countrycodes` VALUES ('109', 'CD', 'COD', '180', '.cd', 'COD', null, 'Kongo, Demokratische Republik');
INSERT INTO `countrycodes` VALUES ('110', 'CG', 'COG', '178', '.cg', 'CGO', null, 'Republik Kongo');
INSERT INTO `countrycodes` VALUES ('111', 'KP', 'PRK', '408', '.kp', 'PRK', null, 'Korea, Demokratische Volksrepublik');
INSERT INTO `countrycodes` VALUES ('112', 'KR', 'KOR', '410', '.kr', 'KOR', null, 'Korea, Republik');
INSERT INTO `countrycodes` VALUES ('113', 'HR', 'HRV', '191', '.hr', 'CRO', null, 'Kroatien');
INSERT INTO `countrycodes` VALUES ('114', 'CU', 'CUB', '192', '.cu', 'CUB', null, 'Kuba');
INSERT INTO `countrycodes` VALUES ('115', 'KW', 'KWT', '414', '.kw', 'KUW', null, 'Kuwait');
INSERT INTO `countrycodes` VALUES ('116', 'LA', 'LAO', '418', '.la', 'LAO', null, 'Laos, Demokratische Volksrepublik');
INSERT INTO `countrycodes` VALUES ('117', 'LS', 'LSO', '426', '.ls', 'LES', null, 'Lesotho');
INSERT INTO `countrycodes` VALUES ('118', 'LV', 'LVA', '428', '.lv', 'LAT', null, 'Lettland');
INSERT INTO `countrycodes` VALUES ('119', 'LB', 'LBN', '422', '.lb', 'LIB', null, 'Libanon');
INSERT INTO `countrycodes` VALUES ('120', 'LR', 'LBR', '430', '.lr', 'LBR', null, 'Liberia');
INSERT INTO `countrycodes` VALUES ('121', 'LY', 'LBY', '434', '.ly', 'LBA', null, 'Libysch-Arabische Dschamahirija');
INSERT INTO `countrycodes` VALUES ('122', 'LI', 'LIE', '438', '.li', 'LIE', null, 'Liechtenstein');
INSERT INTO `countrycodes` VALUES ('123', 'LT', 'LTU', '440', '.lt', 'LTU', null, 'Litauen');
INSERT INTO `countrycodes` VALUES ('124', 'LU', 'LUX', '442', '.lu', 'LUX', null, 'Luxemburg');
INSERT INTO `countrycodes` VALUES ('125', 'MO', 'MAC', '446', '.mo', null, null, 'Macao');
INSERT INTO `countrycodes` VALUES ('126', 'MG', 'MDG', '450', '.mg', 'MAD', null, 'Madagaskar');
INSERT INTO `countrycodes` VALUES ('127', 'MW', 'MWI', '454', '.mw', 'MAW', null, 'Malawi');
INSERT INTO `countrycodes` VALUES ('128', 'MY', 'MYS', '458', '.my', 'MAS', null, 'Malaysia');
INSERT INTO `countrycodes` VALUES ('129', 'MV', 'MDV', '462', '.mv', 'MDV', null, 'Malediven');
INSERT INTO `countrycodes` VALUES ('130', 'ML', 'MLI', '466', '.ml', 'MLI', null, 'Mali');
INSERT INTO `countrycodes` VALUES ('131', 'MT', 'MLT', '470', '.mt', 'MLT', null, 'Malta');
INSERT INTO `countrycodes` VALUES ('132', 'MA', 'MAR', '504', '.ma', 'MAR', null, 'Marokko');
INSERT INTO `countrycodes` VALUES ('133', 'MH', 'MHL', '584', '.mh', 'MHL', null, 'Marshallinseln');
INSERT INTO `countrycodes` VALUES ('134', 'MQ', 'MTQ', '474', '.mq', null, null, 'Martinique');
INSERT INTO `countrycodes` VALUES ('135', 'MR', 'MRT', '478', '.mr', 'MTN', null, 'Mauretanien');
INSERT INTO `countrycodes` VALUES ('136', 'MU', 'MUS', '480', '.mu', 'MRI', null, 'Mauritius');
INSERT INTO `countrycodes` VALUES ('137', 'YT', 'MYT', '175', '.yt', null, null, 'Mayotte');
INSERT INTO `countrycodes` VALUES ('138', 'MK', 'MKD', '807', '.mk', 'MKD', null, 'Mazedonien, ehem. jugoslawische Republik');
INSERT INTO `countrycodes` VALUES ('139', 'MX', 'MEX', '484', '.mx', 'MEX', null, 'Mexiko');
INSERT INTO `countrycodes` VALUES ('140', 'FM', 'FSM', '583', '.fm', 'FSM', null, 'Mikronesien');
INSERT INTO `countrycodes` VALUES ('141', 'MD', 'MDA', '498', '.md', 'MDA', null, 'Moldawien');
INSERT INTO `countrycodes` VALUES ('142', 'MC', 'MCO', '492', '.mc', 'MON', null, 'Monaco');
INSERT INTO `countrycodes` VALUES ('143', 'MN', 'MNG', '496', '.mn', 'MGL', null, 'Mongolei');
INSERT INTO `countrycodes` VALUES ('144', 'ME', 'MNE', '499', '.me', 'MNE', null, 'Montenegro');
INSERT INTO `countrycodes` VALUES ('145', 'MS', 'MSR', '500', '.ms', null, null, 'Montserrat');
INSERT INTO `countrycodes` VALUES ('146', 'MZ', 'MOZ', '508', '.mz', 'MOZ', null, 'Mosambik');
INSERT INTO `countrycodes` VALUES ('147', 'MM', 'MMR', '104', '.mm', 'MYA', null, 'Myanmar');
INSERT INTO `countrycodes` VALUES ('148', 'NA', 'NAM', '516', '.na', 'NAM', null, 'Namibia');
INSERT INTO `countrycodes` VALUES ('149', 'NR', 'NRU', '520', '.nr', 'NRU', null, 'Nauru');
INSERT INTO `countrycodes` VALUES ('150', 'NP', 'NPL', '524', '.np', 'NEP', null, 'Nepal');
INSERT INTO `countrycodes` VALUES ('151', 'NC', 'NCL', '540', '.nc', null, null, 'Neukaledonien');
INSERT INTO `countrycodes` VALUES ('152', 'NZ', 'NZL', '554', '.nz', 'NZL', null, 'Neuseeland');
INSERT INTO `countrycodes` VALUES ('153', 'NI', 'NIC', '558', '.ni', 'NCA', null, 'Nicaragua');
INSERT INTO `countrycodes` VALUES ('154', 'NL', 'NLD', '528', '.nl', 'NED', null, 'Niederlande');
INSERT INTO `countrycodes` VALUES ('155', 'AN', 'ANT', '530', '.an', 'AHO', null, 'Niederländische Antillen');
INSERT INTO `countrycodes` VALUES ('156', 'NE', 'NER', '562', '.ne', 'NIG', null, 'Niger');
INSERT INTO `countrycodes` VALUES ('157', 'NG', 'NGA', '566', '.ng', 'NGR', null, 'Nigeria');
INSERT INTO `countrycodes` VALUES ('158', 'NU', 'NIU', '570', '.nu', null, null, 'Niue');
INSERT INTO `countrycodes` VALUES ('159', 'MP', 'MNP', '580', '.mp', null, null, 'Nördliche Marianen');
INSERT INTO `countrycodes` VALUES ('160', 'NF', 'NFK', '574', '.nf', null, null, 'Norfolkinsel');
INSERT INTO `countrycodes` VALUES ('161', 'NO', 'NOR', '578', '.no', 'NOR', null, 'Norwegen');
INSERT INTO `countrycodes` VALUES ('162', 'OM', 'OMN', '512', '.om', 'OMA', null, 'Oman');
INSERT INTO `countrycodes` VALUES ('163', 'AT', 'AUT', '040', '.at', 'AUT', null, 'Österreich');
INSERT INTO `countrycodes` VALUES ('164', 'TL', 'TLS', '626', '.tl', 'TLS', null, 'Osttimor');
INSERT INTO `countrycodes` VALUES ('165', 'PK', 'PAK', '586', '.pk', 'PAK', null, 'Pakistan');
INSERT INTO `countrycodes` VALUES ('166', 'PS', 'PSE', '275', '.ps', 'PLE', null, 'Palästinensische Autonomiegebiete');
INSERT INTO `countrycodes` VALUES ('167', 'PW', 'PLW', '585', '.pw', 'PLW', null, 'Palau');
INSERT INTO `countrycodes` VALUES ('168', 'PA', 'PAN', '591', '.pa', 'PAN', null, 'Panama');
INSERT INTO `countrycodes` VALUES ('169', 'PG', 'PNG', '598', '.pg', 'PNG', null, 'Papua-Neuguinea');
INSERT INTO `countrycodes` VALUES ('170', 'PY', 'PRY', '600', '.py', 'PAR', null, 'Paraguay');
INSERT INTO `countrycodes` VALUES ('171', 'PE', 'PER', '604', '.pe', 'PER', null, 'Peru');
INSERT INTO `countrycodes` VALUES ('172', 'PH', 'PHL', '608', '.ph', 'PHI', null, 'Philippinen');
INSERT INTO `countrycodes` VALUES ('173', 'PN', 'PCN', '612', '.pn', null, null, 'Pitcairninseln');
INSERT INTO `countrycodes` VALUES ('174', 'PL', 'POL', '616', '.pl', 'POL', null, 'Polen');
INSERT INTO `countrycodes` VALUES ('175', 'PT', 'PRT', '620', '.pt', 'POR', null, 'Portugal');
INSERT INTO `countrycodes` VALUES ('176', 'PR', 'PRI', '630', '.pr', 'PUR', null, 'Puerto Rico');
INSERT INTO `countrycodes` VALUES ('177', 'RE', 'REU', '638', '.re', null, null, 'Réunion');
INSERT INTO `countrycodes` VALUES ('178', 'RW', 'RWA', '646', '.rw', 'RWA', null, 'Ruanda');
INSERT INTO `countrycodes` VALUES ('179', 'RO', 'ROU', '642', '.ro', 'ROU', null, 'Rumänien');
INSERT INTO `countrycodes` VALUES ('180', 'RU', 'RUS', '643', '.ru', 'RUS', null, 'Russische Föderation');
INSERT INTO `countrycodes` VALUES ('181', 'SB', 'SLB', '090', '.sb', 'SOL', null, 'Salomonen');
INSERT INTO `countrycodes` VALUES ('182', 'BL', 'BLM', '652', null, null, null, 'Saint-Barthélemy');
INSERT INTO `countrycodes` VALUES ('183', 'MF', 'MAF', '663', null, null, null, 'Saint-Martin (franz. Teil)');
INSERT INTO `countrycodes` VALUES ('184', 'ZM', 'ZMB', '894', '.zm', 'ZAM', null, 'Sambia');
INSERT INTO `countrycodes` VALUES ('185', 'WS', 'WSM', '882', '.ws', 'SAM', null, 'Samoa');
INSERT INTO `countrycodes` VALUES ('186', 'SM', 'SMR', '674', '.sm', 'SMR', null, 'San Marino');
INSERT INTO `countrycodes` VALUES ('187', 'ST', 'STP', '678', '.st', 'STP', null, 'São Tomé und Príncipe');
INSERT INTO `countrycodes` VALUES ('188', 'SA', 'SAU', '682', '.sa', 'KSA', null, 'Saudi-Arabien');
INSERT INTO `countrycodes` VALUES ('189', 'SE', 'SWE', '752', '.se', 'SWE', null, 'Schweden');
INSERT INTO `countrycodes` VALUES ('190', 'CH', 'CHE', '756', '.ch', 'SUI', null, 'Schweiz');
INSERT INTO `countrycodes` VALUES ('191', 'SN', 'SEN', '686', '.sn', 'SEN', null, 'Senegal');
INSERT INTO `countrycodes` VALUES ('192', 'RS', 'SRB', '688', '.rs', 'SRB', null, 'Serbien');
INSERT INTO `countrycodes` VALUES ('193', 'SC', 'SYC', '690', '.sc', 'SEY', null, 'Seychellen');
INSERT INTO `countrycodes` VALUES ('194', 'SL', 'SLE', '694', '.sl', 'SLE', null, 'Sierra Leone');
INSERT INTO `countrycodes` VALUES ('195', 'ZW', 'ZWE', '716', '.zw', 'ZIM', null, 'Simbabwe');
INSERT INTO `countrycodes` VALUES ('196', 'SG', 'SGP', '702', '.sg', 'SIN', null, 'Singapur');
INSERT INTO `countrycodes` VALUES ('197', 'SK', 'SVK', '703', '.sk', 'SVK', null, 'Slowakei');
INSERT INTO `countrycodes` VALUES ('198', 'SI', 'SVN', '705', '.si', 'SLO', null, 'Slowenien');
INSERT INTO `countrycodes` VALUES ('199', 'SO', 'SOM', '706', '.so', 'SOM', null, 'Somalia');
INSERT INTO `countrycodes` VALUES ('200', 'ES', 'ESP', '724', '.es', 'ESP', null, 'Spanien');
INSERT INTO `countrycodes` VALUES ('201', 'LK', 'LKA', '144', '.lk', 'SRI', null, 'Sri Lanka');
INSERT INTO `countrycodes` VALUES ('202', 'SH', 'SHN', '654', '.sh', null, null, 'St. Helena');
INSERT INTO `countrycodes` VALUES ('203', 'KN', 'KNA', '659', '.kn', 'SKN', null, 'St. Kitts und Nevis');
INSERT INTO `countrycodes` VALUES ('204', 'LC', 'LCA', '662', '.lc', 'LCA', null, 'St. Lucia');
INSERT INTO `countrycodes` VALUES ('205', 'PM', 'SPM', '666', '.pm', null, null, 'Saint-Pierre und Miquelon');
INSERT INTO `countrycodes` VALUES ('206', 'VC', 'VCT', '670', '.vc', 'VIN', null, 'St. Vincent und die Grenadinen');
INSERT INTO `countrycodes` VALUES ('207', 'ZA', 'ZAF', '710', '.za', 'RSA', null, 'Südafrika');
INSERT INTO `countrycodes` VALUES ('208', 'SD', 'SDN', '736', '.sd', 'SUD', null, 'Sudan');
INSERT INTO `countrycodes` VALUES ('209', 'GS', 'SGS', '239', '.gs', null, null, 'Südgeorgien und die Südlichen Sandwichinseln');
INSERT INTO `countrycodes` VALUES ('210', 'SR', 'SUR', '740', '.sr', 'SUR', null, 'Suriname');
INSERT INTO `countrycodes` VALUES ('211', 'SJ', 'SJM', '744', '.sj', null, null, 'Svalbard');
INSERT INTO `countrycodes` VALUES ('212', 'SZ', 'SWZ', '748', '.sz', 'SWZ', null, 'Swasiland');
INSERT INTO `countrycodes` VALUES ('213', 'SY', 'SYR', '760', '.sy', 'SYR', null, 'Syrien, Arabische Republik');
INSERT INTO `countrycodes` VALUES ('214', 'TJ', 'TJK', '762', '.tj', 'TJK', null, 'Tadschikistan');
INSERT INTO `countrycodes` VALUES ('215', 'TW', 'TWN', '158', '.tw', 'TPE', null, 'Taiwan (Republik China)');
INSERT INTO `countrycodes` VALUES ('216', 'TZ', 'TZA', '834', '.tz', 'TAN', null, 'Tansania, Vereinigte Republik');
INSERT INTO `countrycodes` VALUES ('217', 'TH', 'THA', '764', '.th', 'THA', null, 'Thailand');
INSERT INTO `countrycodes` VALUES ('218', 'TG', 'TGO', '768', '.tg', 'TOG', null, 'Togo');
INSERT INTO `countrycodes` VALUES ('219', 'TK', 'TKL', '772', '.tk', null, null, 'Tokelau');
INSERT INTO `countrycodes` VALUES ('220', 'TO', 'TON', '776', '.to', 'TGA', null, 'Tonga');
INSERT INTO `countrycodes` VALUES ('221', 'TT', 'TTO', '780', '.tt', 'TRI', null, 'Trinidad und Tobago');
INSERT INTO `countrycodes` VALUES ('222', 'TD', 'TCD', '148', '.td', 'CHA', null, 'Tschad');
INSERT INTO `countrycodes` VALUES ('223', 'CZ', 'CZE', '203', '.cz', 'CZE', null, 'Tschechische Republik');
INSERT INTO `countrycodes` VALUES ('224', 'TN', 'TUN', '788', '.tn', 'TUN', null, 'Tunesien');
INSERT INTO `countrycodes` VALUES ('225', 'TR', 'TUR', '792', '.tr', 'TUR', null, 'Türkei');
INSERT INTO `countrycodes` VALUES ('226', 'TM', 'TKM', '795', '.tm', 'TKM', null, 'Turkmenistan');
INSERT INTO `countrycodes` VALUES ('227', 'TC', 'TCA', '796', '.tc', null, null, 'Turks- und Caicosinseln');
INSERT INTO `countrycodes` VALUES ('228', 'TV', 'TUV', '798', '.tv', 'TUV', null, 'Tuvalu');
INSERT INTO `countrycodes` VALUES ('229', 'UG', 'UGA', '800', '.ug', 'UGA', null, 'Uganda');
INSERT INTO `countrycodes` VALUES ('230', 'UA', 'UKR', '804', '.ua', 'UKR', null, 'Ukraine');
INSERT INTO `countrycodes` VALUES ('231', 'HU', 'HUN', '348', '.hu', 'HUN', null, 'Ungarn');
INSERT INTO `countrycodes` VALUES ('232', 'UM', 'UMI', '581', '.um', null, null, 'United States Minor Outlying Islands');
INSERT INTO `countrycodes` VALUES ('233', 'UY', 'URY', '858', '.uy', 'URU', null, 'Uruguay');
INSERT INTO `countrycodes` VALUES ('234', 'UZ', 'UZB', '860', '.uz', 'UZB', null, 'Usbekistan');
INSERT INTO `countrycodes` VALUES ('235', 'VU', 'VUT', '548', '.vu', 'VAN', null, 'Vanuatu');
INSERT INTO `countrycodes` VALUES ('236', 'VA', 'VAT', '336', '.va', null, null, 'Vatikanstadt');
INSERT INTO `countrycodes` VALUES ('237', 'VE', 'VEN', '862', '.ve', 'VEN', null, 'Venezuela');
INSERT INTO `countrycodes` VALUES ('238', 'AE', 'ARE', '784', '.ae', 'UAE', null, 'Vereinigte Arabische Emirate');
INSERT INTO `countrycodes` VALUES ('239', 'US', 'USA', '840', '.us', 'USA', null, 'Vereinigte Staaten von Amerika');
INSERT INTO `countrycodes` VALUES ('240', 'GB', 'GBR', '826', '.uk', 'GBR', null, 'Vereinigtes Königreich Großbritannien und Nordirland');
INSERT INTO `countrycodes` VALUES ('241', 'VN', 'VNM', '704', '.vn', 'VIE', null, 'Vietnam');
INSERT INTO `countrycodes` VALUES ('242', 'WF', 'WLF', '876', '.wf', null, null, 'Wallis und Futuna');
INSERT INTO `countrycodes` VALUES ('243', 'CX', 'CXR', '162', '.cx', null, null, 'Weihnachtsinsel');
INSERT INTO `countrycodes` VALUES ('244', 'EH', 'ESH', '732', '.eh', null, null, 'Westsahara');
INSERT INTO `countrycodes` VALUES ('245', 'CF', 'CAF', '140', '.cf', 'CAF', null, 'Zentralafrikanische Republik');
INSERT INTO `countrycodes` VALUES ('246', 'CY', 'CYP', '196', '.cy', 'CYP', null, 'Zypern');

-- ----------------------------
-- Procedure structure for `InsertCountryCode`
-- ----------------------------
DROP PROCEDURE IF EXISTS `InsertCountryCode`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `InsertCountryCode`(iso3166Alpha2 CHAR(2), iso3166Alpha3 CHAR(3), iso3166Numeric INT(3) UNSIGNED, tld CHAR(3), ioc CHAR(3), nameEN VARCHAR(64), nameDE VARCHAR(64))
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

INSERT INTO countrycodes (
	countrycodes.Country_ISO3166_ALPHA2,
	countrycodes.Country_ISO3166_ALPHA3,
	countrycodes.Country_ISO3166_Numeric,
	countrycodes.Country_TLD,
	countrycodes.Country_IOC,
	countrycodes.Country_Name_EN,
	countrycodes.Country_Name_DE)
VALUES (iso3166Alpha2,
	iso3166Alpha3,
	iso3166Numeric,
	tld,
	ioc,
	nameEN,
	nameDE);

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `QueryCountryCodes`
-- ----------------------------
DROP PROCEDURE IF EXISTS `QueryCountryCodes`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `QueryCountryCodes`(iso3166Alpha2List TEXT)
    DETERMINISTIC
BEGIN

IF iso3166Alpha2List IS NULL THEN
	SELECT	Country_ID,
		Country_ISO3166_ALPHA2,
		Country_ISO3166_ALPHA3,
		Country_ISO3166_Numeric,
		Country_TLD,
		Country_IOC,
		Country_Name_EN,
		Country_Name_DE
	FROM	countrycodes
	ORDER BY Country_ID;
ELSE
	CALL SplitString( iso3166Alpha2List, ',');

	SELECT	Country_ID,
		Country_ISO3166_ALPHA2,
		Country_ISO3166_ALPHA3,
		Country_ISO3166_Numeric,
		Country_TLD,
		Country_IOC,
		Country_Name_EN,
		Country_Name_DE
	FROM	countrycodes
	INNER JOIN SplitValues sv ON Country_ISO3166_ALPHA2 = sv.value
	ORDER BY Country_ID;
	DROP TEMPORARY TABLE SplitValues;
END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `QueryEmailDomainIsBlacklisted`
-- ----------------------------
DROP PROCEDURE IF EXISTS `QueryEmailDomainIsBlacklisted`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `QueryEmailDomainIsBlacklisted`(domain VARCHAR(255))
    READS SQL DATA
    DETERMINISTIC
BEGIN

SELECT	COUNT(blacklistedemaildomains.BlacklistedEmailDomain_ID)
FROM	blacklistedemaildomains
WHERE blacklistedemaildomains.BlacklistedEmailDomain_Name = domain;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `SplitString`
-- ----------------------------
DROP PROCEDURE IF EXISTS `SplitString`;
DELIMITER ;;
CREATE DEFINER=`www`@`localhost` PROCEDURE `SplitString`(IN input TEXT, IN `delimiter` VARCHAR(10))
BEGIN

DECLARE currentPosition INT DEFAULT 1;
DECLARE remainder TEXT;
DECLARE currentString VARCHAR(1000);
DECLARE delimiterLength TINYINT UNSIGNED;

DROP TEMPORARY TABLE IF EXISTS SplitValues;
CREATE TEMPORARY TABLE SplitValues (
	value VARCHAR(1000) NOT NULL PRIMARY KEY
) ENGINE=MyISAM;

SET remainder = input;
SET delimiterLength = CHAR_LENGTH(`delimiter`);

WHILE CHAR_LENGTH(remainder) > 0 AND currentPosition > 0 DO

	SET currentPosition = INSTR(remainder, `delimiter`);

	IF currentPosition = 0 THEN
		SET currentString = remainder;
	ELSE
		SET currentString = LEFT(remainder, currentPosition - 1);
	END IF;
	SET currentString = TRIM(currentString);
	IF currentString != '' THEN
		INSERT INTO SplitValues VALUES (currentString);
	END IF;
	SET remainder = SUBSTRING(remainder, currentPosition + delimiterLength);

END WHILE;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `TruncateCountryCodes`
-- ----------------------------
DROP PROCEDURE IF EXISTS `TruncateCountryCodes`;
DELIMITER ;;
CREATE DEFINER=`www-site-admin`@`localhost` PROCEDURE `TruncateCountryCodes`()
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN

TRUNCATE TABLE countrycodes;
ALTER TABLE countrycodes AUTO_INCREMENT = 1;

END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `QueryEmailDomainIsBlacklistedFunc`
-- ----------------------------
DROP FUNCTION IF EXISTS `QueryEmailDomainIsBlacklistedFunc`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `QueryEmailDomainIsBlacklistedFunc`(domain VARCHAR(255)) RETURNS tinyint(1) unsigned
    DETERMINISTIC
BEGIN

DECLARE isBlacklisted TINYINT(1) UNSIGNED;

SELECT	COUNT(blacklistedemaildomains.BlacklistedEmailDomain_ID)
INTO	isBlacklisted
FROM	blacklistedemaildomains
WHERE blacklistedemaildomains.BlacklistedEmailDomain_Name = domain;

RETURN isBlacklisted;

END
;;
DELIMITER ;
