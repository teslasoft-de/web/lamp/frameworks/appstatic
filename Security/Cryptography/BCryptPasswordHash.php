<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2014
 * File: BCryptPasswordHash.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Security\Cryptography;

/**
 * Provides basic utillities for password hashing.
 * 
 * @package AppStatic
 * @name BCrypt
 * @version 1.0
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
final class BCryptPasswordHash extends PasswordHash
{

    function __construct()
    {
        parent::__construct( 4, 31, 22 );
    }

    /**
     * Hashes the given password, using blowfish when available, with
     * fallback to PBKDF2 repeated hashing.
     *
     * @param string $password
     * 		The password to hash.
     *
     * @param int $cost
     * 		A cost parameter - the base 2 logarithm of the iteration count
     * 		for the underlying algorithm. Must be in the range 04-31.
     *
     * @return string
     * 		The hashed string, including the generated salt.
     */
    public function Hash( $password, $cost = PASSWORD_BCRYPT_ROUNDS )
    {
        return parent::Hash( $password, $cost );
    }

    /**
     * Hashes the given password, using blowfish when available, with
     * fallback to PBKDF2 repeated hashing.
     *
     * @param string $password
     *        The password to hash.
     *
     * @param int $cost
     *        A cost parameter - the base 2 logarithm of the iteration count
     *        for the underlying algorithm. Must be in the range 4-31.
     *
     * @param string $salt The salt.
     * @return string The hashed string, including the generated salt.
     */
    protected function HashProc( $password, $cost, $salt )
    {
        // If we have blowfish, use it
        if (CRYPT_BLOWFISH === 1) {
            $algorithm = version_compare( PHP_VERSION, '5.3.7' ) >= 0
                    ? '2y' // BCrypt, with fixed unicode problem
                    : '2a'; // BCrypt
            return crypt( $password, sprintf( '$%s$%02d$%s', $algorithm, $cost, $salt ) );
        }
        $pdkdf2 = new PBKDF2PasswordHash();
        // Fall back to PBKDF2
        return $pdkdf2->Hash( $password, $cost );
    }

    /**
     * Checks the given input against the stored hash.
     *
     * @param string $password
     * 		The user input to check.
     *
     * @param string $hash
     * 		The stored salt + hash.
     *
     * @return bool
     * 		TRUE if the given input matches the original
     * 		password, otherwise FALSE.
     */
    public function Validate( $password, $hash )
    {
        if (empty( $password ))
            throw new \InvalidArgumentException( 'Cannot hash an empty password.' );

        // First try the fall back method, then crypt
        $pdkdf2 = new PBKDF2PasswordHash();        
        $answer = $pdkdf2->Validate( $password, $hash );
        if ($answer) {
            $this->Cost = $pdkdf2->getCost();
            return $answer;
        }

        // Check if the given salt is valid or not
        if (!preg_match( '%\$(2a|2y|2x)\$(\d{2})\$([a-zA-Z0-9\./]{' . $this->SaltLength . '})(.*)$%', $hash, $matches ))
            return false;

        $this->Cost = $matches[ 2 ];

        unset( $matches );
        
        return parent::Validate( $password, $hash );
    }

}
