<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 03.01.2014
 * File: SHA256PasswordHash.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Security\Cryptography;

/**
 * Description of SHA256
 * 
 * @package AppStatic
 * @name SHA256
 * @version 1.0
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
final class SHA256PasswordHash extends PasswordHash
{
    function __construct()
    {
        parent::__construct( 1000, 999999999, 16 );
    }

    /**
     * Hashes the given password, using SHA256.
     *
     * @param string $password
     *        The password to hash.
     *
     * @param int $cost
     *        A cost parameter - the base 2 logarithm of the iteration count
     *        for the underlying algorithm. Must be in the range 1,000-999,999,999.
     * @return string The hashed string, including the generated salt.
     * The hashed string, including the generated salt.
     * @throws PasswordHashException
     */
    public function Hash( $password, $cost = PASSWORD_SHA256_ROUNDS )
    {
        if (CRYPT_SHA256 !== 1)
            throw new PasswordHashException( 'The CRYPT_SHA256 algorithm is required.' );
        
        return parent::Hash( $password, $cost );
    }
    
    protected function HashProc( $password, $cost, $salt )
    {        
        return crypt( $password, sprintf( '$5$rounds=%d$%s', $cost, $salt ) );
    }
    
    public function Validate( $password, $hash )
    {
        if (empty( $password ))
            throw new \InvalidArgumentException( 'Cannot hash an empty password.' );
        
        // Check if the given salt is valid or not
        if (!preg_match( '%\$5\$rounds=(\d{4,9})\$([a-zA-Z0-9\./]{' . $this->SaltLength . '})\$(.*)$%', $hash, $matches ))
            return false;

        $this->Cost = $matches[ 1 ];

        unset( $matches );
        
        return parent::Validate( $password, $hash );
    }

}
