<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 02.01.2014
 * File: PasswordHash.Benchmark.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Security\Cryptography;

use AppStatic\Core\Timer as Timer;
use AppStatic\Security\Password;

// Load AppStatic environment
require_once __DIR__ . '/../../Include.inc.php';

// Define benchmark parameters
define('BENCHMARK_VERIFICATIONS_PER_SECOND', 5);
define('BENCHMARK_CURRENT_SETTINGS', 'Current Settings');
define('BENCHMARK_MD5', 'MD5');
define('BENCHMARK_BCrypt', 'BCrypt');
define('BENCHMARK_PBKDF2', 'PBKDF2');
define('BENCHMARK_SHA256', 'SHA256');
define('BENCHMARK_SHA512', 'SHA512');
define('BENCHMARK_TWOFISH', 'Twofish');

global $actionList, $hashAlgorithm, $twofishCrypt;

// Define available benchmark tasks and algorithms
$algorithmList = array(
    BENCHMARK_CURRENT_SETTINGS => null,
    BENCHMARK_BCrypt => new BCryptPasswordHash(),
    BENCHMARK_MD5 => new MD5PasswordHash(),
    BENCHMARK_PBKDF2 => new PBKDF2PasswordHash(),
    BENCHMARK_SHA256 => new SHA256PasswordHash(),
    BENCHMARK_SHA512 => new SHA512PasswordHash());
$actionList = array_keys($algorithmList);

// Read GET parameters
$hashAlgorithm = filter_input(INPUT_GET, 'hashAlgorithm');
if (!$hashAlgorithm)
    $hashAlgorithm = BENCHMARK_CURRENT_SETTINGS;

$twofishCrypt = filter_input(INPUT_GET, 'twofishCrypt');

// Generate Password
$password = Password::Generate();

$results = array();

// Remove unused benchmark task
unset($algorithmList[BENCHMARK_CURRENT_SETTINGS]);

// Run Benchmark
switch ($hashAlgorithm) {
    case BENCHMARK_CURRENT_SETTINGS:
        // Create MD5 benchmark at first
        $results [] = HashBenchmark($algorithmList[BENCHMARK_MD5], $password, null, $twofishCrypt);
        unset($algorithmList[BENCHMARK_MD5]);

        // Benchmark all other algorithms once with default cost parameters
        foreach ($algorithmList as $hashCrypt)
            $results [] = HashBenchmark($hashCrypt, $password, null, $twofishCrypt);
        break;
    default:
        // Benchmark the selected has algorithm until the BENCHMARK_VERIFICATIONS_PER_SECOND have been reached.
        if (array_key_exists($hashAlgorithm, $algorithmList))
            Benchmark($results, $algorithmList[$hashAlgorithm], $password, $twofishCrypt);
        else
            trigger_error("Unknown hash algorithm specified.");
        break;
}

/**
 * Executes the benchmark procedure on the specified PasswordHash object with the specified password.
 * @param array $results
 * @param PasswordHash $passwordHash The PasswordHash instance
 * @param $password
 * @param bool $twofishCrypt
 * @return array The benchmark results
 */
function Benchmark(array &$results, PasswordHash $passwordHash, $password, $twofishCrypt = false)
{
    $cost = $passwordHash->getMinCost();
    do {
        $results [] = $result = HashBenchmark(
            $passwordHash,
            $password,
            $passwordHash->getMinCost() <= 31
                ? $cost++
                : $cost += $cost,
            $twofishCrypt);
        if ($passwordHash instanceof MD5PasswordHash)
            break;
    } while ($result['Verifications/s_'] > BENCHMARK_VERIFICATIONS_PER_SECOND);
}


/**
 * Executes the benchmark on the specified PasswordHash object and the specified password.
 * @param PasswordHash $passwordHash The PasswordHash instance
 * @param $password
 * @param $cost
 * @param bool $twofishCrypt
 * @return array The benchmark results
 */
function HashBenchmark(PasswordHash $passwordHash, $password, $cost = null, $twofishCrypt = false)
{
    $results = array();

    /**
     * The test procedure helper function collects all test results.
     * @param string $name
     * @param \Closure $action
     * @param \Closure $callback
     */
    $benchmark = function ($name, $action, $callback = null) use (&$results) {
        $timer = new Timer();
        $timer->Start();
        $results[$name] = $action($results);
        $timer->Stop();
        if ($callback != null)
            $callback($timer, $results);
    };

    $benchmark('Algorithm',
        function () use ($passwordHash) {
            $split = preg_split('~\\\\~', $passwordHash->getClassName());
            return preg_replace("~PasswordHash~", '', array_pop($split));
        });

    $results['Password_'] = $password;

    $benchmark('Hash',
        function (&$tests) use ($passwordHash, $password, $cost) {
            $hash = $cost
                ? $passwordHash->Hash($password, $cost)
                : $passwordHash->Hash($password);
            $tests['Salt'] = $passwordHash->getSalt();
            return $hash;
        }, function (Timer $timer, &$tests) use ($passwordHash) {
            // TODO: Add power display for PBKDF2
            $tests['Cost_'] = $tests['Algorithm'] == 'BCrypt'
                ? new Power(2, $passwordHash->getCost())
                : $passwordHash->getCost();
            $tests['Computed_'] = $timer->getTotal();
            $total = $timer->getTotal(false);
            $tests['Calculations/s_'] = round(1 / $total, 1);
            $tests['Calculations/d_'] = number_format(round(60 * 60 * 24 / $total), 0, '.', ',');
        });

    $benchmark('Verified_',
        function ($tests) use ($passwordHash, $password) {
            return (int)$passwordHash->Validate($password, $tests['Hash']);
        }, function (Timer $timer, &$tests) {
            $tests['Verify Time_'] = $timer->getTotal();
            $total = $timer->getTotal(false);
            $tests['Verifications/s_'] = round(1 / $total, 1);
            $tests['Verifications/d_'] = number_format(round(60 * 60 * 24 / $total), 0, '.', ',');
        });

    if ($twofishCrypt) {
        $benchmark('Twofish Encrypted',
            function ($tests) use ($password) {
                return base64_encode(TwofishCrypt::Encrypt($tests['Hash'], $password));
            }, function (Timer $timer, &$tests) {
                $tests['EncTime_'] = $timer->getTotal();
            });

        $benchmark('Decrypted_',
            function ($tests) use ($password, $results) {
                $decrypted = TwofishCrypt::Decrypt(base64_decode($results['Twofish Encrypted']), $password);
                return (int)$decrypted == $results['Hash'];
            }, function (Timer $timer, &$tests) {
                $tests['DecTime_'] = $timer->getTotal();
            });
    }

    return $results;
}

class Power{
    public $Base;
    public $Exponent;

    function __construct($Base, $Exponent)
    {
        $this->Base = $Base;
        $this->Exponent = $Exponent;
    }


    /**
     * @return string
     */
    function __toString()
    {
        return ''.pow($this->Base, $this->Exponent);
    }
}

include './PasswordHash.Benchmark.phtml';
