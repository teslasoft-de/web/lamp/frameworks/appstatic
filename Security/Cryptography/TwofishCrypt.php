<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 04.01.2014
 * File: TwofishCrypt.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Security\Cryptography;
use AppStatic\Security\TwofishCryptException;

/**
 * Description of TwofishCrypt
 *
 * @package AppStatic
 * @name TwofishCrypt
 * @version 1.0
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class TwofishCrypt
{

    /**
     * Encrypts data with the TWOFISH algorithm. The IV vector will be
     * included in the resulting binary string.
     * @param string $data Data to encrypt. Trailing \0 characters will get lost.
     * @param string $key This key will be used to encrypt the data. The key
     *   will be hashed to a binary representation before it is used.
     * @return string Returns the encrypted data in form of a binary string.
     * @throws TwofishCryptException
     */
    public static function Encrypt($data, $key)
    {
        if (!defined('MCRYPT_DEV_URANDOM'))
            throw new TwofishCryptException('The MCRYPT_DEV_URANDOM source is required (PHP 5.3).');
        if (!defined('MCRYPT_TWOFISH'))
            throw new TwofishCryptException('The MCRYPT_TWOFISH algorithm is required (PHP 5.3).');

        // The cbc mode is preferable over the ecb mode
        $td = mcrypt_module_open(MCRYPT_TWOFISH, '', MCRYPT_MODE_CBC, '');

        // Twofish accepts a key of 32 bytes. Because usually longer strings
        // with only readable characters are passed, we build a binary string.
        $binaryKey = hash('sha256', $key, true);

        // Create initialization vector of 16 bytes
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_DEV_URANDOM);

        mcrypt_generic_init($td, $binaryKey, $iv);
        $encryptedData = mcrypt_generic($td, $data);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        // Combine iv and encrypted text
        return $iv . $encryptedData;
    }

    /**
     * Decrypts data, formerly encrypted with @see encryptTwofish.
     * @param string $encryptedData Binary string with encrypted data.
     * @param string $key This key will be used to decrypt the data.
     * @return string Returns the original decrypted data.
     * @throws TwofishCryptException
     */
    public static function Decrypt($encryptedData, $key)
    {
        if (!defined('MCRYPT_TWOFISH'))
            throw new TwofishCryptException('The MCRYPT_TWOFISH algorithm is required (PHP 5.3).');

        $td = mcrypt_module_open(MCRYPT_TWOFISH, '', MCRYPT_MODE_CBC, '');

        // Extract initialization vector from encrypted data
        $ivSize = mcrypt_enc_get_iv_size($td);
        $iv = substr($encryptedData, 0, $ivSize);
        $encryptedData = substr($encryptedData, $ivSize);

        $binaryKey = hash('sha256', $key, true);

        mcrypt_generic_init($td, $binaryKey, $iv);
        $decryptedData = mdecrypt_generic($td, $encryptedData);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        // Original data was padded with 0-characters to block-size
        return rtrim($decryptedData, "\0");
    }

}
namespace AppStatic\Security;
use AppStatic\Core\ExceptionBase;


final class TwofishCryptException extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }

}