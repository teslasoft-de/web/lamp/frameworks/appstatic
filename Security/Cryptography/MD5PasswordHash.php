<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 03.01.2014
 * File: MD5PasswordHash.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Security\Cryptography;

/**
 * Description of MD5
 * 
 * @package AppStatic
 * @name MD5
 * @version 1.0
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
final class MD5PasswordHash extends PasswordHash
{

    function __construct()
    {
        parent::__construct( 0, 0, 8 );
    }

    protected function HashProc( $password, $cost, $salt )
    {
        if (CRYPT_MD5 !== 1)
            throw new PasswordHashException( 'The CRYPT_MD5 algorithm is required.' );

        return crypt( $password, '$1$' . $salt );
    }

    public function Validate( $password, $hash )
    {
        if (empty( $password ))
            throw new \InvalidArgumentException( 'Cannot hash an empty password.' );
        
        // Check if the given salt is valid or not
        if (!preg_match( '%\$1\$([a-zA-Z0-9\./]{8})\$(.*)$%', $hash, $matches ))
            return false;

        unset( $matches );
        return parent::Validate( $password, $hash );
    }

}
