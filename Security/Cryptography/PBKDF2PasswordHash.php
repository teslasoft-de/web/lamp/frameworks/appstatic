<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 03.01.2014
 * File: PBKDF2PasswordHash.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Security\Cryptography;

/**
 * Description of PDKDF2
 * 
 * @package AppStatic
 * @name PDKDF2
 * @version 1.0
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
final class PBKDF2PasswordHash extends PasswordHash
{

    function __construct()
    {
        parent::__construct( 4, 2147483648, 22 );
    }
    
    /**
     * Hashes the given password, using PDKDF2.
     *
     * @param string $password
     * 		The password to hash.
     *
     * @param int $cost
     * 		A cost parameter - the base 2 logarithm of the iteration count
     * 		for the underlying algorithm. Must be in the range 4-31.
     *
     * @return string
     * 		The hashed string, including the generated salt.
     */
    public function Hash( $password, $cost = PASSWORD_PBKDF2_ROUNDS )
    {
        return parent::Hash( $password, $cost );
    }

    protected function HashProc( $password, $cost, $salt )
    {
        return self::PBKDF2( $password, sprintf( '$F$%02d$1$%s', $cost, $salt ), $this->Cost );
    }

    public function Validate( $password, $hash )
    {
        if (empty( $password ))
            throw new \InvalidArgumentException( 'Cannot hash an empty password.' );

        $result = self::PBKDF2( $password, $hash, $this->Cost );        
        return $result !== null && self::CompareTimingSafe( $hash, $result );
    }

    /**
     * Hashes the given password using PBKDF2.
     * Salt takes the form $F$<cost>$<blocks>$<salt> where the cost is a 2 digit
     * cost parameter, blocks is a 1 digit number defining how long the key should
     * be (block * 32) bytes, and the salt is a 22 digit salt using the alphabet
     * ./0-9A-Za-z.
     *
     * @param string $password
     *        The password to hash.
     *
     * @param string $salt
     *        A salt to base the hashing on.
     *
     * @param $repetitions
     * @return string The hashed string, including the original salt.
     */
    private function PBKDF2( $password, $salt, &$repetitions )
    {
        // Check if the given salt is valid or not
        if (!preg_match( '%\$F\$(\d{2,6})\$(\d)\$([a-zA-Z0-9\./]{22})(.*)$%', $salt, $matches ))
            return null;

        $workload = $matches[ 1 ];
        $key_blocks = $matches[ 2 ];
        $salt = $matches[ 3 ];

        unset( $matches );

        if($repetitions <= 31)
            $repetitions = pow( 2, $workload + 4 ); // Increase the workload since PBKDF2 is faster than blowfish.
        $output = '';

        for ($block = 0; $block < $key_blocks; $block++) {
            // Initial hash for this block
            $ib = $b = hash_hmac( 'sha256', $salt . pack( 'N', $block ), $password, true );

            // Perform block iterations
            for ($i = 0; $i < $repetitions; $i++)
                $ib ^= ($b = hash_hmac( 'sha256', $b, $password, true ));

            $output .= $ib;
        }

        // Return the salt + hash        
        return sprintf( '$F$%02d$%s$%s', $workload, $key_blocks, $salt . parent::Base64Encode( $output ) );
    }
}
