<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2014
 * File: PasswordHash.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Security\Cryptography;

use AppStatic\Core\ExceptionBase;
use AppStatic\Core\PropertyBase;

require_once __DIR__ . '/../Security.Config.inc.php';

/**
 * Provides basic utilities for password hashing.
 *
 * @package AppStatic
 * @name PasswordHash
 * @version 1.0
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
abstract class PasswordHash extends PropertyBase
{

    protected $Cost;
    protected $MinCost;
    protected $MaxCost;
    protected $SaltLength;
    protected $Salt;
    protected $HasSecureSalt;

    public function getCost()
    {
        return $this->Cost;
    }

    public function getMinCost()
    {
        return $this->MinCost;
    }

    public function getMaxCost()
    {
        return $this->MaxCost;
    }

    public function getSaltLength()
    {
        return $this->SaltLength;
    }

    public function getSalt()
    {
        return $this->Salt;
    }

    /**
     * Retruns a boolean value indicating if the salt has been generated through
     * high entropy algorithms.
     * 'null' - If the Hash method has not been called yet.
     *
     * @return mixed
     */
    public function getHasSecureSalt()
    {
        return $this->HasSecureSalt;
    }

    protected function __construct($minCost, $maxCost, $rawSaltLength)
    {
        $this->MinCost = $minCost;
        $this->MaxCost = $maxCost;
        $this->SaltLength = $rawSaltLength;
    }

    /**
     * Hashes the given password.
     *
     * @param string $password
     *        The password to hash.
     *
     * @param int $cost
     *        A cost parameter - the base 2 logarithm of the iteration count
     *        for the underlying algorithm.
     *
     * @return string
     *        The hashed string, including the generated salt.
     */
    public function Hash($password, $cost = 0)
    {
        if (empty($password))
            throw new \InvalidArgumentException('Cannot hash an empty password.');

        // Validate the workload is within sensible bounds
        if ($cost < $this->MinCost)
            $cost = $this->MinCost;

        if ($cost > $this->MaxCost)
            $cost = $this->MaxCost;

        $this->Cost = $cost;
        $this->Salt = self::RandomKey($this->SaltLength, $this->HasSecureSalt);
        // Hash the password with a random base64 encoded salt.
        return $this->HashProc($password, $cost, $this->Salt);
    }

    /**
     * @param string $password
     * @param int $cost
     * @param string $salt
     * @return mixed
     */
    protected abstract function HashProc($password, $cost, $salt);

    public function Validate($password, $hash)
    {
        if (empty($password))
            throw new \InvalidArgumentException('Cannot hash an empty password.');

        return self::CompareTimingSafe($hash, crypt($password, $hash));
    }

    /**
     * Fetches random data from a secure source if possible -
     * /dev/urandom on UNIX systems. Falls back to mt_rand()
     * if no better source is available.
     *
     * @param string $length
     *        Number of bytes of random data to generate.
     *
     * @param bool $raw_output
     *        When set to TRUE, the data is returned in raw
     *        binary form, otherwise the returned value is a
     *        ($length * 2)-character hexadecimal number.
     *
     * @param bool $secure
     *          Is set to 'false' when no mcrypt nor
     *          open open ssl php addins where found.
     *
     * @return string
     *        The random data.
     */
    public static function RandomBytes($length, $raw_output = false, &$secure = false)
    {
        if (function_exists('mcrypt_create_iv') && !defined('PHALANGER')) {
            $data = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
            $secure = true;
        } else if (function_exists('openssl_random_pseudo_bytes')) {
            $data = openssl_random_pseudo_bytes($length);
            $secure = true;
        } else {
            $data = '';

            // On a UNIX system use /dev/urandom
            if (is_readable('/dev/urandom')) {
                $handle = @fopen('/dev/urandom', 'rb');
                if ($handle !== false) {
                    $data = fread($handle, $length);
                    fclose($handle);
                }
            }

            // Fall back to using md_rand() - not cryptographically secure, but available everywhere
            while (strlen($data) < $length)
                $data .= pack('i', mt_rand());

            if (strlen($data) > $length)
                $data = substr($data, 0, $length);
        }

        // If requested return the raw output
        if ($raw_output)
            return $data;

        // Otherwise return the data as a hex string
        return bin2hex($data);
    }

    /**
     * Generates a random key using the alphabet ./0-9A-Za-z.
     *
     * @param string $length
     *        Length of the string to generate.
     *
     * @param bool $secure
     * @return string
     *        The generated random string.
     */
    public static function RandomKey($length, &$secure = false)
    {
        $bytes = ceil($length / 1.33);
        $key = self::Base64Encode(self::RandomBytes($bytes, true, $secure));
        return substr($key, 0, $length);
    }

    /**
     * Encodes data in base64 using the alphabet ./0-9A-Za-z.
     *
     * @param string $str
     *        The data to encode.
     *
     * @return string
     *        The encoded data, as a string.
     */
    protected static function Base64Encode($str)
    {
        $from = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        $to = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

        $str = substr(base64_encode($str), 0, -2);
        return strtr($str, $from, $to);
    }

    /**
     * Compares two hashes dictionary attack resistant.
     *
     * To prevent leaking length information, it is important
     * that user input is always used as the second parameter.
     *
     * @param string $safeHash The internal (safe) value to be checked.
     * @param string $userHash The user submitted (unsafe) value.
     * @return boolean 'True' - if the two strings are identical.
     */
    public static function CompareTimingSafe($safeHash, $userHash)
    {
        $safeHash .= chr(0);
        $userHash .= chr(0);

        $safeLen = strlen($safeHash);
        $userLen = strlen($userHash);

        $result = $safeLen - $userLen;

        // Note that we ALWAYS iterate over the user-supplied length
        // This is to prevent leaking length information
        for ($i = 0; $i < $userLen; $i++) {
            // Using % here is a trick to prevent notices
            // It's safe, since if the lengths are different
            // $result is already non-0
            $result |= (ord($safeHash[$i % $safeLen]) ^ ord($userHash[$i]));
        }
        return $result === 0;
    }

}

final class PasswordHashException extends ExceptionBase
{

    function __construct($_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null)
    {
        parent::__construct($_message, $_code, $_innerException);
    }

}