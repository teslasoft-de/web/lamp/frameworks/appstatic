<?php
/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2014
 * File: Password.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Security;

use AppStatic\Collections\ArrayUtility;
use AppStatic\Core\ExceptionBase;
use AppStatic\Core\PropertyBase;
use AppStatic\Security\Cryptography\BCryptPasswordHash;
use AppStatic\Security\Cryptography\MD5PasswordHash;
use AppStatic\Security\Cryptography\PBKDF2PasswordHash;
use AppStatic\Security\Cryptography\SHA256PasswordHash;
use AppStatic\Security\Cryptography\SHA512PasswordHash;

require_once __DIR__ . '/Security.Config.inc.php'; // Necessary for importing security constants

// As from PHP >= 5.5 bcrypt password hashing is enabled by default.
define( 'PASSWORD_UNKNOWN', 0x0 );
if (!defined( 'PASSWORD_BCRYPT' ))
    define( 'PASSWORD_BCRYPT', 0x1 );
define( 'PASSWORD_MD5', 0x2 );
define( 'PASSWORD_PBKDF2', 0x4 );
define( 'PASSWORD_SHA256', 0x8 );
define( 'PASSWORD_SHA512', 0x10 );

/**
 * Provides basic utilities for password hashing and supports >= PHP 5.3.
 *
 * @package   AppStatic
 * @name Password
 * @version   1.0
 * @author    H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class Password extends PropertyBase
{
    private static $SupportedCyptSchemes = [ PASSWORD_BCRYPT, PASSWORD_PBKDF2, PASSWORD_SHA512, PASSWORD_SHA256, PASSWORD_MD5 ];

    /**
     * @return array
     */
    public static function getSupportedCyptSchemes()
    {
        return self::$SupportedCyptSchemes;
    }

    /**
     * @var BCryptPasswordHash
     */
    private $passwordHash;

    /**
     * @var string
     */
    private $String;

    /**
     * @return string
     */
    public function getString() { return $this->String; }

    /**
     * @var string
     */
    private $Hash;

    /**
     * @return string
     */
    public function getHash() { return $this->Hash; }

    /**
     * Initialises the Password object with the specified hashing algorithm and password.
     * If no password was specified it will be randomly generated.
     *
     * @param int    $algorithm The hash algorithm to use (Defined by PASSWORD_* constants).
     * @param string $password  If null the password will be generated with default PasswordHash settings
     * @param int    $length
     *
     * @throws PasswordException
     */
    function __construct( $algorithm = PASSWORD_DEFAULT, $password = null, $length = PASSWORD_LENGTH )
    {
        switch ($algorithm) {
            case PASSWORD_BCRYPT:
                $this->passwordHash = new BCryptPasswordHash();
                break;
            case PASSWORD_PBKDF2:
                $this->passwordHash = new PBKDF2PasswordHash();
                break;
            case PASSWORD_MD5:
                $this->passwordHash = new MD5PasswordHash();
                break;
            case PASSWORD_SHA256:
                $this->passwordHash = new SHA256PasswordHash();
                break;
            case PASSWORD_SHA512:
                $this->passwordHash = new SHA512PasswordHash();
                break;
            default:
                throw new PasswordException( "Unknown hash algorithm '$algorithm'." );
        }
        $this->Hash = self::DetectHashType( $password ) != PASSWORD_UNKNOWN
            ? $password
            : $this->passwordHash->Hash( $this->String = ($password ? $password : self::Generate( $length )) );
    }

    /**
     * @param     $currentPassword string
     * @param     $userPassword    string
     *
     * @param int $algorithm
     *
     * @param int $length
     *
     * @return bool
     */
    public static function Compare( $currentPassword, $userPassword, $algorithm = PASSWORD_DEFAULT, $length = PASSWORD_LENGTH )
    {
        return (new Password( $algorithm, $currentPassword, $length ))->CompareTo( new Password( $algorithm, $userPassword, $length ) );
    }

    /**
     * Generates a random sequence of unmistakable password characters.
     *
     * @param int    $length         The password length
     * @param int    $numericRate    Numeric chracter rate
     * @param int    $letterRate     Lower case character rate
     * @param int    $capitalizeRate Upper case character rate
     * @param string $allowedSymbols Sequence of unmistakable password characters
     *
     * @return string
     */
    public static function RandomChars( $length = PASSWORD_LENGTH, $numericRate = 2, $letterRate = 4, $capitalizeRate = 4,
                                        $allowedSymbols = '2346789abcdefghkmnpqrstuvxyz' )
    {
        $pass = '';
        $array = [];
        $array[0] = [];
        $array[1] = [];
        $array[2] = [];
        $count = 0;

        while ($numericRate-- > 0)
            $array[0][] = ++$count;

        while ($letterRate-- > 0)
            $array[1][] = ++$count;

        while ($capitalizeRate-- > 0)
            $array[2][] = ++$count;

        while (strlen( $pass ) < $length) {

            $randomChar = mt_rand( 1, $count );

            if (ArrayUtility::BinarySearch( $randomChar, $array[0] ))
                $randomChar = chr( mt_rand( 48, 57 ) ); // 0-9

            elseif (ArrayUtility::BinarySearch( $randomChar, $array[1] ))
                $randomChar = chr( mt_rand( 97, 122 ) ); // a-z

            elseif (ArrayUtility::BinarySearch( $randomChar, $array[2] ))
                $randomChar = chr( mt_rand( 65, 90 ) ); // A-Z

            if (strpos( $allowedSymbols, strtolower( $randomChar ) ) === false)
                continue;

            $pass .= $randomChar;
        }

        unset($count);
        unset($randomChar);
        unset($array);

        return $pass;
    }

    public static function Generate( $length = PASSWORD_LENGTH )
    {
        return self::RandomChars( 1, 0, 0, 1 ) . self::RandomChars( $length - 1, 2, 8, 0 );
    }

    public static function DetectHashType( $password )
    {
        $matches = [];
        preg_match( '~^\$((2a|2y|2x)|(F)|(6)|(5)|(1))\$~', $password, $matches );
        if (count( $matches )) {
            $keys = array_slice( $matches, 2 );
            $keys = array_diff( $keys, [ '' ] );
            $key = array_keys($keys)[0];
            return self::$SupportedCyptSchemes[ $key ];
        }

        return PASSWORD_UNKNOWN;
    }

    /**
     * @param Password $password
     *
     * @return bool
     * @throws PasswordException
     */
    public function CompareTo( Password $password )
    {
        if (!$password->String)
            throw new PasswordException( 'Cannot compare an already hashed password.' );

        return $this->passwordHash->Validate( $password->String, $this->Hash );
    }

    public function __toString()
    {
        return $this->Hash;
    }
}

final class PasswordException extends ExceptionBase
{
    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}

?>