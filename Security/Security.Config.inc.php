<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 05.01.2014
 * File: Security.Config.inc.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Security\Cryptography;

define( 'PASSWORD_LENGTH', 8 );

// Verify ~80 hashes per second.
/*define( 'PASSWORD_PBKDF2_ROUNDS', 8 );
define( 'PASSWORD_BCRYPT_ROUNDS', 8 );
define( 'PASSWORD_SHA256_ROUNDS', 20000 );
define( 'PASSWORD_SHA512_ROUNDS', 25000 );*/

// Verify ~40 hashes per second.
/*define( 'PASSWORD_PBKDF2_ROUNDS', 9 );
define( 'PASSWORD_BCRYPT_ROUNDS', 9 );
define( 'PASSWORD_SHA256_ROUNDS', 40000 );
define( 'PASSWORD_SHA512_ROUNDS', 50000 );*/

// Verify ~20 hashes per second.
define( 'PASSWORD_PBKDF2_ROUNDS', 10 );
define( 'PASSWORD_BCRYPT_ROUNDS', 10);
define( 'PASSWORD_SHA256_ROUNDS', 80000 );
#define( 'PASSWORD_SHA512_ROUNDS', 100000 );
define( 'PASSWORD_SHA512_ROUNDS', 30000 );

// Verify ~10 hashes per second.
/*define( 'PASSWORD_PBKDF2_ROUNDS', 11 );
define( 'PASSWORD_BCRYPT_ROUNDS', 11 );
define( 'PASSWORD_SHA256_ROUNDS', 180000 );
define( 'PASSWORD_SHA512_ROUNDS', 200000 );*/

// Verify ~5 hashes per second.
/*define( 'PASSWORD_PBKDF2_ROUNDS', 12 );
define( 'PASSWORD_BCRYPT_ROUNDS', 12 );
define( 'PASSWORD_SHA256_ROUNDS', 340000 );
define( 'PASSWORD_SHA512_ROUNDS', 400000 );*/