<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2014
 * File: AdminLogin.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

/**
 * Description of AdminLogin
 *
 * @author Christian Kusmanow
 */

use AppStatic\Configuration\System;
use AppStatic\Core\ExceptionHandler;
use AppStatic\Web\Session;
use AppStatic\Web\SessionException;

if (System::IsLocal())
    ExceptionHandler::SetSaveTrace( false );
else {
    /** @noinspection PhpUndefinedClassInspection */
    ExceptionHandler::SetSaveTrace( !(Session::getInstance()->IsValid() && (User::getInstance()->GetAccessLevel() > 8)) );
    if (ExceptionHandler::GetSaveTrace()) {
        http_response_code( 403 );
        ob_flush();
        throw new SessionException( 'Denied! Login required.', 403 );
    }
}

