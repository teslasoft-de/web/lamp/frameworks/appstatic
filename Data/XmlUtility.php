<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 25.05.2014
 * File: ArrayUtility.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Data;

use AppStatic\Collections\ArrayUtility;
use AppStatic\Web\HtmlUtility;
use DOMElement;
use DOMNode;
use DOMXPath;

/**
 * Provides essential helper functions for editing xml and xhtml documents.
 *
 * @package   AppStatic
 * @name ArrayUtility
 * @version   1.0
 * @author    H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
final class XmlUtility
{
    /**
     * Returns the inner HTML from the specified DOMNode.
     * Fixes self closing html tags for valid xhtml.
     *
     * @param DOMNode $node
     *
     * @return string
     */
    public static function GetHtmlContent(DOMNode $node/*, DOMXPath $xPath = null*/)
    {
        $innerHTML = "";
        /*$xpath = $xPath ? $xPath : new \DOMXPath( $node->ownerDocument );
        self::IndentChildNodes( $xpath, $node );*/

        foreach ($node->childNodes as $child)
            $innerHTML .= HtmlUtility::ConvertHtmlToXHtml($node->ownerDocument->saveHTML($child));

        return trim($innerHTML);
    }


    /**
     * Inserts the specified HTML content to the specified $domNode.
     *
     * @param DOMNode $domNode The DOMNode
     * @param string  $html    The node content.
     */
    public static function SetHtmlContent(DOMNode $domNode, $html)
    {
        // Convert standalone and symbols to html entities
        // and line breaks to br tag.
        $html = preg_replace('~(\s)&(\s)~', '$1&amp;$2', $html);

        // Text nodes cannot contain document fragments so set value directly and return.
        if ($domNode->nodeName == "#text") {
            $domNode->nodeValue = $html;

            return;
        }

        // Create a document fragment and replace node content.
        $fragment = $domNode->ownerDocument->createDocumentFragment();
        if (!$html) {
            $domNode->nodeValue = null;

            return;
        }
        $fragment->appendXML($html);
        $domNode->nodeValue = null;
        $domNode->appendChild($fragment);
    }

    /**
     * @param DOMNode $element
     * @param string  $name
     * @param string  $value
     * @param bool    $encode
     */
    public static function SetAttribute(DOMNode $element, $name, $value, $encode = false)
    {
        $attribute = $element->attributes->getNamedItem($name);
        if (!$attribute)
            $attribute = $element->appendChild($element->ownerDocument->createAttribute($name));
        $value = strip_tags($value);
        if ($encode)
            self::SetHtmlContent($attribute, $value);
        else
            $attribute->nodeValue = $value;
    }

    public static function HasAttributeValue(DOMNode $element, $name, $value)
    {
        if(!$element->attributes)
            return false;

        $attribute = $element->attributes->getNamedItem($name);

        return $attribute && $value == trim($attribute->nodeValue);
    }

    public static function ContainsAttributeValue(DOMNode $element, $name, $value)
    {
        if(!$element->attributes)
            return false;

        $attribute = $element->attributes->getNamedItem($name);

        return ($attribute && preg_match('~\s*' . $value . '\s*~', $attribute->nodeValue));
    }

    /**
     * @param DOMNode $node
     * @param array   $styles
     */
    public static function SetCSS(DOMNode $node, array $styles)
    {
        // Convert present styles to key value array.
        $presentStyles = [];
        $stylesAttribute = $node->attributes->getNamedItem('style');
        if (!$stylesAttribute)
            $stylesAttribute = $node->appendChild($node->ownerDocument->createAttribute('style'));
        else {
            foreach (explode(';', $stylesAttribute->nodeValue) as $style) {
                if (empty($style))
                    continue;
                $s = explode(':', $style);
                $presentStyles[ trim($s[0]) ] = trim($s[1]);
            }
        }

        // Update present styles with the new styles.
        foreach ($styles as $style => $value)
            $presentStyles[ $style ] = $value;

        // Convert styles to string
        $styles = [];
        foreach ($presentStyles as $style => $value)
            $styles [] = "$style: $value";

        $stylesAttribute->nodeValue = implode('; ', $styles);
    }

    /**
     * Sets or removes the specified class name from the DOMNode.
     * For removal of multiple classes class name can contain a regex pattern.
     *
     * @param DOMElement|DOMNode $node
     * @param string             $className
     * @param boolean            $condition
     */
    public static function SetClassAttribute(DOMNode $node, $className, $condition)
    {
        // Get class attribute
        $classAttribute = $node->attributes->getNamedItem('class');
        // Remove class attribute if not active and no other classes are present.
        if (!$condition && $classAttribute && $classAttribute->nodeValue == '')
            $node->removeChild($classAttribute);
        elseif ($condition || $classAttribute) {
            // Add missing class attribute.
            if (!$classAttribute)
                $classAttribute = $node->appendChild($node->ownerDocument->createAttribute('class'));

            $active = $condition ? $className : false;
            // Fix spaces and explode attribute value if not empty.
            $classes = $classAttribute->nodeValue ? explode(' ', preg_replace('~\s+~', ' ', $classAttribute->nodeValue)) : [];

            // If class is inactive find matching class names and remove them.
            if (!$active) {
                $removeClasses = [];
                foreach ($classes as $class) {
                    if (preg_match("~$className~i", $class))
                        $removeClasses [] = $class;
                }
                foreach ($removeClasses as $class) {
                    $index = array_search($class, $classes);
                    if ($index !== false)
                        unset($classes[ $index ]);
                }
            }

            // Get class name index
            $classIndex = ArrayUtility::IndexOf($className, $classes);

            // Remove class if not active
            if (!$active && $classIndex > -1)
                unset($classes[ $classIndex ]);
            // or add class if active and not present.
            elseif ($active && $classIndex == -1)
                $classes [] = $active;

            // Remove class attribute if empty
            if (count($classes) == 0)
                $node->removeAttribute('class');
            // or highlight active menu link.
            else
                $classAttribute->nodeValue = implode(' ', $classes);
        }
    }

    public static function IndentChildNodes(DOMXPath $xPath, DOMNode $domNode)
    {
        // Retrieve all text nodes using XPath
        $nodeList = $xPath->query("//text()");
        foreach ($nodeList as $node) {
            // 1. "Trim" each text node by removing its leading and trailing spaces and newlines.
            /*$node->nodeValue = preg_replace( "/^[\s\r\n]+/", "", $node->nodeValue );
            $node->nodeValue = preg_replace( "/[\s\r\n]+$/", "", $node->nodeValue );*/
            $node->nodeValue = preg_replace("/[\x20]{4}+/", "\t", $node->nodeValue);
            $node->nodeValue = preg_replace("/^[\t\r\n]+/", "$1", $node->nodeValue);
            $node->nodeValue = preg_replace("/[\t\r\n]+$/", "$1", $node->nodeValue);
            // 2. Resulting text node may have become "empty" (zero length nodeValue) after trim. If so, remove it from the dom.
            if (strlen($node->nodeValue) == 0 || $node->nodeValue == ' ')
                $node->parentNode->removeChild($node);
        }
        // 3. Starting from root (documentElement), recursively indent each node. 
        self::_indentChildNodes($domNode, 0);
    }

    static $InlineIndentExcludes = [
        'abbr', 'dfn', 'q',
        'cite', 'code', 'kbd', 'samp', 'var',
        'span', 'font', 'i', 'em', 'b', 'strong', 'small', 'sub', 'sup', 'bdo',
        'a', 'map', 'img', 'object', 'button', 'input', 'label', 'select', 'textarea',
        'br'];

    private static function _indentChildNodes(DOMNode $currentNode, $depth)
    {
        $indentCurrent = true;

        if (($currentNode->nodeType == XML_TEXT_NODE &&
                ($currentNode->parentNode->childNodes->length == 1 ||
                    in_array($currentNode->parentNode->nodeName, self::$InlineIndentExcludes) /*||
                preg_match( '~^[,\.:;?!\(\)]~', $currentNode->nodeValue )*/)
            ) ||
            in_array($currentNode->nodeName, self::$InlineIndentExcludes) ||
            ($currentNode->previousSibling && in_array($currentNode->previousSibling->nodeName, self::$InlineIndentExcludes))
        ) {
            // A text node being the unique child of its parent will not be indented.
            // In this special case, we must tell the parent node not to indent its closing tag.
            $depth -= 1;
            $indentCurrent = false;
        }
        if ($indentCurrent && $depth > 0) {
            // Indenting a node consists of inserting before it a new text node
            // containing a newline followed by a number of tabs corresponding
            // to the node depth.
            $textNode = $currentNode->ownerDocument->createTextNode("\n" . str_repeat("\t", $depth));
            $currentNode->parentNode->insertBefore($textNode, $currentNode);
        }
        if ($currentNode->childNodes) {
            $indentClosingTag = false;
            foreach ($currentNode->childNodes as $childNode)
                $indentClosingTag = self::_indentChildNodes($childNode, $depth + 1);
            if ($indentClosingTag) {
                // If children have been indented, then the closing tag
                // of the current node must also be indented.
                $textNode = $currentNode->ownerDocument->createTextNode("\n" . str_repeat("\t", $depth));
                $currentNode->appendChild($textNode);
            }
        }

        return $indentCurrent;
    }
}
