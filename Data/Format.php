<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: Format.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Data;


class Format {

    /**
     * Formats the specified bytes in Bytes, KB, MB, GB or TB
     *
     * @param integer $bytes
     * @return string
     */
    public static function FileSize($bytes)
    {
        $types = Array('Bytes', 'KB', 'MB', 'GB', 'TB');
        $current = 0;
        $neg = $bytes < 0;
        if ($neg)
            $bytes *= -1;
        while ($bytes > 1024) {
            $current++;
            $bytes /= 1024;
        }
        if ($neg)
            $bytes *= -1;

        if ($current == 1)
            $result = round($bytes, 0) . ' ' . $types[$current];
        else
            $result = sprintf('%.2f %s', round($bytes, 2), $types[$current]);

        return $result;
    }

    public static function HexToByte($hex)
    {
        $bytes = array();
        $hexCount = strlen($hex);

        for ($b = 0, $h = 0; $h < $hexCount; $b++, $h += 2)
            $bytes[$b] = hexdec($hex[$h] . $hex[$h + 1]);
        return $bytes;
    }

    public static function HexToBinary($hex)
    {
        $newString = '';
        $hexCount = strlen($hex);

        for ($b = 0, $h = 0; $h < $hexCount; $b++, $h += 2)
            $newString .= unichr(hexdec($hex[$h] . $hex[$h + 1]));

        return $newString;
    }

    public static function BytesToBinary($bytes)
    {
        $newString = '';
        $byteCount = count($bytes);
        for ($n = 0; $n < $byteCount; $n++) {
            $newString .= chr($bytes[$n]);
        }
        return $newString;
    }
}