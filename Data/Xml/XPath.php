<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 19.05.2014
 * File: XPath.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Data\Xml;

use DOMNode;
use DOMXPath;
use DOMDocument;

/**
 * Provides functions for generating XPath query strings or performing queries on a DOMNode.
 *
 * @package   AppStatic
 * @name XPath
 * @version   0.9
 * @author    H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
final class XPath
{

    /**
     *
     * @param string $value
     *
     * @return string
     */
    public static function SaveXPathString( $value )
    {
        $quote = "'";
        if (FALSE === strpos( $value, $quote ))
            return $quote . $value . $quote;
        else
            return sprintf( "concat('%s')", implode( "', \"'\", '", explode( $quote, $value ) ) );
    }

    /**
     * Returns a DOMNodeList from a relative xPath.
     *
     * @param DOMNode $node
     * @param string  $xPath
     *
     * @return \DOMNodeList
     */
    public static function SelectNodes( DOMNode $node, $xPath )
    {
        $pos = strpos( self::GetFullXpath( $node ), '/', 1 );
        $xPathQuery = substr( self::GetFullXpath( $node ), $pos ); //to paste  /#document[1]/
        $xPathQueryFull = $xPathQuery . $xPath;
        $domXPath = new DOMXPath( $node->ownerDocument );

        return $domXPath->query( $xPathQueryFull );
    }

    /**
     * Returns a DOMNode from a xPath from other DOMNode.
     *
     * @param DOMNode $node
     * @param string  $xPath
     *
     * @return \DOMNodeList
     */
    public static function SelectSingleNode( DOMNode $node, $xPath )
    {
        $pos = strpos( self::GetFullXpath( $node ), '/', 1 );
        $xPathQuery = substr( self::GetFullXpath( $node ), $pos ); //to paste  /#document[1]/
        $xPathQueryFull = $xPathQuery . $xPath;
        $domXPath = new DOMXPath( $node->ownerDocument );

        return $domXPath->query( $xPathQueryFull )->item( 0 );
    }

    /**
     *
     * @param DOMDocument $document
     * @param string      $className
     * @param string      $xPath The element type xpath.
     *
     * @return \DOMNodeList
     */
    public static function SelectNodesContainsClass( DOMDocument $document, $className, $xPath = '//*' )
    {
        $domXPath = new DOMXPath( $document );
        // XPath 2.0
        // return $xpath->query( "//*[count( index-of( tokenize( @class, '\s+' ), '$classname' ) ) = 1]" );
        // XPath 1.0
        return $domXPath->query( $xPath . self::ContainsAttributeValue( 'class', $className ) );
    }

    /**
     * @param DOMNode $contextNode
     * @param         $value
     * @param string  $nodeSelector
     * @param array   $identifiers
     *
     * @return \DOMNodeList
     */
    public static function SelectNodesContainsAttributeIdentifier( DOMNode $contextNode, $value, $nodeSelector = '*', array $identifiers = ['id', 'role', 'class'] )
    {
        $xpath = new \DOMXPath( $contextNode->ownerDocument );
        foreach ($identifiers as $identifier) {
            $node = $xpath->query( $nodeSelector . XPath::ContainsAttributeValue( $identifier, $value ), $contextNode );
            if ($node->length > 0)
                break;
        }

        return $node;
    }

    /**
     * Generates a XPath query to determine if the specified attribute value contains the specified value.
     *
     * @param string $attribute
     * @param string $value
     *
     * @return string
     */
    public static function ContainsAttributeValue( $attribute, $value )
    {
        return <<<EOT
[contains(normalize-space(@$attribute), " $value ")
or substring(normalize-space(@$attribute), 1, string-length("$value") + 1) = "$value "
or substring(normalize-space(@$attribute), string-length(@$attribute) - string-length("$value")) = " $value"
or @$attribute = "$value"]
EOT;
    }

    /**
     * Returns the node position.
     *
     * @param DOMNode $pNode
     * @param string  $nodeName
     *
     * @return int
     */
    private static function GetNodePos( $pNode, $nodeName )
    {
        if ($pNode == null || !($pNode instanceof DOMNode)) {
            return 0;
        } else {
            $var = 0;
            if ($pNode->previousSibling != null) {
                if ($pNode->previousSibling->nodeName == $nodeName) {
                    $var = 1;
                }
            }

            return self::GetNodePos( $pNode->previousSibling, $nodeName ) + $var;
        }
    }

    /**
     *
     * @param DOMNode $pNode
     *
     * @return string
     */
    private static function GetFullXpath( $pNode )
    {
        if ($pNode == null) {
            return '';
        } else {

            return self::GetFullXpath( $pNode->parentNode ) . "/" . $pNode->nodeName . "[" . strval( self::GetNodePos( $pNode, $pNode->nodeName ) + 1 ) . "]"; //+1 to get the real xPath index
        }
    }

}
