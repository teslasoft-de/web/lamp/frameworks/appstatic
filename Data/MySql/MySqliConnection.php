<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2014
 * File: MySqliConnection.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Data\MySql;
use AppStatic\Core\ExceptionBase;
use AppStatic\Core\PropertyBase;
use Exception;
use mysqli;
use mysqli_stmt;

/**
 * @package AppStatic
 * @name MySQLiConnection
 * @version 1.1 (PHP 5.4)
 *
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow
 *
 */
final class MySqliConnection extends PropertyBase
{
    protected $MySQLi;
    protected $DBName;

    /**
     * Returnes the mysqli object.
     *
     * @return mysqli
     */
    public function getMySQLi()
    {
        return $this->MySQLi;
    }

    /**
     * @return string
     */
    public function getDBName()
    {
        return $this->DBName;
    }

    /**
     * The constuctor initializes the MySQLi connection object.
     *
     * @param string $ip
     * @param string $user
     * @param string $password
     * @param string $dbName
     * @throws MySqliConnectionException
     */
    private function __construct($ip, $user, $password, $dbName)
    {
        $this->MySQLi = mysqli_init();
        $this->MySQLi->real_connect($ip, $user, $password, $dbName);
        $this->MySQLi->set_charset('utf8');

        //$this->MySQLi = new mysqli( $ip, $user, $password, $dbName );
        if (mysqli_connect_errno())
            throw new MySqliConnectionException("Connect failed: " . mysqli_connect_error() . "\n");
        $this->DBName = $dbName;
    }

    /**
     * Disposes the MySQLi connection object.
     */
    function __destruct()
    {
        $this->Close();
    } // __destruct


    public function SelectDB($dbName)
    {
        if (!$this->MySQLi->select_db($dbName))
            throw new MySqliConnectionException("Database select failed: " . $this->GetError() . "\n");
        $this->DBName = $dbName;
    }

    public function GetError($stmt = null)
    {
        $message = "";
        if ($this->MySQLi->error)
            $message = $this->MySQLi->error;
        elseif ($stmt instanceof mysqli_stmt && mysqli_stmt_errno($stmt))
            $message = mysqli_stmt_error($stmt);
        else {
            $array = error_get_last();
            if (isset($array['message']))
                $message = $array['message'];
        }
        return $message;
    }

    function Close()
    {
        $MySQLi = $this->MySQLi;
        /* @var $mysqli mysqli */
        if ($MySQLi !== null)
            $MySQLi->close();
    }

    private static $instance;

    /**
     * Retuns a static MySqliConnection object.
     * @return MySqliConnection
     * @throws MySqliConnectionException
     */
    public static function GetInstance()
    {
        if (!self::$instance)
            throw new MySqliConnectionException("MySQLi connection not initialized.");
        return self::$instance;
    }

    public static function SetInstance($ip, $user = null, $password = null, $dbName = null)
    {
        self::$instance = $ip instanceof MySqliConnection ? $ip : new MySqliConnection($ip, $user, $password, $dbName);
    }
}

final class MySqliConnectionException extends ExceptionBase
{
    function __construct( $_message = null, $_code = E_USER_ERROR, Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}