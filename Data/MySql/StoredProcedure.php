<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: StoredProcedure.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Data\MySql;
use AppStatic\Core\ExceptionBase;
use AppStatic\Core\PropertyBase;
use Exception;
use mysqli;
use mysqli_stmt;

/**
 * @package AppStatic
 * @name StoredProcedure
 * @version 1.0 (2009-01-01)
 *
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow
 *
 */
class StoredProcedure extends PropertyBase
{
    public function getMySQLiConnection()
    {
        return $this->mySQLiConnection;
    }

    private $mySQLiConnection;

    /**
     * Enter description here...
     *
     * @return mysqli
     */
    public function getMySQLi()
    {
        return $this->mySQLi;
    }

    /* @var $mySQLi mysqli */
    private $mySQLi;

    /**
     * @return mysqli_stmt
     */
    public function getStmt()
    {
        return $this->stmt;
    }

    /* @var $stmt mysqli_stmt */
    private $stmt;
    private $bindParam;
    private $bindResult;
    private $prepare;

    /**
     * Enter description here...
     *
     * @return string
     */
    public function getName()
    {
        return $this->Name;
    }

    protected $Name;

    protected $LastCharset;
    protected $LastDataBase;

    private $prepared;

    public function getFetched()
    {
        return $this->hasFetched;
    }

    private $fetched;
    private $hasFetched;

    private $bindResultError;

    public function getBindResultError()
    {
        return $this->bindResultError;
    }

    /**
     * Initializes a new instance of the StoredProcedure class.
     *
     * @param string $name
     * @param null $dataBase
     * @param null $charset
     * @throws MySqliConnectionException
     */
    public function __construct($name, $dataBase = null, $charset = null)
    {
        $this->mySQLiConnection = MySqliConnection::GetInstance();
        $this->mySQLi = $this->mySQLiConnection->getMySQLi();
        $this->Name = $name;

        if ($charset != null) {
            $this->LastCharset = $this->mySQLi->get_charset()->charset;
            $this->mySQLi->set_charset($charset);
        }

        if ($dataBase != null && $dataBase != $this->mySQLiConnection->getDBName()) {
            $this->LastDataBase = $this->mySQLiConnection->getDBName();
            $this->mySQLiConnection->SelectDB($dataBase);
        }
        $this->bindResultError = false;
        $this->prepared = false;
        $this->fetched = false;

        // >= PHP 5.3
        if(version_compare( PHP_VERSION, '5.3' ) >= 0) {
            $this->bindParam = new \ReflectionMethod('mysqli_stmt', 'bind_param');
            $this->bindResult = new \ReflectionMethod('mysqli_stmt', 'bind_result');
            $this->prepare = new \ReflectionMethod($this, 'Prepare');
        }
    }

    public function __destruct()
    {
        $this->Close();
        if ($this->LastCharset != null && $this->LastCharset != $this->mySQLi->get_charset())
            $this->mySQLi->set_charset($this->LastCharset);

        if (($this->LastDataBase != null && $this->LastDataBase != $this->mySQLiConnection->getDBName()) && !$this->bindResultError)
            $this->mySQLiConnection->SelectDB($this->LastDataBase);
    }

    /**
     * Enter description here...
     *
     */
    public function Prepare()
    {
        if ($this->stmt != null)
            $this->Close();

        $this->bindResultError = false;
        $this->prepared = false;
        $this->fetched = false;
        $this->stmt = $this->mySQLi->stmt_init();

        $arguments = func_get_args();

        $parameters = implode(',', array_fill(0, func_num_args(), '?'));

        if (!$this->stmt->prepare("Call $this->Name($parameters)"))
            throw new StoredProcedureException("Invalid query: " . $this->mySQLi->error, $this->mySQLi->errno);

        $this->prepared = true;

        if ($parameters == "")
            return;

        $parameters = "";
        $blobs = array();

        foreach ($arguments as $arg) {
            if ((is_integer($arg) && $arg < 2147483648) || is_bool($arg) || (!is_array($arg) && $arg == null))
                $parameters .= 'i';
            elseif (is_double($arg))
                $parameters .= 'd';
            elseif (is_string($arg) || (is_integer($arg) && $arg > 2147483647))
                $parameters .= 's';
            elseif (is_array($arg)) {
                $index = strlen($parameters);
                $blobs[$index] = $arguments[$index];
                $parameters .= "b";
                unset($index);
            } else
                throw new StoredProcedureException("Not supported paramter type.", E_USER_ERROR);
        }
        $args = array();
        $args [] = &$parameters;

        foreach ($arguments as &$arg)
            $args [] = &$arg;

        if(!$this->bindParam)
            $result = call_user_func_array( array(&$this->stmt, "bind_param"), $args);
        else{
            try{
                $result = $this->bindParam->invokeArgs( $this->stmt, $args );
            }
            catch(Exception $ex){
                throw new StoredProcedureException( "Invalid parameter: " . $this->mySQLi->error, $this->mySQLi->errno, $ex );
            }
        }
        if (!$result)
            throw new StoredProcedureException("Invalid parameter: " . $this->mySQLi->error, $this->mySQLi->errno);

        if (count($blobs) > 0) {
            $sequence = null;
            foreach ($blobs as $key => $value) {
                if (count($value) == 0)
                    continue;
                $sequence = '';
                foreach ($value as $byte)
                    $sequence .= chr($byte);

                $this->stmt->send_long_data($key, $sequence);
            }
            unset($sequence);
        }

        unset($args);
    }

    public function _Prepare($args)
    {
        if (!$this->prepare)
            $result = call_user_func_array(array(&$this, "Prepare"), $args);
        else {
            // >= PHP 5.3
            try {
                $result = $this->prepare->invokeArgs($this, $args);
            } catch (Exception $ex) {
                throw new StoredProcedureException("Unable to invoke Prepare on " . $this::getClassName(), E_USER_ERROR, $ex);
            }
        }
    }

    public function Execute()
    {
        if (!$this->stmt->execute())
            throw new StoredProcedureException("Query failed: " . $this->mySQLi->error, $this->mySQLi->errno);
        return $this->stmt->store_result();
    }

    /**
     * Enter description here...
     *
     * @param array $args
     * @throws StoredProcedureException
     */
    public function BindResult(array $args)
    {
        if (count($args) == 0)
            throw new StoredProcedureException("Invalid result parameter count. Minimum one parameter needed.");

        $hack = array();
        foreach ($args as &$v) {
            $hack[] = &$v;
        }
        if (!$this->bindResult)
            $result = call_user_func_array([&$this->stmt, "bind_result"], $args);
        else {
            // >= PHP 5.3
            try {
                $result = $this->bindResult->invokeArgs($this->stmt, $args);
            } catch (Exception $ex) {
                throw new StoredProcedureException("Invalid parameter: " . $this->mySQLi->error, $this->mySQLi->errno, $ex);
            }
        }
        //$stmt = $this->stmt;/* @var $stmt mysqli_stmt*/		 
        if (!$result) {
            if ($this->mySQLi->error) {
                $message = $this->mySQLi->error;
                $errno = $this->mySQLi->errno;
            } else {
                $errno = mysqli_stmt_errno($this->stmt);
                if (!empty($errno))
                    $message = mysqli_stmt_error($this->stmt);
                else {
                    $array = error_get_last();
                    $message = $array['message'];
                    $errno = $array['type'];
                }
            }
            $this->bindResultError = true;
            throw new StoredProcedureException("Error: $message", $errno);
        }

        unset($hack);
        unset($args);
    }

    /**
     * Enter description here...
     *
     * @param bool $allowEmptyResult
     * @return bool
     * @throws StoredProcedureEmptyResultException
     */
    public function Fetch($allowEmptyResult = false)
    {
        $hasFetched = $this->stmt->fetch();
        if (!$allowEmptyResult && !$hasFetched && !$this->fetched)
            throw new StoredProcedureEmptyResultException("Result was empty.");
        $this->fetched = true;
        $this->hasFetched = (boolean)$hasFetched;
        return $this->hasFetched;
    }

    /**
     * Free all cached results and close the underlying stored procedure statement object.
     *
     */
    public function Close()
    {
        if ($this->stmt != null) {
            // Cleanup existing result sets.
            if ($this->fetched) {
                while ($this->mySQLi->next_result()) // flush multi_queries
                    if (!$this->mySQLi->more_results()) break;
                $this->stmt->free_result();
            }
            if ($this->prepared)
                $this->stmt->close();
            $this->stmt = null;
        }
    }
}

class StoredProcedureException extends ExceptionBase
{
    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}

final class StoredProcedureEmptyResultException extends StoredProcedureException
{
    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}