<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 17.01.2014
 * File: PdoConnection.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic;
use AppStatic\Core\PropertyBase;

/**
 * Description of PdoConnection
 * 
 * @package AppStatic
 * @name PdoConnection
 * @version 0.1
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class PdoConnection extends PropertyBase
{
    //put your code here
}
