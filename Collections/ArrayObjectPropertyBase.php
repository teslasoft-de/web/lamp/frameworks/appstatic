<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 26.06.2014
 * File: ArrayObjectPropertyBase.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Collections;

use AppStatic\Core\ExceptionBase;
use ArrayObject;
use BadMethodCallException;
use Exception;

/**
 * ArrayObject with virtual property support where object properties will be preferred
 * and all unknown property strings are threaded as inner array object keys.
 * 
 * @package AppStatic
 * @name ArrayAccessPropertyBase
 * @version 1.0
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
abstract class ArrayObjectPropertyBase extends \ArrayObject
{

    /**
     * Magic function to intercept read calls to unknown properties.
     * It will return the property value if the member variable is publicly
     * accessible and property implementations are present.
     * Otherwise the property name will be used as offset to return a value
     * from the inner list (storage).
     * 
     * @param string $propertyName
     * @return mixed
     * @throws ArrayObjectPropertyBaseException
     */
    final public function __get( $propertyName )
    {
        $class = get_class( $this );
        $isKey = parent::offsetExists( $propertyName );
        $ex = null;

        // validate if property exists
        if (!$isKey && !property_exists( $class, $propertyName ))
            $ex = "Member $class::$propertyName does not exist.";

        // validate if property is readable
        if (!$isKey && !$ex && $propertyName[ 0 ] == strtolower( $propertyName[ 0 ] ))
            $ex = "Member $class::$propertyName is not publicly readable. Use pascal casing to make it accessible.";

        // use custom get method if available.
        if (!$isKey && !$ex &&
                method_exists( $class, ($method = "get" . $propertyName ) ) ||
                method_exists( $class, ($method = "Get" . $propertyName ) ))
            return $this->$method();

        // if property exists, return value
        if (!$isKey && !$ex)
            return $this->$propertyName;
        
        // throw exception if array key not found
        if (!$isKey){
            // Intercept unknown property read calls by derived classes.
            $value = $this->OnPropertyOffsetGet( $propertyName );
            if($value)
                return $value;
            throw new ArrayObjectPropertyBaseException( $ex );
        }
        
        // return array value
        return parent::offsetGet( $propertyName );
    }

    /**
     * Virtual callback function which is called when an offset is unknown.
     * @param string $offset
     * @return mixed
     */

    protected function OnPropertyOffsetGet( /** @noinspection PhpUnusedParameterInspection */
        $offset )
    {
        return false;
    }

    /**
     * Magic function to intercept write calls to properties. If no property found
     * the name will be used as offset to a value of the inner list (storage).
     * 
     * @param string $propertyName
     * @param mixed $val
     */
    final public function __set( $propertyName, $val )
    {
        $class = get_class( $this );
        $isProp = true;

        // validate if property exists
        if (!property_exists( $class, $propertyName ))
            $isProp = false;

        // Cancel if the member variable lower case, which means private.
        if ($isProp && $propertyName[ 0 ] == strtolower( $propertyName[ 0 ] ))
            $isProp = false;

        if ($isProp &&
                !method_exists( $class, ($method = "set" . $propertyName ) ) &
                !method_exists( $class, ($method = "Set" . $propertyName ) ))
            $isProp = false;

        if ($isProp)
            /** @noinspection PhpUndefinedVariableInspection */
            $this->$method( $val );
        else{
            // Intercept property write calls by derived classes.
            if($this->OnPropertyOffsetSet( $propertyName, $val ))
                return;
            parent::offsetSet( $propertyName, $val );
        }
    }
    
    /**
     * Virtual callback function which is called when the specified offset receives a value.
     * @param string $offset
     * @param mixed $value
     * @return boolean
     */
    protected function OnPropertyOffsetSet( /** @noinspection PhpUnusedParameterInspection */
        $offset, $value )
    {
        return false;
    }

    /**
     * Because this ArrayObject can't be handled like normal arrays,
     * it will be extended with the php array functions.
     * 
     * @param callback|callable $func
     * @param array $argv
     * @return mixed
     * @throws BadMethodCallException
     * @example / $myArrayObject->array_keys();
     */
    public function __call( $func, $argv )
    {
        if (!is_callable( $func ) || substr( $func, 0, 6 ) !== 'array_')
            throw new BadMethodCallException( __CLASS__ . '->' . $func );

        return call_user_func_array( $func, array_merge( array( $this->getArrayCopy() ), $argv ) );
    }

    /**
     * Returns a String that represents the current Object.
     *
     * @return string
     */
    public function __toString()
    {
        return get_class( $this );
    }

    /**
     * Serves as a hash function for a particular type.
     * 
     * @return string
     */
    public function getHashCode()
    {
        return spl_object_hash( $this );
    }

    /**
     * Returns the class name of the object.
     *
     * @return string
     */
    public static function getClassName()
    {
        return get_called_class();
    }

    public function ToArray()
    {
        // TODO: Implement ArrayObjectPropertyBase conversion to array.
    }

    /**
     * Converts the current object to xml.
     *
     * @return string
     */
    public function ToXml()
    {
        // TODO: Implement ArrayObjectPropertyBase conversion to xml.
    }
}

final class ArrayObjectPropertyBaseException extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }

}
