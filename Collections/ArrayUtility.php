<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.01.2009
 * File: ArrayUtility.php
 * Encoding: UTF-8
 * Project: AppStatic
 * */

namespace AppStatic\Collections;


final class ArrayUtility {

    /**
     * Returns a numeric index greater than -1 if the value was found in the array.
     * @param $value
     * @param $array
     * @return int|mixed
     */
    public static function IndexOf($value, $array)
    {
        return ($index = array_search($value, $array)) !== false ? $index : -1;
    }

    /**
     * Iterative (as opposed to Recursive) Binary Search can only be used in situations when the haystack is in ascending order.
     * @param mixed $elem
     * @param array $array
     * @return bool
     */
    public static function BinarySearch($elem, $array)
    {
        $top = sizeof($array) - 1;
        $bot = 0;

        while ($top >= $bot) {
            $p = (int)floor(($top + $bot) / 2);

            if ($array[$p] < $elem)
                $bot = $p + 1;
            elseif ($array[$p] > $elem)
                $top = $p - 1;
            else
                return TRUE;
        }

        return FALSE;
    }
}