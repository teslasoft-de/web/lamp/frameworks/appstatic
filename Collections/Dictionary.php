<?php

namespace AppStatic\Collections;
use AppStatic\Core\ExceptionBase;
use AppStatic\Core\PropertyBase;

/**
 * @package AppStatic
 * @name Dictionary
 * @version 1.0 (2009-01-01)
 * 
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow
 *
 */
class Dictionary extends PropertyBase implements \ArrayAccess, \Countable, \IteratorAggregate
{
    private $keys;
    protected $KeysCount;
    private $values;
    protected $ValuesCount;

    #######################################################################################
    /* .ctor */
    #######################################################################################
    

    /**
     * Initializes a new instance of this class.
     *
     * @param array $valuesArray
     * @param string $keyName
     */
    public function __construct( $valuesArray = null, $keyName = null )
    {
        $this->keys = array();
        $this->KeysCount = 0;
        $this->values = array();        
        $this->ValuesCount = 0;
        
        if(!empty($valuesArray))
            $this->AddObjectsWithPropertyKey( $valuesArray, $keyName );
    }

    #######################################################################################
    /* ArrayAccess Interface Members */
    #######################################################################################
    

    /**
     * Sets the value at the specified offset.
     *
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet( $offset, $value )
    {
        $this->Add( $offset, $value );
    }

    /**
     * Gets a value indicating if the specified offset exists.
     *
     * @param mixed $offset
     * @return boolean
     */
    public function offsetExists( $offset )
    {
        return $this->ContainsKey( $offset );
    }

    /**
     * Gets the value at the specified offset.
     *
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet( $offset )
    {
        return $this->GetValue( $offset );
    }

    /**
     * Unsets the value at the specified offset.
     *
     * @param mixed $offset
     */
    public function offsetUnset( $offset )
    {
        
        if (!$this->offsetExists( $offset ))
            return;
        
        unset( $this->keys[ArrayUtility::IndexOf($offset, $this->keys)] );
        --$this->KeysCount;
        
        if (is_object( $offset ))
            $offset = spl_object_hash( $offset );
            
        // Decrement values count if objects were added by property key
        if (is_array( $this->values[$offset] )) {
            $realValuesCount = count( $this->values );
            if ($this->ValuesCount > $realValuesCount)
                $this->ValuesCount -= count( $this->values[$offset] );
            else
                --$this->ValuesCount;
        }else
            --$this->ValuesCount;
        
        unset( $this->values[$offset] );
    }

    #######################################################################################
    /* Countable Interface Members */
    #######################################################################################
    

    /**
     * Retunes the values count.
     *
     * @return integer
     */
    public function count()
    {
        return $this->ValuesCount;
    }

    #######################################################################################
    /* IteratorAggregate Interface Members */
    #######################################################################################
    

    /**
     * Gets the DictionaryIterator for this instance.
     *
     * @return DictionaryEnumerator
     */
    public function getIterator()
    {
        return new DictionaryEnumerator( $this );
    }

    #######################################################################################
    /* Dictionary Implementation */
    #######################################################################################


    /**
     * Adds the value with the specified key.
     *
     * @param mixed $key
     * @param mixed $value
     * @param boolean $byRef
     * @throws DictionaryException
     */
    public function Add( $key, &$value, $byRef = false )
    {
        if (is_null( $key ))
            throw new DictionaryException( "Key can not be null" );
        
        if (is_null( $value ))
            throw new DictionaryException( "Value can not be null" );
        
        if (!in_array( $key, $this->keys ))
            $this->keys[] = $key;
        
        if (is_object( $key ))
            $key = spl_object_hash( $key );
        
        if ($byRef)
            $this->values[$key] = & $value;
        else
            $this->values[$key] = $value;
        
        $this->KeysCount = count( $this->keys );
        $this->ValuesCount = count( $this->values );
    }

    /**
     * Returns a value which indicates if the specified key exists.
     *
     * @param mixed $key
     * @return boolean
     */
    public function ContainsKey( $key )
    {
        return in_array( $key, $this->keys );
    }

    /**
     * Returns an array which contains all collection keys.
     *
     * @return array
     */
    public function Keys()
    {
        return $this->keys;
    }
    
    /**
     * Returns an array which contains all collection values.
     *
     * @return array
     */
    public function Values()
    {
        return $this->values;
    }

    /**
     * Gets the value of the specified element in the current Dictionary.
     *
     *
     * @param mixed $key
     * @param bool $byRef
     * @return mixed
     * @throws DictionaryException
     */
    public function GetValue( $key, $byRef = false )
    {
        if (!$this->ContainsKey( $key ))
            throw new DictionaryException( "The Key '$key' was not present in the collection" );
        
        if (is_object( $key ))
            $key = spl_object_hash( $key );
        
        if (!$byRef)
            $value = $this->values[$key];
        else {
            $value = & $this->values[$key];
            $this->values[$key] = & $value;
        }
        return $value;
    }

    /**
     * Gets the value associated with the specified key.
     *
     * @param mixed $key
     * @param mixed $value
     * @param bool $byRef
     * @return bool
     * @throws DictionaryException
     */
    public function TryGetValue( $key, &$value, $byRef = false )
    {
        if (!$this->ContainsKey( $key ))
            return false;
        
        $value = $this->GetValue( $key, $byRef );
        
        if ($byRef) {
            if (is_object( $key ))
                $key = spl_object_hash( $key );
            $this->values[$key] = & $value;
        }
        return true;
    }

    #######################################################################################
    /* Public Methods */
    #######################################################################################


    /**
     * Adds the objects of the specified object array to the dictionary using the specified propertyname as key.
     *
     * @param array $objectArray
     * @param string $propertyName
     * @throws DictionaryException
     */
    public function AddObjectsWithPropertyKey( array $objectArray, $propertyName )
    {
        if (!is_string( $propertyName ))
            throw new DictionaryException( "Property name must be type of string" );
        
        $keys = & $this->keys;
        $values = & $this->values;
        $keysCount = & $this->KeysCount;
        $valuesCount = & $this->ValuesCount;
        
        foreach( $objectArray as $value ) {
            
            $key = $value->$propertyName;
            if (!in_array( $key, $keys )) {
                $keys[] = $key;
                
                $objectArray = array();
                $values[$key] = & $objectArray;
                $keysCount++;
            }else
                $objectArray = & $values[$key];
            
            $objectArray[] = $value;
            unset( $objectArray );
            $valuesCount++;
        }
        
        unset( $keys );
        unset( $values );
        unset( $keysCount );
        unset( $valuesCount );
        
    /*array_unshift($keys, $this->keys);
        $this->keys =& $keys[0];                
        call_user_func_array('array_push', $keys);        
        
        foreach ($values as $Key => $value)
            $this->values[$Key] = $value;*/
    }
}

final class DictionaryException extends ExceptionBase
{

    function __construct( $_message = null, $_code = E_USER_ERROR, \Exception $_innerException = null )
    {
        parent::__construct( $_message, $_code, $_innerException );
    }
}

?>