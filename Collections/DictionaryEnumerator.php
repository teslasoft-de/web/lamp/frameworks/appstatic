<?php

namespace AppStatic\Collections;
use AppStatic\Core\PropertyBase;

/**
 * @package AppStatic
 * @name DictionaryEnumerator
 * @version 1.0 (2009-01-01)
 * 
 * @author H. Christian Kusmanow
 * @copyright © 2009 H. Christian Kusmanow
 *
 */
final class DictionaryEnumerator extends PropertyBase implements \Iterator
{
    private $dictionary;
    private $keys;
    
    /**
     * Initializes a new instance of this class.
     *
     * @param Dictionary $dictionary
     */
    function __construct( &$dictionary )
    {
        $this->dictionary = $dictionary;
        $this->keys = $dictionary->Keys();
    }
    
    /**
     * Retuns the current key ( If the key is type of object, the index of the key will be returned
     * and the key object can be accessed through the KeyValuePair returned by the current() function ).
     *
     * @return mixed
     */
    public function key()
    {
        $currentKey = current( $this->keys );
        if (is_object( $currentKey ))
            return key( $this->keys );
        else
            return $currentKey;
    }
    
    /**
     * Retuns the current value ( If the key is type of object a KeyValuePair will be returned ).
     *
     * @return mixed
     */
    public function current()
    {
        $key = current( $this->keys );
        $value = $this->dictionary->GetValue( $key );
        if (is_object( $key ))
            $value = new KeyValuePair( $key, $value );
        return $value;
    }
    
    public function next()
    {
        next( $this->keys );
    }
    
    public function rewind()
    {
        reset( $this->keys );
    }
    
    public function valid()
    {
        //return isset($this->keys[$this->position]);
        return (bool) current( $this->keys );
    }
}

class KeyValuePair extends PropertyBase
{
    /**
     * Gets the key
     *
     * @return mixed
     */
    public function Key()
    {
        return $this->Key;
    }
    private $Key;
    
    /**
     * Gets the value
     *
     * @return mixed
     */
    public function Value()
    {
        return $this->Value;
    }
    private $Value;
    
    function __construct( $key, $value )
    {
        $this->Key = $key;
        $this->Value = $value;
    }
}
?>