<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 26.06.2014
 * File: MultiDimensionalObject.php
 * Encoding: UTF-8
 * Project: AppStatic 
 * */

namespace AppStatic\Collections;

/**
 * Provides an easy to use interface for reading/writing associative array based information
 * by exposing properties that represents each key of the array
 * 
 * @package AppStatic
 * @name MultiDimensionalObject
 * @version 0.1 (26.06.2014 17:45:14)
 * @author H. Christian Kusmanow
 * @copyright © 2014 H. Christian Kusmanow <christian.kusmanow@teslasoft.de>
 */
class MultiDimensionalObject
{
    /**
     * Keeps the state of each property.
     * @var array
     */
    private $properties;

    /**
     * Creates a new MultiDimensionalObject instance initialized with $properties
     * @param array $properties
     */
    public function __construct( $properties = array() )
    {
        $this->properties = array();
        $this->populate( $properties );
    }

    /**
     * Creates properties for this instance whose names/contents are defined by the keys/values in the $properties associative array
     * @param array|\ArrayObject $properties array and ArrayAccess / ArrayObject are also supported.
     */
    private function populate( $properties )
    {
        foreach ($properties as $name => $value)
            $this->create_property( $name, $value );
    }

    /**
     * Creates a new property or overrides an existing one using $name as property name and $value as its value
     * @param string $name
     * @param mixed $value
     */
    private function create_property( $name, $value )
    {
        $this->properties[ $name ] = is_array( $value ) ? $this->create_complex_property( $value ) : $this->create_simple_property( $value );
    }

    /**
     * Creates a new complex property. Complex properties are created from arrays and are represented by instances of MultiDimensionalObject
     * @param array $value
     * @return MultiDimensionalObject
     */
    private function create_complex_property( $value = array() )
    {
        return new MultiDimensionalObject( $value );
    }

    /**
     * Creates a simple property. Simple properties are the ones that are not arrays: they can be strings, bools, objects, etc.
     * @param mixed $value
     * @return mixed
     */
    private function create_simple_property( $value )
    {
        return $value;
    }

    /**
     * Gets the value of the property named $name
     * If $name does not exists, it is initilialized with an empty instance of MultiDimensionalObject before returning it
     * By using this technique, we can initialize nested properties even if the path to them don't exist
     * I.e.: $config->foo
      - property doesn't exists, it is initialized to an instance of MultiDimensionalObject and returned

      $config->foo->bar = "hello";
      - as explained before, doesn't exists, it is initialized to an instance of MultiDimensionalObject and returned.
      - when set to "hello"; bar becomes a string (it is no longer an MultiDimensionalObject instance)
     * @param string $name
     * @return string
     */
    public function __get( $name )
    {
        $this->create_property_if_not_exists( $name );
        return $this->properties[ $name ];
    }

    private function create_property_if_not_exists( $name )
    {
        if (array_key_exists( $name, $this->properties ))
            return;
        $this->create_property( $name, array() );
    }

    public function __set( $name, $value )
    {
        $this->create_property( $name, $value );
    }

}
