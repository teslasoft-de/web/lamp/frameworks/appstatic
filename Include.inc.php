<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 03.01.2014
 * File: Include.inc.php
 * Encoding: UTF-8
 * Project: AppStatic 
 **/

require_once __DIR__ . '/Configuration/Config.inc.php';